import Vue from 'vue'
import Router from 'vue-router'


Vue.use(Router)


import Layout from '../views/layout/Layout'



export const constantRouterMap = [{
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    name: 'Dashboard',
    hidden: true,
    children: [{
      path: 'dashboard',
      component: () => import('@/views/dashboard/index')
    }]
  },
  {
    path: '/resource',
    component: Layout,
    redirect: '/',
    name: 'Find',
    meta: {
      title: '资源维护',
      icon: 'example'
    },
    children: [{
        path: 'find',
        name: 'Table',
        component: () => import('@/views/resource/upload'),
        meta: {
          title: '资源上传',
          icon: 'table'
        }
      },
      {
        path: 'tree',
        name: 'Tree',
        component: () => import('@/views/resource/solrlist'),
        meta: {
          title: '资源管理',
          icon: 'tree'
        },
      },
      {
        path: '/solrfrom',
        name: 'Solrfrom',
         hidden: true,
        component: () => import('@/views/resource/Solrfrom'),
        meta: {
          title: '资源编辑',
          icon: 'table'
        },
      }
    ]
  },
  {
    path: '/sao',
    component: Layout,
    redirect: '/',
    name: 'Find',
    meta: {
      title: 'SAO管理',
      icon: 'example'
    },
    children: [
      {
        path: 'tree',
        name: 'Tree',
        component: () => import('@/views/resource/extract'),
        meta: {
          title: 'SAO自动抽取',
          icon: 'table'
        }
      },
      {
        path: 'discriminate',
        name: 'Discriminate',
        component: () => import('@/views/sao/discriminate'),
        meta: {
          title: 'SAO识别分析',
          icon: 'tree'
        }
      },
      {
          path: 'find',
          name: 'Table',
          component: () => import('@/views/sao/saolist'),
          meta: {
            title: 'SAO数据管理',
            icon: 'table'
          }
        },
    ]
  },
  {
    path: '/front',
    component: Layout,
    redirect: '/',
    replace:true,
    name: 'Find',
    meta: {
      title: '前沿知识管理',
      icon: 'example'
    },
    children: [{
        path: 'tree',
        name: 'Tree',
        component: () => import('@/views/sao/saofront'),
        meta: {
          title: '前沿等级管理',
          icon: 'table'
        }
      },
      {
        path: 'relation',
        name: 'relation',
        component: () => import('@/views/sao/relation'),
        meta: {
          title: '节点关系管理',
          icon: 'tree'
        }
      },
      {
        path: 'weight',
        name: 'weight',
        component: () => import('@/views/sao/weight'),
        meta: {
          title: '类型权重管理',
          icon: 'table'
        }
      }
    ]
  },
  {
    path: 'external-link',
    component: Layout,
    children: [{
      path: 'http://knowledge.tree.com.cn/#/resource/find',
      meta: {
        title: '前沿知识关联软件',
        icon: 'link'
      }
    }]
  },

  {
    path: '*',
    redirect: '/404',
    hidden: true
  }
]

export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRouterMap
})
