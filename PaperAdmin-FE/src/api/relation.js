import axios from '@/utils/request.js'

export function list(currentPage,pageSize) {
  return axios({
    url: `/tree/selectNodeRelation`,
    method: 'GET'
  })
}

export function add(query) {
  return axios({
    url: `/tree/addNodeRelation`,
    method: 'POST',
    data:query
  })
}

export function update(query) {
  return axios({
    url: `/tree/updateRelation`,
    method: 'POST',
    data:query
  })
}

export function deleteById(query) {
  console.log(query)
  return axios({
    url: `/tree/deleteRelation/${query}`,
    method: 'GET'
  })
}


export function getOne(query) {
  return axios({
    url: `/tree/selectNodeRelation/${query}`,
    method: 'GET'
  })
}
