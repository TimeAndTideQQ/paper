import axios from '@/utils/request.js'

export function selectSolr(query) {
  return axios({
    url: `/resource/selectSolr`,
    method: 'POST',
    data:query
  })
}

export function solrupdate(query) {
  return axios({
    url: `/resource/solrupdate`,
    method: 'POST',
    data:query
  })
}
export function solrdelete(id,type) {
  return axios({
    url: `/resource/solrdelete?id=${id}&type=${type}`,
    method: 'get'
  })
}
export function solrdeletepage(query,param) {
  return axios({
    url: `/batch/selectPage?param=${param}`,
    method: 'POST',
    data:query
  })
}
export function batchSelect() {
  return axios({
    url: `/batch/select`,
    method: 'get'
  })
}
export function selfExtracting(param) {
  return axios({
    url: `/batch/createSao?id=${param}`,
    method: 'get'
  })
}
