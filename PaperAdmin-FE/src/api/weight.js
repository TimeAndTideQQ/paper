import axios from '@/utils/request.js'

export function listType() {
  return axios({
    url: `/type/list`,
    method: 'GET'
  })
}

export function update(query) {
  return axios({
    url: `/type/update`,
    method: 'POST',
    data:query
  })
}

export function proportion() {
  return axios({
    url: `/type/proportion`,
    method: 'GET'
  })
}


export function criticUpdate(query) {
  return axios({
    url: `/type/criticUpdate`,
    method: 'POST',
    data:query
  })
}
