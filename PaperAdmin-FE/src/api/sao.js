import axios from '@/utils/request.js'

export function list(currentPage,pageSize,param) {
  return axios({
    url: `/sao/list/${currentPage}/${pageSize}?param=${param}`,
    method: 'GET'
  })
}

export function add(query) {
  return axios({
    url: `/sao/addBatch`,
    method: 'POST',
    data:query
  })
}

export function update(query) {
  return axios({
    url: `/sao/update`,
    method: 'POST',
    data:query
  })
}

export function deleteById(query) {
  console.log(query)
  return axios({
    url: `/sao/delete/${query}`,
    method: 'GET'
  })
}

export function getOne(query) {
  return axios({
    url: `/sao/get/${query}`,
    method: 'GET'
  })
}

export function calculate(query) {
  return axios({
    url: `/sao/calculate`,
    method: 'POST',
    data:query
  })
}

export function selectSaoPretreatment(query,param) {
  return axios({
    url: `/sao/selectSaoPretreatment?param=${param}`,
    method: 'POST',
    data:query
  })
}

export function createSAO(query) {
  return axios({
    url: `/sao/createSAO`,
    method: 'POST',
    data:query
  })
}


export function avg() {
  return axios({
    url: `/sao/avg`,
    method: 'GET',
  })
}


export function easyCalculate() {
  return axios({
    url: `/sao/easyCalculate`,
    method: 'GET',
  })
}
