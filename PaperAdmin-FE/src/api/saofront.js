import axios from '@/utils/request.js'

export function list(currentPage,pageSize) {
  return axios({
    url: `/sao/front/list/${currentPage}/${pageSize}`,
    method: 'GET'
  })
}

export function add(query) {
  return axios({
    url: `/sao/front/add`,
    method: 'POST',
    data:query
  })
}

export function update(query) {
  return axios({
    url: `/sao/front/update`,
    method: 'POST',
    data:query
  })
}

export function deleteById(query) {
  console.log(query)
  return axios({
    url: `/sao/front/delete/${query}`,
    method: 'GET'
  })
}


export function getOne(query) {
  return axios({
    url: `/sao/front/get/${query}`,
    method: 'GET'
  })
}
