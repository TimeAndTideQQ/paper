import request from '@/utils/request'

export function keywords(eduCode) {
  return request({
    url: `/find/hotspot/getKeywords/${eduCode}`,
    method: 'GET'
  })
}
