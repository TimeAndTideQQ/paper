// import { version } from '../../package.json';
import RelationGraph from './core4vue';
import { RelationGraphFinal } from '@/plugins/RelationGraph/models/RelationGraphFinal';
import BidirectionalTreeLayouter from '@/plugins/RelationGraph/layouters/SeeksBidirectionalTreeLayouter';
import CenterLayouter from '@/plugins/RelationGraph/layouters/SeeksCenterLayouter';
import CircleLayouter from '@/plugins/RelationGraph/layouters/SeeksCircleLayouter';
import FixedLayouter from '@/plugins/RelationGraph/layouters/SeeksFixedLayouter';
import ForceLayouter from '@/plugins/RelationGraph/layouters/SeeksForceLayouter';

import * as SeeksRGLink from '@/plugins/RelationGraph/models/RGLink';
import * as SeeksRGNode from '@/plugins/RelationGraph/models/RGNode';
import * as SeeksRGOptions from '@/plugins/RelationGraph/models/RGOptions';

// RelationGraph.install = function(Vue) {
//   Vue.component('RelationGraph', RelationGraph);
//   Vue.component('SeeksRelationGraph', RelationGraph);
// };
export const RelationGraphCore = RelationGraphFinal;
export const Layout = {
  BidirectionalTreeLayouter,
  CenterLayouter,
  CircleLayouter,
  FixedLayouter,
  ForceLayouter
};
export const RGOptions = SeeksRGOptions;
export const RGLink = SeeksRGLink;
export const RGNode = SeeksRGNode;
const install = (Vue, options) => {
  Vue.component('RelationGraph', RelationGraph);
  Vue.component('SeeksRelationGraph', RelationGraph);
};
/* istanbul ignore if */
if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue);
}
export default {
  ...RelationGraph,
  RelationGraphCore,
  Layout,
  RGOptions,
  RGLink,
  RGNode,
  // version,
  install
};
