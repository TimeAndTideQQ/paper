import Vue from 'vue'
import Vuex from 'vuex'
import request from '../utils/request'
import { Message } from 'element-ui'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    treeList: [],
    treeId: 1,
    tree: {
      style: ''  // json 字符串
    },
    nodeLang: 'cn',
    treeSetting: {
      frontTypeColor: {},
    }
  },
  getters: {
    treeStyle(state) {
      try {
        return JSON.parse(state?.tree?.style)
      } catch (error) {
        return {}
      }
    }
  },
  mutations: {
    setTreeId(state, data) {
      state.treeId = data
    },
    setTree(state, data) {
      state.tree = data
    },
    setNodeLang(state, data) {
      state.nodeLang = data
    },
    setTreeSetting(state, data) {
      state.treeSetting = data
    },
    setTreeSettingAttr(state, { name, data }) {
      state.treeSetting[name] = data
    },
  },
  actions: {
    // 获取树列表
    async getTreeList(context) {
      try {
        const res = await request.get(`/tree/selectTrees`)
        if (res.data.code === 200 || res.data.code === 20000) {
          context.state.treeList = res.data.data
          return res.data.data
        } else {
          Message.error(res.data.msg)
        }
      } catch (error) {
        console.error('🚀 ~ 获取树列表失败:', error)
      }
    },
  },
  modules: {
  }
})
