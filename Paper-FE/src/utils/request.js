//封装网络情求
import axios from 'axios';

//创建axios实例
const instance = axios.create({
  baseURL: process.env.NODE_ENV === "development" ? "http://knowledge.tree.com.cn/api" : "/api",
  timeout: -1,
});

//拦截器
instance.interceptors.request.use(
  (request) => {
    //请求成功的拦截函数
    return request; //拦截后必须返回去,否则会请求失败
  },
  (err) => {
    console.error('请求失败', err);
  }
);
//响应拦截
instance.interceptors.response.use(
  (response) => {
    //响应成功的拦截函数
    return response; //必须返回,否则instance(config)函数不会接收到数据
  },
  (err) => {
    //响应失败的拦截函数
    console.error('响应失败', err.message);
  }
);

export default instance