package com.wanfangdata.topic.service.sao.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wanfangdata.topic.entry.sao.SaoDataPreEntry;
import com.wanfangdata.topic.entry.sao.Type;
import com.wanfangdata.topic.mapper.sao.ITypeDataMapper;
import com.wanfangdata.topic.service.sao.ISaoDataPreService;
import com.wanfangdata.topic.service.sao.ITypeDataService;
import com.wanfangdata.topic.util.CRITIC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description
 * @Authors smj
 * @Date 2023/3/4 12:29
 * @Version
 */
@Service
public class ITypeDataServiceImpl extends ServiceImpl<ITypeDataMapper, Type> implements ITypeDataService {


    @Autowired
    private ISaoDataPreService saoDataPreService;

    /**
     * List转二维数组
     *
     * @param array 数组
     * @return 二维数组
     */
    private double[][] listToDimensionalArray(List<List<Double>> array) {
        double[][] resultArray = new double[array.size()][];
        for (int i = 0; i < array.size(); i++) {
            List<Double> innerList = array.get(i);
            double[] innerArray = new double[innerList.size()];
            for (int j = 0; j < innerList.size(); j++) {
                innerArray[j] = innerList.get(j);
            }
            resultArray[i] = innerArray;
        }
        return resultArray;
    }

    /**
     * 计算数据库到样本矩阵
     *
     * @param d    前沿度
     * @param type 样本类型
     * @return 矩阵
     */
    private List<Double> frontierMatrix(List<List<Double>> d, int type) {
        CRITIC critic = new CRITIC();

        double[][] componentMatrix = listToDimensionalArray(d);
        System.out.println("--------------------原始数据矩阵---------------------");
        critic.matrixoutput(componentMatrix);

        List<Integer> negList = new ArrayList<>();
        if (type == 1 || type == 3) {
            negList.add(1);
            negList.add(2);
        } else if (type == 2) {
            negList.add(1);
        }
        double[][] normalizedMatrix = critic.normalized(componentMatrix, negList);
        List<List<Double>> matrixOutput = critic.matrixoutput(normalizedMatrix);
        List<Double> outReduce = new ArrayList<>();
        for (List<Double> list : matrixOutput) {
            Double reduce = list.stream().filter(x -> !x.isNaN()).reduce(0.0, Double::sum);
            outReduce.add(reduce);
        }
        return outReduce;
    }


    /**
     * 计算样本矩阵到样本比例
     *
     * @param componentMatrix 样本矩阵
     * @return 样本比例
     */
    private double[] calculate(double[][] componentMatrix) {
        CRITIC critic = new CRITIC();

        double[][] normalizedMatrix = critic.normalized(componentMatrix, null);
        System.out.println("--------------------标准化数据矩阵---------------------");
        critic.matrixoutput(normalizedMatrix);

        double[][] pearson = critic.correlation(normalizedMatrix);
        System.out.println("--------------------皮尔逊相关系数矩阵---------------------");

        double[] informationVolume = critic.information(normalizedMatrix, pearson);
        System.out.println("--------------------指标信息承载量---------------------");
        critic.matrixoutput1(informationVolume);

        double[] weight = critic.weight(informationVolume);
        System.out.println("--------------------指标权重---------------------");
        critic.matrixoutput1(weight);
        return weight;
    }


    @Override
    public double[] getSampleProportions() {

        List<SaoDataPreEntry> records = saoDataPreService.list();
        List<SaoDataPreEntry> c = new ArrayList<>();
        String ts = "";
        for (SaoDataPreEntry record : records) {

            List<SaoDataPreEntry> termEn = saoDataPreService.list(new QueryWrapper<SaoDataPreEntry>().eq("termEN", record.getTermEn()));
            if (!ts.equals(record.getTermEn()) && termEn.size() == 3) {
                int i = 0;
                for (SaoDataPreEntry term : termEn) {
                    if (term.getNovelty() <= 0) {
                        i++;
                    }
                }
                if (i == 0) {
                    c.addAll(termEn);
                }
            }
            ts = record.getTermEn();
        }

        List<List<Double>> patentDoubleList = new ArrayList<>();
        List<List<Double>> projectDoubleList = new ArrayList<>();
        List<List<Double>> thesisDoubleList = new ArrayList<>();

        for (SaoDataPreEntry dataPreEntry : c) {
            if (dataPreEntry.getType().equals(1)) {
                List<Double> patentCollection = new ArrayList<>();
                patentCollection.add(dataPreEntry.getNovelty());
                patentCollection.add(dataPreEntry.getIncrease());
                patentCollection.add(dataPreEntry.getAttention());
                patentCollection.add(dataPreEntry.getCrossover());
                patentDoubleList.add(patentCollection);
            } else if (dataPreEntry.getType().equals(2)) {
                List<Double> projectCollection = new ArrayList<>();
                projectCollection.add(dataPreEntry.getNovelty());
                projectCollection.add(dataPreEntry.getIncrease());
                projectCollection.add(dataPreEntry.getInterests() / 10000);
                projectDoubleList.add(projectCollection);
            } else if (dataPreEntry.getType().equals(3)) {
                List<Double> thesisCollection = new ArrayList<>();
                thesisCollection.add(dataPreEntry.getNovelty());
                thesisCollection.add(dataPreEntry.getIncrease());
                thesisCollection.add(dataPreEntry.getAttention());
                thesisCollection.add(dataPreEntry.getCrossover());
                thesisDoubleList.add(thesisCollection);
            }
        }
        List<Double> frontierPatentMatrix = frontierMatrix(patentDoubleList, 1);
        List<Double> frontierProjectMatrix = frontierMatrix(projectDoubleList, 2);
        List<Double> frontierThesisMatrix = frontierMatrix(thesisDoubleList, 3);

        List<List<Double>> sampleList = new ArrayList<>();
        for (int i = 0; i < frontierPatentMatrix.size(); i++) {
            List<Double> sample = new ArrayList<>();
            sample.add(frontierPatentMatrix.get(i));
            sample.add(frontierProjectMatrix.get(i));
            sample.add(frontierThesisMatrix.get(i));
            sampleList.add(sample);
        }
        System.out.println("sampleList ====================================== " + sampleList);
        return calculate(listToDimensionalArray(sampleList));
    }
}
