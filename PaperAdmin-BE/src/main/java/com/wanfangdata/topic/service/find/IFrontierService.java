package com.wanfangdata.topic.service.find;

import com.wanfangdata.topic.solr.Query;

import java.util.Map;

/**
 * @Description 研究前沿接口
 * @Authors smj
 * @Date 2023/2/19 10:23
 * @Version
 */
public interface IFrontierService {

    /**
     * 更新最热数据
     * @param eduCode 学科号
     */
    public void updateHots(String eduCode);


    /**
     * 获取5年学科关键词
     * @return HotSpotSubjectEntity
     */
    Map<String, Long> getFiveYearKeywords( Query query);

}
