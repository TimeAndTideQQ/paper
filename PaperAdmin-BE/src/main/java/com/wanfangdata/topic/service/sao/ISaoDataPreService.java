package com.wanfangdata.topic.service.sao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wanfangdata.topic.entry.sao.SaoDataPreEntry;
import com.wanfangdata.topic.solr.Query;
import com.wanfangdata.topic.solr.entity.SolrQueryResponseEntity;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description
 * @Authors smj
 * @Date 2023/3/4 12:26
 * @Version
 */
public interface ISaoDataPreService extends IService<SaoDataPreEntry> {



    LinkedHashMap<String, Long> getSaoYears(Query query,String type);


    List<LinkedHashMap<String, Long>> getCrossoverCount(Query query, String type);

    SolrQueryResponseEntity getCrossover(Query query, String type);


    Map<String, Object> getInterestsAwardedAmountToDate(Query query);

    Long getResultCount(Query query);


}
