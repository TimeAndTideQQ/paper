package com.wanfangdata.topic.service.treestructure.impl;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wanfangdata.topic.entry.treestructure.*;
import com.wanfangdata.topic.mapper.treestructure.NodeRelationMapper;
import com.wanfangdata.topic.mapper.treestructure.TreeMapper;
import com.wanfangdata.topic.mapper.treestructure.TreeStructureMapper;
import com.wanfangdata.topic.service.sao.ISaoDataService;
import com.wanfangdata.topic.service.treestructure.ITreeStructureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName TreeService
 * @Author ZhouZhM
 * @Date 2023/3/7 15:34
 * @Description
 * @Version 1.0
 */
@Service
public class TreeStructureServiceImpl extends ServiceImpl<TreeStructureMapper, TreeStructureEntry> implements ITreeStructureService {

    @Autowired
    private TreeStructureMapper treeStructureMapper;

    @Autowired
    private NodeRelationMapper nodeRelationMapper;

    @Autowired
    private TreeMapper treeMapper;


    /**
     * 插入树节点
     * @param treeStructureEntry 树结构实体
     */
    @Override
    public int insertStructureEntry(TreeStructureEntry treeStructureEntry) {
        return treeStructureMapper.insert(treeStructureEntry);
    }

    @Override
    public boolean isExist(int treeId, int saoId) {
        return treeStructureMapper.isExit(treeId,saoId)>0;
    }

    /**
     * 删除树节点
     */
    @Override
    public void deleteStructureEntry(int id,int treeId) {
        treeStructureMapper.deleteStructureEntry(id, treeId);
    }

    /**
     * 通过树Id删除树结构（删除树时调用）
     */
    public int deleteStructureByTreeId(int treeId){
        QueryWrapper<TreeStructureEntry> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("treeId",treeId);
        //先删除树结构表
        return treeStructureMapper.delete(queryWrapper);
    }


    /**
     * 查询遍历树
     */
    @Override
    public List<Tree<Integer>> selectStructureEntry(int treeId) {
        List<NodeStructure> nodeStructures = treeStructureMapper.selectStructureEntry(treeId);
        TreeNodeConfig config = new TreeNodeConfig();
        return TreeUtil.build(nodeStructures, 0, config, (object, tree) -> {
            tree.setId(object.getId());//必填属性
            tree.setParentId(object.getpId());//必填属性
            tree.setName(object.getTermCn());
            // 扩展属性 ...
            tree.putExtra("termEn",object.getTermEn());
            tree.putExtra("nodeRelation",object.getNodeRelation());
            tree.putExtra("nodeGrade",object.getNodeGrade());
            tree.putExtra("frontName",object.getFrontName());
            tree.putExtra("frontComments",object.getFrontComments());
            tree.putExtra("frontType",object.getFrontType());
            tree.putExtra("treeId",object.getTreeId());
            tree.putExtra("saoId",object.getSaoId());
            tree.putExtra("novelty",object.getNovelty());
            tree.putExtra("increase",object.getIncrease());
            tree.putExtra("attention",object.getAttention());
            tree.putExtra("crossover",object.getCrossover());
            tree.putExtra("interests",object.getInterests());
            tree.putExtra("coordinate",object.getCoordinate());
            tree.putExtra("crossRelation",object.getCrossRelation());
        });
    }

    /**
     * 查询节点关系
     */
    @Override
    public List<NodeRelationEntry> selectNodeRelation() {
        return nodeRelationMapper.selectList(null)
                .stream()
                .filter(nodeRelationEntry -> !"null".equals(nodeRelationEntry.getName()))
                .collect(Collectors.toList());
    }

    /**
     * 查询节点关系有没有正在使用
     * @return
     */
    @Override
    public boolean nodeRelationIsUsing(int relationId) {
        QueryWrapper<TreeStructureEntry> queryWrapper = new QueryWrapper();
        queryWrapper.eq("nodeRelation",relationId);
        return treeStructureMapper.selectCount(queryWrapper)>0;
    }


    /**
     * 新增节点关系
     */
    @Override
    public int addNodeRelation(NodeRelationEntry nodeRelationEntry) {
        return nodeRelationMapper.insert(nodeRelationEntry);
    }

    /**
     * 修改节点关系
     */
    @Override
    public int updateNodeRelation(NodeRelationEntry nodeRelationEntry) {
        return nodeRelationMapper.updateById(nodeRelationEntry);
    }

    /**
     * 删除节点关系
     */
    @Override
    public int deleteNodeRelation(int relationId) {
        return nodeRelationMapper.deleteById(relationId);
    }

    /**
     * 修改节点交叉关系
     */
    @Override
    public int updateCrossRelation(CrossRelationRequest crossRelationRequest) {
        return treeStructureMapper.updateCrossRelation(crossRelationRequest.getTreeId(),crossRelationRequest.getId(),crossRelationRequest.getCrossRelation());
    }


    /**
     * 增加一颗树
     */
    @Override
    public int addTree(TreeEntry treeEntry) {
        // 增加一棵树
        return treeMapper.insert(treeEntry);
    }

    /**
     * 查询所有树
     */
    @Override
    public List<com.wanfangdata.topic.entry.treestructure.Tree> selectTrees() {
        return treeMapper.selectTrees();
    }

//    /**
//     * 通过树名称查询树
//     */
//    public TreeEntry selectTreeByName(String treeName){
//        QueryWrapper<TreeEntry> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("name",treeName);
//        return treeMapper.selectOne(queryWrapper);
//    }


    /**
     * 修改一棵树
     */
    @Override
    public int updateTree(TreeEntry treeEntry) {
        UpdateWrapper<TreeEntry> updateWrapper = new UpdateWrapper<>();
        updateWrapper
                .eq("id",treeEntry.getId())
                .set("name",treeEntry.getName())
                .set("creator",treeEntry.getCreator())
                .set("module",treeEntry.getModule())
                .set("style",treeEntry.getStyle());
        return treeMapper.update(null,updateWrapper);
    }

    /**
     * 删除一棵树
     */
    @Override
    public int deleteTree(int treeId) {
        return treeMapper.deleteById(treeId);
    }
}
