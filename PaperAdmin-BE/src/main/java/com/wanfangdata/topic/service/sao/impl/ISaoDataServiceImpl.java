package com.wanfangdata.topic.service.sao.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wanfangdata.topic.constant.SolrCollection;
import com.wanfangdata.topic.constant.SolrFiled;
import com.wanfangdata.topic.entry.batch.BatchEntry;
import com.wanfangdata.topic.entry.sao.SaoDataEntry;
import com.wanfangdata.topic.entry.sao.SaoDataPreEntry;
import com.wanfangdata.topic.entry.sao.SaoExcelUpload;
import com.wanfangdata.topic.mapper.batch.impl.IBatchServiceImpl;
import com.wanfangdata.topic.mapper.sao.ISaoMapper;
import com.wanfangdata.topic.mapper.sao.ISaoPretreatmentMapper;
import com.wanfangdata.topic.service.sao.ISaoDataService;
import com.wanfangdata.topic.service.sao.SaoExcelUploadService;
import com.wanfangdata.topic.solr.Query;
import com.wanfangdata.topic.solr.SolrUtils;
import com.wanfangdata.topic.solr.execute.facet.FacetRequest;
import com.wanfangdata.topic.solr.execute.search.SearchRequest;
import com.wanfangdata.topic.util.ExcelUtils;
import com.wanfangdata.topic.util.SaoUtils;
import com.wanfangdata.topic.util.StringUtil;
import com.wanfangdata.topic.vo.sao.SaoDataVO;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.solr.client.solrj.SolrQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @Description
 * @Authors smj
 * @Date 2023/3/4 12:29
 * @Version
 */
@Service
public class ISaoDataServiceImpl extends ServiceImpl<ISaoMapper, SaoDataEntry> implements ISaoDataService {

    private static final Logger logger = LoggerFactory.getLogger(ISaoDataServiceImpl.class);

    @Autowired
    private ISaoPretreatmentMapper iSaoPretreatmentMapper;

    @Autowired
    private IBatchServiceImpl batchService;

    @Autowired
    private SaoExcelUploadService saoExcelUploadService;


    @Override
    public List<SaoDataVO> getInformation(Query query) {
        SearchRequest searchRequest = SearchRequest.create(SolrCollection.HANDLE_PAPER)
                .q(query.build())
                .fl(SolrFiled.ABSTRACT, SolrFiled.TITLE, SolrFiled.ID)
                .rows(5);
        return SolrUtils.beans(searchRequest, SaoDataVO.class);
    }

    @Override
    public boolean createSao(int id) {
        try {
            FacetRequest facetRequest = new FacetRequest(SolrCollection.HANDLE_PAPER).q("Batch:" + id);
            facetRequest.facet("Type");
            LinkedHashMap<String, Long> facet = SolrUtils.facet(facetRequest);
            String key = new ArrayList<>(facet.keySet()).get(0);
            Long aLong = facet.get(key);
            if (aLong <= 0) {
                return false;
            }
            SolrQuery solrQuery = new SolrQuery();
            solrQuery.setQuery("Batch:" + id);
            SearchRequest searchRequest = new SearchRequest(SolrCollection.HANDLE_PAPER, solrQuery);
            searchRequest.fl("Title", "Abstract");
            searchRequest.rows(Math.toIntExact(aLong));
            Map<String, Object> map = SolrUtils.search(searchRequest);
            List<LinkedHashMap<String, Object>> data = (List<LinkedHashMap<String, Object>>) map.get("data");
            Set<String> collect = data.stream()
                    .map(LinkedHashMap::values)
                    .map(String::valueOf)
                    .distinct()
                    .map(SaoUtils::getSAo)
                    .flatMap((Function<Set<String>, Stream<String>>) Collection::stream)
                    .collect(Collectors.toSet());
            collect.parallelStream()
                    .forEach(termEN ->iSaoPretreatmentMapper.addSaoPretreatment(id,termEN,key));
        } catch (Exception e) {
            logger.error("sao生成失败！批次号:{} 异常信息:{}", id, e);
            return false;
        }
        UpdateWrapper<BatchEntry> updateWrapper = new UpdateWrapper();
        updateWrapper.eq("id", id).set("status", 1);
        boolean update = batchService.update(updateWrapper);
        return true;
    }

    @Override
    public List<SaoDataPreEntry> saoUpload(Workbook workbook) {
        //第一个sheet表格
        Sheet sheet = workbook.getSheetAt(0);
        ArrayList<SaoDataPreEntry> saoDataEntries = new ArrayList<>();
        if (sheet != null) {
            //首行 标题栏
            Row rowFiled = sheet .getRow(0);
            int cod = 0;
            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                Row row = sheet.getRow(i);
                if (row != null) {
                    Map<String, String> rowMap = new HashMap<>();
                    for (int j = 0; j < rowFiled.getLastCellNum(); j++) {
                        rowMap.put(ExcelUtils.getCellValueForStr(rowFiled.getCell(j)), ExcelUtils.getCellValueForStr(row.getCell(j)));
                    }
                    SaoExcelUpload saoExcelUpload = new SaoExcelUpload();
                    saoExcelUpload.setTermEN(rowMap.get("termEN"));
                    saoExcelUpload.setTermCN(rowMap.get("termCN"));
                    if (StringUtil.isNotNull(rowMap.get("resourcesPatent")) && rowMap.get("resourcesPatent").equals("1")){
                        saoExcelUpload.setResourcesPatent(1);
                        SaoDataPreEntry saoDataPreEntry = new SaoDataPreEntry(1, rowMap.get("termEN"), rowMap.get("termCN"));
                        saoDataEntries.add(saoDataPreEntry);
                    }
                    if (StringUtil.isNotNull(rowMap.get("resourcesProject")) && rowMap.get("resourcesProject").equals("1")){
                        saoExcelUpload.setResourcesProject(1);
                        SaoDataPreEntry saoDataPreEntry = new SaoDataPreEntry(2, rowMap.get("termEN"), rowMap.get("termCN"));
                        saoDataEntries.add(saoDataPreEntry);
                    }
                    if (StringUtil.isNotNull(rowMap.get("resourcesThesis")) && rowMap.get("resourcesThesis").equals("1")){
                        saoExcelUpload.setResourcesThesis(1);
                        SaoDataPreEntry saoDataPreEntry = new SaoDataPreEntry(3, rowMap.get("termEN"), rowMap.get("termCN"));
                        saoDataEntries.add(saoDataPreEntry);
                    }
                    saoExcelUploadService.save(saoExcelUpload);
                }
            }
        }
        return saoDataEntries;
    }


}
