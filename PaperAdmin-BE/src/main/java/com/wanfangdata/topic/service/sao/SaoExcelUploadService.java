package com.wanfangdata.topic.service.sao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wanfangdata.topic.entry.sao.SaoExcelUpload;

/**
 * 圣excel上传服务
 *
 * @author wangyou
 * @description
 * @date 2023/05/15
 */
public interface SaoExcelUploadService  extends IService<SaoExcelUpload> {


}
