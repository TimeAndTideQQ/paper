package com.wanfangdata.topic.service.sao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wanfangdata.topic.constant.SolrCollection;
import com.wanfangdata.topic.constant.SolrFiled;
import com.wanfangdata.topic.entry.sao.SaoDataPreEntry;
import com.wanfangdata.topic.mapper.sao.ISaoPreMapper;
import com.wanfangdata.topic.service.sao.ISaoDataPreService;
import com.wanfangdata.topic.solr.Query;
import com.wanfangdata.topic.solr.SolrUtils;
import com.wanfangdata.topic.solr.entity.SolrQueryResponseEntity;
import com.wanfangdata.topic.solr.execute.facet.FacetRequest;
import com.wanfangdata.topic.solr.execute.search.SearchRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description
 * @Authors smj
 * @Date 2023/3/4 12:29
 * @Version
 */
@Service
public class ISaoDataPreServiceImpl extends ServiceImpl<ISaoPreMapper, SaoDataPreEntry> implements ISaoDataPreService {


    @Override
    public LinkedHashMap<String, Long> getSaoYears(Query query, String type) {
        FacetRequest searchRequest;
        switch (type) {
            case "Patent":
                searchRequest = FacetRequest.create(SolrCollection.HANDLE_PAPER)
                        .q(query.build())
                        .facet(SolrFiled.PY);
                break;
            case "Project":
                searchRequest = FacetRequest.create(SolrCollection.HANDLE_PAPER)
                        .q(query.build())
                        .facet(SolrFiled.START_YEAR);
                break;
            case "Thesis":
                searchRequest = FacetRequest.create(SolrCollection.HANDLE_PAPER)
                        .q(query.build())
                        .facet(SolrFiled.PUBLICATION_YEAR);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + type);
        }

        return SolrUtils.facet(searchRequest);
    }

    @Override
    public List<LinkedHashMap<String, Long>> getCrossoverCount(Query query, String type) {
        FacetRequest searchRequest;
        switch (type) {
            case "Patent":
                searchRequest = FacetRequest.create(SolrCollection.HANDLE_PAPER)
                        .q(query.build())
                        .facet(SolrFiled.IP);
                break;
            case "Thesis":
                searchRequest = FacetRequest.create(SolrCollection.HANDLE_PAPER)
                        .q(query.build())
                        .facet(SolrFiled.CLASSIFICATION_CODE);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + type);
        }

        return SolrUtils.facetMulti(searchRequest);
    }

    @Override
    public SolrQueryResponseEntity getCrossover(Query query, String type) {
        SearchRequest searchRequest;
        if ("Thesis".equals(type)){
            searchRequest = SearchRequest.create(SolrCollection.HANDLE_THESIS)
                    .q(query.build())
                    .statsFiled(SolrFiled.CLASSIFICATION_CODE);
        }else {
            searchRequest = SearchRequest.create(SolrCollection.HANDLE_PATENT)
                    .q(query.build())
                    .statsFiled(SolrFiled.IP);
        }
        return  SolrUtils.statsMtu(searchRequest);
    }

    @Override
    public Map<String, Object> getInterestsAwardedAmountToDate(Query query) {
        SearchRequest queryRequest = SearchRequest.create(SolrCollection.HANDLE_PAPER).q(query.build());
        return SolrUtils.search(queryRequest);
    }

    @Override
    public Long getResultCount(Query query) {
        SearchRequest queryRequest = SearchRequest.create(SolrCollection.HANDLE_PAPER).q(query.build());
        return SolrUtils.count(queryRequest);
    }
}
