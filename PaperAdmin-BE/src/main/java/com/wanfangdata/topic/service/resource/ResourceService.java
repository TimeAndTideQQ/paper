package com.wanfangdata.topic.service.resource;

import org.apache.poi.ss.usermodel.Workbook;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Administrator
 */

public interface ResourceService {

    /**
     *  excel解析
     * @param dataType
     * @param workbook
     */
    public void dataAnalysis(String dataType, Workbook workbook);

    /**
     *  solr列表查询
     * @param queryMap
     * @return
     */
    public Map selectSolr(HashMap<String,String> queryMap);

    /**
     *  solr列表查询
     * @param dataMap
     * @return
     */
    public boolean UpdateSolr(HashMap<String,String> dataMap);

    /**
     *  solr删除
     * @param id
     * @return
     */
    public boolean deleteSolr(String id,String type);

}
