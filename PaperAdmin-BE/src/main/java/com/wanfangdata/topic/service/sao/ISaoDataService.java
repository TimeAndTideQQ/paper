package com.wanfangdata.topic.service.sao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wanfangdata.topic.entry.sao.SaoDataEntry;
import com.wanfangdata.topic.entry.sao.SaoDataPreEntry;
import com.wanfangdata.topic.solr.Query;
import com.wanfangdata.topic.vo.sao.SaoDataVO;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.List;

/**
 * @Description
 * @Authors smj
 * @Date 2023/3/4 12:26
 * @Version
 */
public interface ISaoDataService  extends IService<SaoDataEntry> {

    /**
     * SAO词推送论文
     * @param query 查询语句
     * @return List
     */
    List<SaoDataVO> getInformation(Query query);


    boolean createSao(int id);

    List<SaoDataPreEntry> saoUpload(Workbook workbook);

}
