package com.wanfangdata.topic.service.sao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wanfangdata.topic.entry.sao.Type;

/**
 * @Description
 * @Authors smj
 * @Date 2023/3/4 12:26
 * @Version
 */
public interface ITypeDataService extends IService<Type> {

    /**
     * 计算样本矩阵的比例
     *
     * @return 比例
     */
    double[] getSampleProportions();
}
