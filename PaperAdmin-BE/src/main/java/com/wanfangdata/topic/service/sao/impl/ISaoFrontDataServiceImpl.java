package com.wanfangdata.topic.service.sao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wanfangdata.topic.entry.sao.SaoFrontier;
import com.wanfangdata.topic.mapper.sao.ISaoFrontMapper;
import com.wanfangdata.topic.service.sao.ISaoFrontDataService;
import org.springframework.stereotype.Service;

/**
 * @Description
 * @Authors smj
 * @Date 2023/3/4 12:29
 * @Version
 */
@Service
public class ISaoFrontDataServiceImpl extends ServiceImpl<ISaoFrontMapper, SaoFrontier> implements ISaoFrontDataService {


}
