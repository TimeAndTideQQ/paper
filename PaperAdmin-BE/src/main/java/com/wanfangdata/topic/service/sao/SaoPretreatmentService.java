package com.wanfangdata.topic.service.sao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wanfangdata.topic.entry.sao.SaoPretreatment;

/**
 * @author 10062
 */
public interface SaoPretreatmentService extends IService<SaoPretreatment> {
}
