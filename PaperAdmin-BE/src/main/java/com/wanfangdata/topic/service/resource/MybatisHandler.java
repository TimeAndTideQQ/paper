//package com.wanfangdata.topic.service.resource;
//
//import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
//import org.apache.ibatis.reflection.MetaObject;
//import org.springframework.stereotype.Component;
//
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
///**
// * @ClassName MybatisHandler
// * @Author ZhouZhM
// * @Date 2023/3/23 10:26
// * @Description 自动装配时间
// * @Version 1.0
// */
//@Component
//public class MybatisHandler implements MetaObjectHandler {
//    @Override
//    public void insertFill(MetaObject metaObject) {
//        Date date = new Date();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        this.setFieldValByName("time", date, metaObject);
//        this.setFieldValByName("name",sdf.format(date), metaObject);
//    }
//
//    @Override
//    public void updateFill(MetaObject metaObject) {
////        this.setFieldValByName("updateTime", new Date(), metaObject);
//    }
//}
