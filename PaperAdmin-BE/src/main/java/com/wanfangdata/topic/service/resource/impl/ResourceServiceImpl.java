package com.wanfangdata.topic.service.resource.impl;

import com.wanfangdata.topic.constant.SolrCollection;
import com.wanfangdata.topic.constant.SolrFiled;
import com.wanfangdata.topic.dao.resource.ResourceDao;
import com.wanfangdata.topic.dao.resource.ResourceUploadDao;
import com.wanfangdata.topic.entry.batch.BatchEntry;
import com.wanfangdata.topic.mapper.batch.IBatchMapper;
import com.wanfangdata.topic.service.resource.ResourceService;
import com.wanfangdata.topic.solr.SolrUtils;
import com.wanfangdata.topic.solr.execute.search.SearchRequest;
import com.wanfangdata.topic.util.ExcelUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.solr.client.solrj.SolrQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 文件上传
 *
 * @author Administrator
 * @date 2023/03/01 13:56
 **/
@Service
public class ResourceServiceImpl implements ResourceService {

    private static final Logger logger = LoggerFactory.getLogger(ResourceServiceImpl.class);
    private static final String ID = "id";
    private static final String TYPE = "type";
    private static final String KEYWORD = "keyword";
    private static final String BATCH = "patch";

    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private ResourceDao resourceDao;

    @Autowired
    private IBatchMapper batchMapper;

    @Override
    public void dataAnalysis(String dataType, Workbook workbook) {
        ResourceUploadDao bean = applicationContext.getBean(dataType, ResourceUploadDao.class);
        String batchEntryID = getBatchEntryID(dataType);
        //第一个sheet表格
        Sheet sheet = workbook.getSheetAt(0);
        if (sheet != null) {
            //首行 标题栏
            Row rowFiled = sheet .getRow(0);
            List<Map<String, String>> rowMapList = new ArrayList<>();
            int cod = 0;
            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                Row row = sheet.getRow(i);
                if (row != null) {
                    Map<String, String> rowMap = new HashMap<>();
                    for (int j = 0; j < rowFiled.getLastCellNum(); j++) {
                        rowMap.put(ExcelUtils.getCellValueForStr(rowFiled.getCell(j)), ExcelUtils.getCellValueForStr(row.getCell(j)));
                    }
                    rowMap.put("Batch", batchEntryID);
                    rowMapList.add(rowMap);
                }
                if (rowMapList.size() >= 50) {
                    cod += 50;
                    if (!bean.createDocument(rowMapList)) {
                        logger.error("solr数据添加错误：{}");
                    }
                    if (cod == 2000) {
                        batchEntryID = getBatchEntryID(dataType);
                        cod = 0;
                    }
                    rowMapList.clear();
                }
            }
            if (rowMapList.size() > 0) {
                if (!bean.createDocument(rowMapList)) {
                    logger.error("solr数据添加错误：{}");
                }
            }
        }
    }

    @Override
    public Map<String, Object> selectSolr(HashMap<String, String> queryMap) {
        List<String> queryList = new ArrayList<>();
        Integer pageSize = 0;
        Integer pagerNum = 0;
        for (Map.Entry<String, String> queryEntry : queryMap.entrySet()) {
            String value = queryEntry.getValue();
            if (value != null && value.length() > 0) {
                switch (queryEntry.getKey()) {
                    case ID:
                        queryList.add(SolrFiled.ID + ":(*" + value + "*)");
                        break;
                    case TYPE:
                        queryList.add(SolrFiled.TYPE + ":" + value);
                        break;
                    case BATCH:
                        queryList.add(SolrFiled.BATCH + ":" + value);
                        break;
                    case KEYWORD:
                        queryList.add("(" + SolrFiled.TITLE + ":(*" + value + "*) OR " + SolrFiled.ABSTRACT + ":(*" + value + "*))");
                        break;
                    case "pageSize":
                        pageSize = Integer.parseInt(value != null && value.length() > 0 ? value : "0");
                        break;
                    case "pagerNum":
                        pagerNum = Integer.parseInt(value != null && value.length() > 0 ? value : "0");
                        break;
                    default:
                        break;
                }
            }
        }
        SolrQuery solrQuery = new SolrQuery();
        if (queryList.size() > 0) {
            solrQuery.setQuery(String.join(" AND ", queryList));
        } else {
            solrQuery.setQuery("*:*");
        }
        solrQuery.setFields(SolrFiled.ID, SolrFiled.TITLE, SolrFiled.ABSTRACT, SolrFiled.TYPE, SolrFiled.BATCH);
        solrQuery.setStart(pageSize * (pagerNum - 1));
        solrQuery.setRows(pageSize <= 0 ? 10 : pageSize);
        Map<String, Object> solrDataMap = resourceDao.selectSolr(new SearchRequest(SolrCollection.HANDLE_PAPER, solrQuery));

        if (solrDataMap.get("data") != null) {
            ArrayList<LinkedHashMap<String, Object>> arrayList = (ArrayList<LinkedHashMap<String, Object>>) solrDataMap.get("data");
            if (arrayList.size() > 0) {
                List<LinkedHashMap<String, Object>> collect = arrayList.stream().peek(x -> x.put("BatchName",
                        batchMapper.selectById(Integer.parseInt(x.get(SolrFiled.BATCH).toString())).getName())).collect(Collectors.toList());
                solrDataMap.remove("data");
                solrDataMap.put("data", collect);
            }
        }

        solrDataMap.put("pageSize", pageSize);
        solrDataMap.put("pagerNum", pagerNum);
        return solrDataMap;
    }

    @Override
    public boolean UpdateSolr(HashMap<String, String> dataMap) {
        LinkedHashMap<String, Object> solrMap;
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery("Id:" + dataMap.get("Id"));
        Map<String, Object> map = resourceDao.selectSolr(new SearchRequest(SolrCollection.HANDLE_PAPER, solrQuery));
        try {
            List<LinkedHashMap<String, Object>> data = (List<LinkedHashMap<String, Object>>) map.get("data");
            solrMap = data.get(0);
            for (Map.Entry<String, String> entry : dataMap.entrySet()) {
                if (entry.getKey().equals("Title") || entry.getKey().equals("Abstract")) {
                    solrMap.put(entry.getKey(), entry.getValue());
                }
            }
            return resourceDao.UpdateSolr(solrMap);
        } catch (Exception e) {
            logger.error("修改数据异常：{}", e);
        }
        return false;
    }

    @Override
    public boolean deleteSolr(String id, String type) {
        String collection = "";
        if (type.equals("Thesis")) {
            collection = SolrCollection.HANDLE_THESIS;
        } else if (type.equals("Patent")) {
            collection = SolrCollection.HANDLE_PATENT;
        } else if (type.equals("Project")) {
            collection = SolrCollection.HANDLE_PROJECT;
        } else {
            return false;
        }
        return SolrUtils.delete(id, collection);
    }

    public String getBatchEntryID(String dataType) {
        // 批次表 插入一条批次数据
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        BatchEntry batchEntry = new BatchEntry(date, dataType + "_" + sdf.format(date));
        int num = batchMapper.insert(batchEntry);
        logger.info("成功插入{}批数据", num);
        return String.valueOf(batchEntry.getId());
    }


}
