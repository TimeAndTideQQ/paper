package com.wanfangdata.topic.service.batch;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wanfangdata.topic.entry.batch.BatchEntry;

/**
 * @ClassName IBatchService
 * @Author ZhouZhM
 * @Date 2023/3/23 14:05
 * @Description
 * @Version 1.0
 */
public interface IBatchService extends IService<BatchEntry> {
}
