package com.wanfangdata.topic.service.sao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wanfangdata.topic.entry.sao.SaoPretreatment;
import com.wanfangdata.topic.mapper.sao.ISaoPretreatmentMapper;
import com.wanfangdata.topic.service.sao.SaoPretreatmentService;
import org.springframework.stereotype.Service;

/**
 * @author 10062
 * @date 2023/04/05 19:21
 **/
@Service
public class SaoPretreatmentServiceImpl extends ServiceImpl<ISaoPretreatmentMapper, SaoPretreatment> implements SaoPretreatmentService {
}
