package com.wanfangdata.topic.service.treestructure;


import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wanfangdata.topic.entry.treestructure.*;

import java.util.List;

/**
 * @ClassName ITreeService
 * @Author ZhouZhM
 * @Date 2023/3/7 15:52
 * @Description
 * @Version 1.0
 */
public interface ITreeStructureService extends IService<TreeStructureEntry> {

    /**
     * 插入树节点
     */
    int insertStructureEntry(TreeStructureEntry treeStructureEntry);

    boolean isExist(int treeId,int saoId);

    /**
     * 删除节点
     * @return 删除的节点数
     */
    void deleteStructureEntry(int id,int treeId);

    /**
     * 查询树
     */
    List<Tree<Integer>> selectStructureEntry(int treeId);

    /**
     * 查询节点关系
     */
    List<NodeRelationEntry> selectNodeRelation();

    /**
     * 查询节点关系有没有在使用
     */
    boolean nodeRelationIsUsing(int relationId);

    /**
     * 新增节点关系
     */
    int addNodeRelation(NodeRelationEntry nodeRelationEntry);

    /**
     * 修改节点关系
     */
    int updateNodeRelation(NodeRelationEntry nodeRelationEntry);

    /**
     * 删除节点关系
     */
    int deleteNodeRelation(int relationId);

    /**
     * 修改节点交叉关系
     */
    int updateCrossRelation(CrossRelationRequest crossRelationRequest);

    /**
     * 增加树
     */
    int addTree(TreeEntry treeEntry);

    /**
     * 查询所有树
     */
    List<com.wanfangdata.topic.entry.treestructure.Tree> selectTrees();

    /**
     * 修改树
     */
    int updateTree(TreeEntry treeEntry);

    /**
     * 删除树
     */
    int deleteTree(int treeId);
}
