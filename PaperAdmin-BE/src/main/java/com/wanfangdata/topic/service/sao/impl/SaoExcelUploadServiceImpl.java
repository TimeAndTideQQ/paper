package com.wanfangdata.topic.service.sao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wanfangdata.topic.entry.sao.SaoExcelUpload;
import com.wanfangdata.topic.mapper.sao.SaoExcelUploadDao;
import com.wanfangdata.topic.service.sao.SaoExcelUploadService;
import org.springframework.stereotype.Service;

/**
 * 圣excel impl上传服务
 *
 * @author wangyou
 * @description
 * @date 2023/05/15
 */
@Service
public class SaoExcelUploadServiceImpl extends ServiceImpl<SaoExcelUploadDao, SaoExcelUpload> implements SaoExcelUploadService {


}
