package com.wanfangdata.topic.service.find.impl;

import com.wanfangdata.topic.dao.find.IFrontierDao;
import com.wanfangdata.topic.service.find.IFrontierService;
import com.wanfangdata.topic.solr.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @Description 研究前沿接口
 * @Authors smj
 * @Date 2023/2/19 10:23
 * @Version
 */
@Service
public class FrontierHotspotSubjectServiceImpl implements IFrontierService {


    @Autowired
    private IFrontierDao iFrontierDao;

    private final Logger logger = LoggerFactory.getLogger(FrontierHotspotSubjectServiceImpl.class);
    private static final List<String> YEAR = Arrays.asList("2018", "2019", "2020", "2021", "2022");


    @Override
    public Map<String, Long> getFiveYearKeywords(Query query) {
        return iFrontierDao.getFiveYearKeywords( query);
    }


    @Override
    public void updateHots(String eduCode) {
//
//        try {
//            //3年的热门选题关键词
//            final PaperRequest paperRequest = PaperRequest.create(Collection.TOPIC_PERIODICAL_CHI, Query.accurate(SearchField.EDU_CODE, eduCode).build()).rows(800);
//            final List<Map<String, String>> filterKeywordsFacet = findRepository.getSubjectHotSpot(paperRequest).stream()
//                    .filter(map -> (map.get(Constant.FACET).length() >= 3) || (!map.get(Constant.FACET).contains("《")) || (!map.get(Constant.FACET).contains("》"))).collect(Collectors.toList());
//            //计算学科下关键词指数并进行排序
//            final Iterator<Map.Entry<String, Long>> keywordTrendsIterator = this.getIndex(filterKeywordsFacet.stream().map(keyword -> keyword.get(Constant.FACET)).collect(Collectors.toList()), eduCode);
//            //加入产品提供的各学科关键词，进行指数计算趋势计算
//            final List<String> productKeywordList = this.handleKeyword(keywordTrendsIterator, eduCode);
//            //合并后的关键词再次排序
//            final Iterator<Map.Entry<String, Long>> entryIterator = this.getIndex(productKeywordList, eduCode);
//
//            final List<Long> indexList = new ArrayList<>(40);
//            final List<List<Long>> quantityList = new ArrayList<>(40);
//            List<String> keywordList = new ArrayList<>(40);
//            while (entryIterator.hasNext()) {
//                final Map.Entry<String, Long> nextKeyIndex = entryIterator.next();
//                final List<Long> trendsList = this.getTrendsList(eduCode, nextKeyIndex);
//                quantityList.add(trendsList);
//                keywordList.add(nextKeyIndex.getKey());
//                indexList.add(nextKeyIndex.getValue());
//            }
//
//            //写入sql；季度更新做最热数据时先清空原有数据
//            this.saveOrUpdateSql(eduCode, keywordList, indexList, quantityList, true);
//
//        } catch (Exception e) {
//            logger.info("学科热点：最热选题查询3年的热门主题词或指数错误！", e);
//        }
    }


    /**
     * 过滤符合规则的关键词,并加入产品提供的关键词
     *
     * @param keywordTrendsIterator 关键词趋势
     * @param eduCode               学科号
     */
    private List<String> handleKeyword(Iterator<Map.Entry<String, Long>> keywordTrendsIterator, String eduCode) throws Exception {
        List<String> keywordList = new ArrayList<>(40);
        int i = 0;
        //取符合的关键词->循环判断关键词的趋势是否符合要求规则，并过滤结果中含有结果禁用词，达到40个词结束循环
        while (keywordTrendsIterator.hasNext()) {
            final Map.Entry<String, Long> nextKeyIndex = keywordTrendsIterator.next();
            final List<Long> trendsList = this.getTrendsList(eduCode, nextKeyIndex);
            //2020>2018 && 2021>2019
            if (trendsList.get(3) > trendsList.get(1) && trendsList.get(2) > trendsList.get(0)) {
                //过滤页面上关键词禁用词 | 查询5年趋势
                if (!this.removePostPositionStopWords(nextKeyIndex.getKey())) {
                    keywordList.add(nextKeyIndex.getKey());
                }
                if (i == 40) {
                    break;
                }
                i++;
            }
        }
        //加入产品提供的各学科关键词，指数计算，趋势计算
        return this.getReplaceKeywordByEducationCode(eduCode, keywordList);
    }

    /**
     * 计算学科号下关键词指数
     *
     * @param educationCode 学科号
     * @param nextKeyIndex  关键词
     * @throws Exception Exception
     */
    private List<Long> getTrendsList(String educationCode, Map.Entry<String, Long> nextKeyIndex) throws Exception {
//        final PaperRequest paperRequestTotalTrend = PaperRequest.create(Collection.PERIO_CHI_FOR_TOPIC, FindParamHandlUtil.getCommonRequestHotSpotCount(educationCode, nextKeyIndex.getKey()));
//        Map<String, Long> subjectHotSpotYearTrend = findRepository.getSubjectHotSpotYearTrend(paperRequestTotalTrend);
//        return this.adjustmentQuantity(subjectHotSpotYearTrend);
        return null;
    }

    /**
     * 获取学科下关键词替换集合
     *
     * @param eduCode 学科号
     * @return 关键词集合
     */
    private List<String> getReplaceKeywordByEducationCode(String eduCode, List<String> keywords) {
//        //产品提供关键词
//        List<String> replaceKeywords = this.replaceKeywords(findRepository.getHotSubjectReplaceKeywordData(), eduCode);
//        replaceKeywords.addAll(keywords);
//        return replaceKeywords.stream().limit(40).distinct().collect(Collectors.toList());
        return null;
    }

    /**
     * 过滤产品提供关键词指数小于0的关键词
     *
     * @param hotSubjectReplaceKeywordData 产品提供的关键词
     * @param eduCode                      学科号
     * @return List<String>
     */
    private List<String> replaceKeywords(Map<String, List<String>> hotSubjectReplaceKeywordData, String eduCode) {
//        List<String> replaceKeywords = hotSubjectReplaceKeywordData.get(eduCode);
//        if (replaceKeywords != null && replaceKeywords.size() > 0) {
//            replaceKeywords = replaceKeywords.stream().filter(key -> {
//                final PaperRequest paperRequestTotalTrend = PaperRequest.create(Collection.PERIO_CHI_FOR_TOPIC, FindParamHandlUtil.getCommonRequestHotSpotCount(eduCode, key));
//                try {
//                    return findRepository.getSubjectHotSpotTotalTrend(paperRequestTotalTrend) > 0;
//                } catch (Exception e) {
//                    logger.info("学科热点：最热选题查询3年的热门主题词和指数错误！", e);
//                }
//                return false;
//            }).collect(Collectors.toList());
//        }
//        return replaceKeywords;
        return null;
    }


    /**
     * 调整趋势年份顺序
     * 调整为 2018,2019,2020,2021,2022
     *
     * @param map 趋势
     * @return 年份趋势
     */
    private List<Long> adjustmentQuantity(Map<String, Long> map) {
        List<Long> adjustmentKeyCount = new ArrayList<>(5);
        YEAR.forEach(x -> adjustmentKeyCount.add(map.get(x) == null ? 0 : map.get(x)));
        return adjustmentKeyCount;
    }


    /**
     * 在结果页面过滤禁用词
     * postPositionStopWords 结果禁用词
     *
     * @return 是否含有禁用词
     */
    private boolean removePostPositionStopWords(String keyword) {
//        return findRepository.getHotSubjectStopWordsData().stream().map(String::toUpperCase).collect(Collectors.toList()).contains(keyword.toUpperCase());
        return false;
    }


    /**
     * 通过关键词标题计算学科下关键词的指数_指数倒叙排序
     *
     * @param keywordList 关键词集合
     * @param eduCode     学科号
     * @return 排序完成的关键词与指数
     * @throws Exception e
     */
    private Iterator<Map.Entry<String, Long>> getIndex(List<String> keywordList, String eduCode) throws Exception {
//        final LinkedHashMap<String, Long> keywordAndIndex = new LinkedHashMap<>();
//        for (String keyword : keywordList) {
//            final PaperRequest paperRequestTotalTrend = PaperRequest.create(Collection.PERIO_CHI_FOR_TOPIC, FindParamHandlUtil.getCommonRequestHotSpotCount(eduCode, keyword));
//            final long subjectHotSpotTotalTrend = findRepository.getSubjectHotSpotTotalTrend(paperRequestTotalTrend);
//            keywordAndIndex.put(keyword, subjectHotSpotTotalTrend);
//        }
//        final List<Map.Entry<String, Long>> keywordEntryArrayList = new ArrayList<>(keywordAndIndex.entrySet());
//        keywordEntryArrayList.sort(((o1, o2) -> o2.getValue().compareTo(o1.getValue())));
//        return keywordEntryArrayList.iterator();
        return null;
    }


}
