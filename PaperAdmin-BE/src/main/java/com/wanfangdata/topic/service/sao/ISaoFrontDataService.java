package com.wanfangdata.topic.service.sao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wanfangdata.topic.entry.sao.SaoFrontier;

/**
 * @Description
 * @Authors smj
 * @Date 2023/3/4 12:26
 * @Version
 */
public interface ISaoFrontDataService extends IService<SaoFrontier> {


}
