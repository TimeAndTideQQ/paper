package com.wanfangdata.topic.dto;

/**
 * @Description 接收前端登录信息
 * @Authors smj
 * @Date 2023/2/18 17:26
 * @Version
 */
public class UserLogin {
    private String username;
    private String password;

    public UserLogin() {
    }

    public UserLogin(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
