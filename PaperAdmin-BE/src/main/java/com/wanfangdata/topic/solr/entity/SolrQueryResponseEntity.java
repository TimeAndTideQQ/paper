package com.wanfangdata.topic.solr.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.solr.client.solrj.response.FieldStatsInfo;
import org.apache.solr.common.SolrDocumentList;

/**
 * @Description
 * @Authors smj
 * @Date 2023/5/19 16:54
 * @Version
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SolrQueryResponseEntity {

    private FieldStatsInfo fieldStatsInfo;
    private SolrDocumentList solrDocuments;


}
