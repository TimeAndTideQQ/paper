package com.wanfangdata.topic.solr.execute.update;

import org.apache.solr.common.SolrInputDocument;

import java.util.List;

/**
 * @Description 修改
 * @Authors smj
 * @Date 2023/3/2 16:21
 * @Version
 */
public class UpdateRequest {

    private final String collection;
    private List<SolrInputDocument> documentList;

    public UpdateRequest(String collection) {
        this.collection = collection;

    }


    public String getCollection() {
        return collection;
    }

    public List<SolrInputDocument> getDocumentList() {
        return documentList;
    }


    public static UpdateRequest create(String collection) {
        return new UpdateRequest(collection);
    }

    /**
     * 添加的数据
     *
     * @param documents 数据集合
     * @return UpdateRequest
     */
    public UpdateRequest documentList(List<SolrInputDocument> documents) {
        this.documentList = documents;
        return this;
    }
}
