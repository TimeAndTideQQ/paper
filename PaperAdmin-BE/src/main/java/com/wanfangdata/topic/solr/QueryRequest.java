package com.wanfangdata.topic.solr;

/**
 * @Description
 * @Authors smj
 * @Date 2023/2/17 15:40
 * @Version
 */
public class QueryRequest {


    private String q;
    private String facet;
    private String fq;
    private String sort;
    private String fl;
    private String collection;

    public QueryRequest(String q, String collection) {
        this.q = q;
        this.collection = collection;
    }

    public static QueryRequest create(String collection, String q) {
        return new QueryRequest(q, collection);
    }


    public String getFl() {
        if (fl != null) {
            return fl;
        }
        return "";
    }

    public QueryRequest fl(String fl) {
        this.fl = fl;
        return this;
    }

    public String getQ() {
        return q;
    }

    public QueryRequest q(String q) {
        this.q = q;
        return this;
    }

    public String getFq() {
        return fq;
    }

    public QueryRequest fq(String fq) {
        this.fq = fq;
        return this;
    }

    public String getSort() {
        return sort;
    }

    public QueryRequest sort(String sort) {
        this.sort = sort;
        return this;
    }

    public String getCollection() {
        return collection;
    }

    public QueryRequest collection(String collection) {
        this.collection = collection;
        return this;
    }

    public String getFacet() {
        return facet;
    }

    public QueryRequest facet(String facet) {
        this.facet = facet;
        return this;
    }
}
