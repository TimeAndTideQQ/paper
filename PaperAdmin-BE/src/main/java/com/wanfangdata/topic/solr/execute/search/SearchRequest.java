package com.wanfangdata.topic.solr.execute.search;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.common.params.StatsParams;

/**
 * @Description 查询
 * @Authors smj
 * @Date 2023/3/2 14:48
 * @Version
 */
public class SearchRequest {

    private final SolrQuery solrQuery;

    private final String collection;

    public SearchRequest(String collection, SolrQuery solrQuery) {
        this.collection = collection;
        this.solrQuery = solrQuery;
    }


    public SolrQuery getSolrQuery() {
        return solrQuery;
    }

    public String getCollection() {
        return collection;
    }


    /**
     * 初始化
     * @param collection 库名
     * @return SearchRequest
     */
    public static SearchRequest create(String collection) {
        return new SearchRequest(collection, new SolrQuery());
    }


    /**
     * 查询
     *
     * @param q 查询条件
     * @return SolrSearchRequest
     */
    public SearchRequest q(String q) {
        solrQuery.setQuery(q);
        return this;
    }

    /**
     * 限制查询
     *
     * @param fq 参数
     * @return SolrSearchRequest
     */
    public SearchRequest fq(String... fq) {
        solrQuery.setFilterQueries(fq);
        return this;
    }

    /**
     * 返回字段
     *
     * @param fl 参数
     * @return SolrSearchRequest
     */
    public SearchRequest fl(String... fl) {
        solrQuery.setFields(fl);
        return this;
    }

    /**
     * 排序
     *
     * @param field 排序字段
     * @param order 排序方式
     * @return SolrSearchRequest
     */
    public SearchRequest sort(String field, SolrQuery.ORDER order) {
        solrQuery.setSort(field, order);
        return this;
    }

    /**
     * 返回条数
     *
     * @param rows 返回条数
     * @return SolrSearchRequest
     */
    public SearchRequest rows(Integer rows) {
        solrQuery.setRows(rows);
        return this;
    }

    /**
     * 返回起始条数
     *
     * @param rows 起始
     * @return SolrSearchRequest
     */
    public SearchRequest start(Integer rows) {
        solrQuery.setStart(rows);
        return this;
    }

    public SearchRequest statsFiled(String filed){
        solrQuery.setParam(StatsParams.STATS, Boolean.toString(true));
        solrQuery.setParam(StatsParams.STATS_FIELD, filed);
        return this;
    }




}
