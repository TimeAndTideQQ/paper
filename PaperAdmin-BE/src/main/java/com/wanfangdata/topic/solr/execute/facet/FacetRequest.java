package com.wanfangdata.topic.solr.execute.facet;

import org.apache.solr.client.solrj.SolrQuery;

import java.util.Arrays;
import java.util.List;

/**
 * @Description 聚类
 * @Authors smj
 * @Date 2023/3/2 14:48
 * @Version
 */
public class FacetRequest {

    private final SolrQuery solrQuery = new SolrQuery();

    private final String collection;



    public SolrQuery getSolrQuery() {
        return solrQuery;
    }

    public String getCollection() {
        return collection;
    }


    public FacetRequest(String collection) {
        this.collection = collection;
        rows();
        facetEnable();
        facetMinCount();
    }


    /**
     * 初始化
     *
     * @param collection 库名
     * @return FacetRequest
     */
    public static FacetRequest create(String collection) {
        return new FacetRequest(collection);
    }


    /**
     * 查询
     *
     * @param q 查询条件
     * @return SolrSearchRequest
     */
    public FacetRequest q(String q) {
        solrQuery.setQuery(q);
        return this;
    }

    /**
     * 限制查询
     *
     * @param fq 参数
     * @return SolrSearchRequest
     */
    public FacetRequest fq(String... fq) {
        solrQuery.setFilterQueries(fq);
        return this;
    }

    /**
     * 返回字段
     *
     * @param fl 参数
     * @return SolrSearchRequest
     */
    public FacetRequest fl(String... fl) {
        solrQuery.setFields(fl);
        return this;
    }

    /**
     * 排序
     *
     * @param field 排序字段
     * @param order 排序方式
     * @return SolrSearchRequest
     */
    public FacetRequest sort(String field, SolrQuery.ORDER order) {
        solrQuery.setSort(field, order);
        return this;
    }

    /**
     * 聚类
     *
     * @param facetField 聚类字段
     * @return SolrSearchRequest
     */
    public FacetRequest facet(String... facetField) {
        solrQuery.addFacetField(facetField);
        return this;
    }

    /**
     * facet条数
     *
     * @param limit 返回facet条数
     * @return SolrSearchRequest
     */
    public FacetRequest limit(Integer limit) {
        solrQuery.setFacetLimit(limit);
        return this;
    }

    /**
     * facet排序
     *
     * @param sort 排序
     * @return SolrSearchRequest
     */
    public FacetRequest facetSort(String sort) {
        solrQuery.setFacetSort(sort);
        return this;
    }


    /**
     * 限制facet返回结果
     *
     * @param prefix 前缀
     * @return SolrSearchRequest
     */
    public FacetRequest facetPrefix(String prefix) {
        solrQuery.setFacetPrefix(prefix);
        return this;
    }


    /**
     * 默认行
     */
    private void rows() {
        solrQuery.setRows(0);
    }

    /**
     * 默认开启facet
     */
    private void facetEnable() {
        solrQuery.setFacet(true);
    }

    /**
     * 默认facet聚类最小数
     */
    private void facetMinCount() {
        solrQuery.setFacetMinCount(1);
    }


    /**
     * 获取聚类字段
     * @return List
     */
    public List<String> getFacetFiled(){
        return Arrays.asList(solrQuery.getFacetFields());
    }

    /**
     * 移除聚类字段
     * @param facetFields 聚类字段
     */
    public void removeFacetField(List<String> facetFields){
        facetFields.forEach(solrQuery::removeFacetField);
        rows();
        facetEnable();
        facetMinCount();
    }





    /**
     * 获取查询条件
     * @return String
     */
    public String getQuery(){
        return solrQuery.getQuery();
    }




}
