package com.wanfangdata.topic.solr;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @Description
 * @Authors smj
 * @Date 2023/2/18 14:23
 * @Version
 */
public class Query {


    private final String NOT = "NOT";
    private final String LEFT = "(";
    private final String RIGHT = ")";
    private final String SPACE = " ";
    private final String COLON = ":";
    private final String MARK = "\"";

    private final StringBuilder stringBuilder;

    public Query() {
        this.stringBuilder = new StringBuilder();
    }

    public Query and() {
        stringBuilder.append(Operation.AND.value());
        return this;
    }

    public Query or() {
        stringBuilder.append(Operation.OR.value());
        return this;
    }

    public Query not() {
        stringBuilder.append(NOT).append(SPACE);
        return this;
    }

    public Query left() {
        stringBuilder.append(LEFT);
        return this;
    }

    public Query right() {
        stringBuilder.append(RIGHT);
        return this;
    }


    /**
     * 精确查询，多参
     *
     * @param operation   连接符
     * @param searchFiled 字段
     * @param values      语句
     * @return Query
     */
    public Query accurate(Operation operation, String searchFiled, String... values) {
        stringBuilder.append(LEFT).append(searchFiled)
                .append(COLON).append(LEFT)
                .append(Arrays.stream(values).map(x -> MARK + x + MARK).collect(Collectors.joining(operation.value())))
                .append(RIGHT).append(RIGHT);
        return this;
    }

    /**
     * 精确查询，单参
     *
     * @param searchFiled 字段
     * @param values      语句
     * @return Query
     */
    public Query accurate(String searchFiled, String values) {
        stringBuilder.append(searchFiled)
                .append(COLON).append(LEFT).append(MARK).append(values).append(MARK).append(RIGHT);
        return this;
    }

    /**
     * 模糊查询,多参
     *
     * @param operation   连接符
     * @param searchFiled 字段
     * @param values      语句
     * @return Query
     */
    public Query fuzziness(Operation operation, String searchFiled, String... values) {
        stringBuilder.append(LEFT).append(searchFiled)
                .append(COLON).append(LEFT)
                .append(Arrays.stream(values).collect(Collectors.joining(operation.value())))
                .append(RIGHT).append(RIGHT);
        return this;
    }

    /**
     * 模糊查询,单参
     *
     * @param searchFiled 字段
     * @param values      语句
     * @return Query
     */
    public Query fuzziness(String searchFiled, String values) {
        stringBuilder.append(searchFiled)
                .append(COLON).append(LEFT)
                .append(values)
                .append(RIGHT);
        return this;
    }


    /**
     * 转string
     *
     * @return String
     */
    public String build() {
        return stringBuilder.toString();
    }


}
