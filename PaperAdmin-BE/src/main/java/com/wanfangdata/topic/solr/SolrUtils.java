package com.wanfangdata.topic.solr;

import com.wanfangdata.topic.solr.entity.SolrQueryResponseEntity;
import com.wanfangdata.topic.solr.execute.SolrExecute;
import com.wanfangdata.topic.solr.execute.facet.FacetRequest;
import com.wanfangdata.topic.solr.execute.search.SearchRequest;
import com.wanfangdata.topic.solr.execute.update.UpdateRequest;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FieldStatsInfo;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.params.StatsParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description
 * @Authors smj
 * @Date 2023/3/2 15:24
 * @Version
 */

@Component
public class SolrUtils {

    private static SolrExecute solrExecute;

    @Autowired
    public void setSolrExecute(SolrExecute solrExecute) {
        SolrUtils.solrExecute = solrExecute;
    }

    /**
     * solr查询
     *
     * @param searchRequest 查询条件
     * @return 查询结果
     */
    public static Map<String, Object> search(SearchRequest searchRequest) {
        Map<String, Object> dataMap = new HashMap<>();
        QueryResponse queryResponse = solrExecute.queryExecute(searchRequest);
        List<SolrDocument> collect = queryResponse.getResults().parallelStream().collect(Collectors.toList());
        List<LinkedHashMap<String, Object>> mapList = new ArrayList<>();
        for (SolrDocument entries : collect) {
            LinkedHashMap<String, Object> linkedHashMap = new LinkedHashMap<>();
            for (Map.Entry<String, Object> entry : entries) {
                linkedHashMap.put(entry.getKey(), entry.getValue());
            }
            mapList.add(linkedHashMap);
        }
        dataMap.put("data", mapList);
        dataMap.put("total", queryResponse.getResults().getNumFound());
        return dataMap;
    }

    /**
     * count
     *
     * @param searchRequest searchRequest
     * @return Long
     */
    public static Long count(SearchRequest searchRequest) {
        QueryResponse queryResponse = solrExecute.queryExecute(searchRequest);
        return queryResponse.getResults().getNumFound();
    }

    /**
     * solr查询反射
     *
     * @param searchRequest 查询条件
     * @param objectClass   对象
     * @return 结果对象
     */
    public static <T> List<T> beans(SearchRequest searchRequest, Class<T> objectClass) {
        QueryResponse queryResponse = solrExecute.queryExecute(searchRequest);
        return queryResponse.getBeans(objectClass);
    }


    /**
     * solr聚类
     *
     * @param facetRequest 查询条件
     * @return 聚类结果
     */
    public static LinkedHashMap<String, Long> facet(FacetRequest facetRequest) {
        List<FacetField> facetFields = solrExecute.facetExecute(facetRequest).getFacetFields();
        List<FacetField.Count> counts = facetFields.stream().map(FacetField::getValues).findFirst().orElseGet(ArrayList::new);
        return counts.stream().collect(Collectors.toMap(FacetField.Count::getName, FacetField.Count::getCount, (i1, i2) -> i2, LinkedHashMap::new));
    }

    /**
     * solr聚类 得到numFound
     *
     * @param facetRequest 查询条件
     * @return 聚类结果
     */
    public static List<LinkedHashMap<String, Long>> facetMulti(FacetRequest facetRequest) {
        List<LinkedHashMap<String, Long>> r = new ArrayList<>();
        QueryResponse queryResponse = solrExecute.facetExecute(facetRequest);
        List<FacetField> facetFields = queryResponse.getFacetFields();
        List<FacetField.Count> counts = facetFields.stream().map(FacetField::getValues).findFirst().orElseGet(ArrayList::new);

        LinkedHashMap<String, Long> collect = counts.stream().collect(Collectors.toMap(FacetField.Count::getName, FacetField.Count::getCount, (i1, i2) -> i2, LinkedHashMap::new));
        if (collect.size() == 0) {
            return r;
        }
        r.add(collect);
        LinkedHashMap<String, Long> c = new LinkedHashMap<>();
        c.put("numFound", queryResponse.getResults().getNumFound());
        r.add(c);
        return r;
    }


    public static List<Object> sonFacet(FacetRequest facetRequest) {
        List<Object> objects = new ArrayList<>();

        List<String> facetFiled = facetRequest.getFacetFiled();
        facetRequest.removeFacetField(facetFiled);
        facetRequest.facet(facetFiled.get(0));
        LinkedHashMap<String, Long> facetFields = facet(facetRequest);
        if (facetFields.size() == 0) {
            return objects;
        }
        objects.add(facetFields);

        String query = facetRequest.getQuery();
        List<LinkedHashMap<String, Long>> sonResults = new ArrayList<>();
        for (Map.Entry<String, Long> entry : facetFields.entrySet()) {
            String build = new Query().and().fuzziness(facetFiled.get(0), entry.getKey()).build();
            facetRequest.removeFacetField(facetFiled);
            facetRequest.facet(facetFiled.get(1));
            facetRequest.q(query + build);
            LinkedHashMap<String, Long> son = facet(facetRequest);
            sonResults.add(son);
        }
        objects.add(sonResults);
        return objects;
    }

    public static FieldStatsInfo stats(SearchRequest searchRequest) {
        QueryResponse queryResponse = solrExecute.queryExecute(searchRequest);
        return queryResponse.getFieldStatsInfo().get(searchRequest.getSolrQuery().get(StatsParams.STATS_FIELD));
    }

    public static SolrQueryResponseEntity statsMtu(SearchRequest searchRequest) {
        QueryResponse queryResponse = solrExecute.queryExecute(searchRequest);
        SolrQueryResponseEntity entity = new SolrQueryResponseEntity();
        entity.setFieldStatsInfo(queryResponse.getFieldStatsInfo().get(searchRequest.getSolrQuery().get(StatsParams.STATS_FIELD)));
        entity.setSolrDocuments(queryResponse.getResults());
        return entity;
    }

    /**
     * solr添加
     *
     * @param updateRequest 数据
     * @return 是否成功
     */
    public static boolean addAll(UpdateRequest updateRequest) {
        return solrExecute.addAllExecute(updateRequest);
    }

    /**
     * solr添加
     *
     * @param updateRequest 数据
     * @return 是否成功
     */
    public static boolean add(UpdateRequest updateRequest) {
        return solrExecute.addAllExecute(updateRequest);
    }

    public static boolean delete(String id, String collection) {
        return solrExecute.deleteExecute(id, collection);
    }
}
