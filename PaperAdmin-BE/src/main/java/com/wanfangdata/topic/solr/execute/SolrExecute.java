package com.wanfangdata.topic.solr.execute;

import com.wanfangdata.topic.solr.execute.facet.FacetRequest;
import com.wanfangdata.topic.solr.execute.search.SearchRequest;
import com.wanfangdata.topic.solr.execute.update.UpdateRequest;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description 检索
 * @Authors smj
 * @Date 2023/2/17 15:39
 * @Version v1
 */
@Component
public class SolrExecute {

    private final Logger logger = LoggerFactory.getLogger(SolrExecute.class);

    @Autowired
    private SolrClient solrClient;


    /**
     * 查询
     *
     * @param solrSearchRequest solrSearchRequest
     * @return QueryResponse
     */
    public  QueryResponse queryExecute(SearchRequest solrSearchRequest) {
        try {
            QueryResponse query = solrClient.query(solrSearchRequest.getCollection(), solrSearchRequest.getSolrQuery());
            logger.info("     _____                                _     \n" +
                    "    / ____|                              | |    \n" +
                    "   | (___     ___    __ _   _ __    ___  | |__  \n" +
                    "    \\___ \\   / _ \\  / _` | | '__|  / __| | '_ \\ \n" +
                    "    ____) | |  __/ | (_| | | |    | (__  | | | |\n" +
                    "   |_____/   \\___|  \\__,_| |_|     \\___| |_| |_| \n"
                    + query.getResponseHeader().toSolrParams().jsonStr());
            return query;
        } catch (Exception e) {
            logger.error("《----检索异常----》 " + e);
        }
        return null;
    }

    /**
     * 聚类
     *
     * @param facetRequest facetRequest
     * @return QueryResponse
     */
    public QueryResponse facetExecute(FacetRequest facetRequest) {
        try {
            QueryResponse query = solrClient.query(facetRequest.getCollection(), facetRequest.getSolrQuery());
            logger.info("    ______                         _   \n" +
                    "   |  ____|                       | |  \n" +
                    "   | |__      __ _    ___    ___  | |_ \n" +
                    "   |  __|    / _` |  / __|  / _ \\ | __|\n" +
                    "   | |      | (_| | | (__  |  __/ | |_ \n" +
                    "   |_|       \\__,_|  \\___|  \\___|  \\__| \n"
                    + query.getResponseHeader().toString());
            return query;
        } catch (Exception e) {
            logger.error("《----聚类异常----》 " + e);
        }
        return null;
    }


    /**
     * solr 批量添加
     *
     * @param updateRequest  updateRequest
     */
    public boolean addAllExecute(UpdateRequest updateRequest) {
        try {
            UpdateResponse add = solrClient.add(updateRequest.getCollection(), updateRequest.getDocumentList());
        } catch (Exception e) {
            logger.error("solr 批量添加异常：", e);
            return false;
        }
        logger.info("solr 批量添加成功!");
        return true;
    }

    /**
     * solr 批量添加
     *
     * @param updateRequest  updateRequest
     */
    public boolean addExecute(UpdateRequest updateRequest) {
        try {
            UpdateResponse add = solrClient.add(updateRequest.getCollection(), updateRequest.getDocumentList().get(0));
        } catch (Exception e) {
            logger.error("solr 批量添加异常：", e);
            return false;
        }
        logger.info("solr 批量添加成功!");
        return true;
    }
    public boolean deleteExecute(String id,String collection) {
        try {
            UpdateResponse updateResponse = solrClient.deleteByQuery(collection,"Id:"+id);
        } catch (Exception e) {
            logger.error("solr 删除异常：", e);
            return false;
        }
        logger.info("solr 删除成功!");
        return true;
    }

}
