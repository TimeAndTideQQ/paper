package com.wanfangdata.topic.solr;

/**
 * @Description
 * @Authors smj
 * @Date 2023/2/18 14:31
 * @Version
 */
public enum Operation {
    AND(" AND "),
    OR(" OR ");

    private String link;

    Operation(String link) {
        this.link = link;
    }
    public String value(){
        return link;
    }
}
