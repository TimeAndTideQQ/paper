package com.wanfangdata.topic.mapper.sao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wanfangdata.topic.entry.sao.SaoFrontier;
import com.wanfangdata.topic.entry.sao.SaoPretreatment;
import org.apache.ibatis.annotations.Param;

/**
 * @Description
 * @Authors smj
 * @Date 2023/3/3 14:32
 * @Version
 */
public interface ISaoPretreatmentMapper extends BaseMapper<SaoPretreatment> {

    void addSaoPretreatment(@Param("pcid") int pcid, @Param("termEN") String termEN, @Param("resources") String resources);

}
