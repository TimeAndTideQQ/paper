package com.wanfangdata.topic.mapper.treestructure;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wanfangdata.topic.entry.treestructure.NodeRelationEntry;
import com.wanfangdata.topic.entry.treestructure.NodeStructure;
import com.wanfangdata.topic.entry.treestructure.TreeStructureEntry;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @ClassName NodeRelationMapper
 * @Author ZhouZhM
 * @Date 2023/3/16 10:03
 * @Description
 * @Version 1.0
 */
@Mapper
public interface NodeRelationMapper extends BaseMapper<NodeRelationEntry> {

}
