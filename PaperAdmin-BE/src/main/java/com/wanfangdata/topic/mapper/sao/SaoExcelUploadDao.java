package com.wanfangdata.topic.mapper.sao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wanfangdata.topic.entry.sao.SaoExcelUpload;

/**
 * 圣excel上传刀
 *
 * @author wangyou
 * @description
 * @date 2023/05/15
 */
public interface SaoExcelUploadDao extends BaseMapper<SaoExcelUpload> {


}
