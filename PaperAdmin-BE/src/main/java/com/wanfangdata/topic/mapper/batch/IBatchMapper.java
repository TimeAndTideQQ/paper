package com.wanfangdata.topic.mapper.batch;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wanfangdata.topic.entry.batch.BatchEntry;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName IBatchMapper
 * @Author ZhouZhM
 * @Date 2023/3/23 10:08
 * @Description
 * @Version 1.0
 */
@Mapper
public interface IBatchMapper extends BaseMapper<BatchEntry> {
}
