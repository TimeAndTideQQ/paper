package com.wanfangdata.topic.mapper.treestructure;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wanfangdata.topic.entry.treestructure.Tree;
import com.wanfangdata.topic.entry.treestructure.TreeEntry;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @ClassName NodeRelationMapper
 * @Author ZhouZhM
 * @Date 2023/3/16 10:03
 * @Description
 * @Version 1.0
 */
@Mapper
public interface TreeMapper extends BaseMapper<TreeEntry> {

    /**
     * 查询所有树
     */
    List<Tree> selectTrees();
}
