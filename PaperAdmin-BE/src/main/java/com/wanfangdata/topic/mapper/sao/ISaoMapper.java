package com.wanfangdata.topic.mapper.sao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wanfangdata.topic.entry.sao.SaoDataEntry;

/**
 * @Description
 * @Authors smj
 * @Date 2023/3/3 14:32
 * @Version
 */
public interface ISaoMapper extends BaseMapper<SaoDataEntry> {
}
