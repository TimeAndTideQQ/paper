package com.wanfangdata.topic.mapper.batch.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wanfangdata.topic.entry.batch.BatchEntry;
import com.wanfangdata.topic.mapper.batch.IBatchMapper;
import com.wanfangdata.topic.service.batch.IBatchService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName IBatchServiceImpl
 * @Author ZhouZhM
 * @Date 2023/3/23 14:06
 * @Description
 * @Version 1.0
 */
@Service
public class IBatchServiceImpl extends ServiceImpl<IBatchMapper, BatchEntry> implements IBatchService {
}
