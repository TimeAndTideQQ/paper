package com.wanfangdata.topic.mapper.treestructure;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wanfangdata.topic.entry.treestructure.CrossRelationRequest;
import com.wanfangdata.topic.entry.treestructure.NodeRelationEntry;
import com.wanfangdata.topic.entry.treestructure.NodeStructure;
import com.wanfangdata.topic.entry.treestructure.TreeStructureEntry;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @ClassName TreeStructureMapper
 * @Author ZhouZhM
 * @Date 2023/3/8 10:40 
 * @Description 
 * @Version 1.0
 */
@Mapper
public interface TreeStructureMapper extends BaseMapper<TreeStructureEntry> {

    /**
     * 查询树
     */
    List<NodeStructure> selectStructureEntry(int treeId);

    /**
     * 检查当前sao数据是否在当前树种存在
     * @param treeId 树Id
     * @param saoId saoId
     * @return
     */
    int isExit(int treeId,int saoId);

    /**
     * 删除节点
     * @param id 节点id
     * @return 删除的节点数
     */
    void deleteStructureEntry(int id,int treeId);

    /**
     * 修改节点交叉关系
     * @param treeId 树Id
     * @param id 节点Id
     * @param crossRelation 交叉关系
     * @return
     */
    int updateCrossRelation(int treeId,int id,String crossRelation);

}
