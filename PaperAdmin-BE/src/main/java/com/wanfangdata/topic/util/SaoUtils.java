package com.wanfangdata.topic.util;

import opennlp.tools.chunker.ChunkerME;
import opennlp.tools.chunker.ChunkerModel;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author 10062
 * @date 2023/04/05 14:47
 **/
public class SaoUtils {

    private static final Logger logger = LoggerFactory.getLogger(SaoUtils.class);
    private static SentenceDetectorME sentenceDetector = null;
    private static TokenizerME tokenizer = null;
    private static POSTaggerME posTagger = null;
    private static ChunkerME chunker = null;

    private static List<String> STARTS_WITH_NO = new ArrayList<>();
    private static List<String> STARTS_WITH_YES = new ArrayList<>();

    static {
        STARTS_WITH_NO.add("abandon");
        STARTS_WITH_NO.add("ability");
        STARTS_WITH_NO.add("accelerate");
        STARTS_WITH_NO.add("access");
        STARTS_WITH_NO.add("accord");
        STARTS_WITH_NO.add("account");
        STARTS_WITH_NO.add("achieve");
        STARTS_WITH_NO.add("acquire");
        STARTS_WITH_NO.add("action");
        STARTS_WITH_NO.add("adapt");
        STARTS_WITH_NO.add("adjust");
        STARTS_WITH_NO.add("affect");
        STARTS_WITH_NO.add("allow");
        STARTS_WITH_NO.add("arrange");
        STARTS_WITH_NO.add("attention");
        STARTS_WITH_NO.add("avoid");
        STARTS_WITH_NO.add("begin");
        STARTS_WITH_NO.add("build");
        STARTS_WITH_NO.add("call");
        STARTS_WITH_NO.add("carry");
        STARTS_WITH_NO.add("change");
        STARTS_WITH_NO.add("collect");
        STARTS_WITH_NO.add("compare");
        STARTS_WITH_NO.add("comprise");
        STARTS_WITH_NO.add("consider");
        STARTS_WITH_NO.add("contain");
        STARTS_WITH_NO.add("correct");
        STARTS_WITH_NO.add("correspond");
        STARTS_WITH_NO.add("create");
        STARTS_WITH_NO.add("determine");
        STARTS_WITH_NO.add("device");
        STARTS_WITH_NO.add("download");
        STARTS_WITH_NO.add("eliminate");
        STARTS_WITH_NO.add("end");
        STARTS_WITH_NO.add("ensure");
        STARTS_WITH_NO.add("establish");
        STARTS_WITH_NO.add("estimation");
        STARTS_WITH_NO.add("evaluate");
        STARTS_WITH_NO.add("exist");
        STARTS_WITH_NO.add("expand");
        STARTS_WITH_NO.add("fix");
        STARTS_WITH_NO.add("form");
        STARTS_WITH_NO.add("get");
        STARTS_WITH_NO.add("give");
        STARTS_WITH_NO.add("help");
        STARTS_WITH_NO.add("improve");
        STARTS_WITH_NO.add("include");
        STARTS_WITH_NO.add("increase");
        STARTS_WITH_NO.add("influence");
        STARTS_WITH_NO.add("input");
        STARTS_WITH_NO.add("introduce");
        STARTS_WITH_NO.add("involve");
        STARTS_WITH_NO.add("keep");
        STARTS_WITH_NO.add("know");
        STARTS_WITH_NO.add("label");
        STARTS_WITH_NO.add("load");
        STARTS_WITH_NO.add("make");
        STARTS_WITH_NO.add("minimize");
        STARTS_WITH_NO.add("need");
        STARTS_WITH_NO.add("object");
        STARTS_WITH_NO.add("obtain");
        STARTS_WITH_NO.add("occur");
        STARTS_WITH_NO.add("operation");
        STARTS_WITH_NO.add("output");
        STARTS_WITH_NO.add("pair");
        STARTS_WITH_NO.add("part");
        STARTS_WITH_NO.add("perform");
        STARTS_WITH_NO.add("problem");
        STARTS_WITH_NO.add("provide");
        STARTS_WITH_NO.add("reach");
        STARTS_WITH_NO.add("realize");
        STARTS_WITH_NO.add("reduce");
        STARTS_WITH_NO.add("relate");
        STARTS_WITH_NO.add("remove");
        STARTS_WITH_NO.add("require");
        STARTS_WITH_NO.add("save");
        STARTS_WITH_NO.add("select");
        STARTS_WITH_NO.add("send");
        STARTS_WITH_NO.add("set");
        STARTS_WITH_NO.add("show");
        STARTS_WITH_NO.add("solve");
        STARTS_WITH_NO.add("take");
        STARTS_WITH_NO.add("uncertainty");
        STARTS_WITH_NO.add("use");
        STARTS_WITH_NO.add("utilize");
        STARTS_WITH_YES.add("network");
        STARTS_WITH_YES.add("identification");
    }

    static {
        try {
            sentenceDetector = new SentenceDetectorME(new SentenceModel(new ClassPathResource("en-sent.bin").getInputStream()));
            tokenizer = new TokenizerME(new TokenizerModel(new ClassPathResource("en-token.bin").getInputStream()));
            posTagger = new POSTaggerME(new POSModel(new ClassPathResource("en-pos-maxent.bin").getInputStream()));
            chunker = new ChunkerME(new ChunkerModel(new ClassPathResource("en-chunker.bin").getInputStream()));
        } catch (IOException e) {
            logger.error("加载sao词汇模板失败！", e);
        }
    }

    public static Set<String> getSAo(String... sentenceList) {
        Set<String> triples = new HashSet<>();
        for (String sentence : sentenceList) {
            logger.info("sao分析：{}",sentence);
            String[] sentences = sentenceDetector.sentDetect(sentence);
            for (String sent : sentences) {
                // 分词
                String[] tokens = tokenizer.tokenize(sent);
                // 词性标注
                if (tokens != null && tokens.length > 0) {
                    String[] tags = posTagger.tag(tokens);
                    // 分块标注
                    if (tags != null && tags.length > 0) {
                        String[] chunks = chunker.chunk(tokens, tags);
                        if (chunks != null && chunks.length > 0) {
                            // 提取三元组
                            triples.addAll(produceSao(tokens, chunks));
                        }
                    }
                }
            }
        }
        return triples;
    }


    /**
     * 提取三元组
     *
     * @param tokens 分词数组
     * @param chunks 分块数组
     * @return 提取的三元组列表
     */
    private static Set<String> produceSao(String[] tokens, String[] chunks) {
        Set<String> triples = new HashSet<>();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < chunks.length; i++) {
            String chunk = chunks[i];
            if (chunk.startsWith("B-NP")) {
                String tokensStr = tokens[i];
                if (containsNumbers(tokensStr)){
                    sb.append(removeSpecialWord(tokensStr)).append(" ");
                }
            } else if (chunk.startsWith("I-NP")) {
                String tokensStr = tokens[i];
                if (containsNumbers(tokensStr)){
                    sb.append(removeSpecialWord(tokensStr)).append(" ");
                }
            } else {
                String s = sb.toString();
                String[] split = s.split(" ");
                if (split.length >=2 && split.length <= 3 && s.length() > 0) {
                    if (startsWithStr(s)){
                        triples.add(s);
                    }
                }
            }
        }
        return triples;
    }


    public static boolean startsWithStr(String word){
        for (String startsWithYe : STARTS_WITH_YES) {
            if (word.endsWith(startsWithYe)){
                return true;
            }
        }
        for (String startsWithNO : STARTS_WITH_NO) {
            if (word.startsWith(startsWithNO)){
                return false;
            }
        }
        return true;
    }


    public static boolean containsNumbers(String word){
        if (word == null || word.length() < 6 || word.length() > 20){
            return false;
        }
        for (int i = 0; i < 10; i++) {
            if (word.contains(""+i)){
                return false;
            }
        }
        return true;
    }

    public static String removeSpecialWord(String word) {
        String withWord = word.replaceAll("\"", "")
                .replaceAll("\\\\", "")
                .replaceAll("“", "")
                .replaceAll("”", "")
                .replaceAll("’", "")
                .replaceAll("‘", "")
                .replaceAll("'", "")
                .replaceAll("\\[", "")
                .replaceAll("]", "")
                .replaceAll("\\{", "")
                .replaceAll("}", "")
                .replaceAll("\\(", "")
                .replaceAll("\\)", "")
                .replaceAll("\\?|？", "")
                .replaceAll("【", "")
                .replaceAll("】", "")
                .replaceAll("\\{", "")
                .replaceAll("}", "")
                .replaceAll("（", "")
                .replaceAll("：", "")
                .replaceAll("\\.", "")
                .replaceAll(":", "")
                .replaceAll("/", "")
                .replaceAll("\\.", "")
                .replaceAll("-", "")
                .replaceAll(" ", "")
                .replaceAll("）", "");
        return withWord;
    }



}
