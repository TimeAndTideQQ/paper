package com.wanfangdata.topic.util;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

/**
 * excel工具类
 *
 * @author Administrator
 * @date 2023/03/01 13:41
 **/
public class ExcelUtils {


    /**
     * 从指定的HSSFCell对象中获取列的值
     *
     * @return
     */
    public static String getCellValueForStr(Cell cell) {
        if (cell==null || cell.toString().trim().equals("")) {
            return "";
        }
        String value = "";
        switch (cell.getCellType()) {
            //字符串类型
            case STRING:
                value = StringUtil.nvl(cell.getStringCellValue());
                break;
            //布尔类型
            case BOOLEAN:
                value = String.valueOf(cell.getBooleanCellValue());
                break;
            //数值类型
            case NUMERIC:
                //判断日期类型
                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    value = simpleDateFormat.format(cell.getDateCellValue());
                } else {
                    value = new DecimalFormat("#.######").format(cell.getNumericCellValue());
                }
                break;
            default:
                value = "";
                break;
        }
        return value;
    }

}
