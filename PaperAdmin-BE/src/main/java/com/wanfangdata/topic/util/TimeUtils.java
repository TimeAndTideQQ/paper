package com.wanfangdata.topic.util;

import com.wanfangdata.topic.solr.Operation;
import com.wanfangdata.topic.solr.Query;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description 时间工具栏
 * @Authors smj
 * @Date 2023/2/19 22:04
 * @Version
 */
public class TimeUtils {


    /**
     * 距今年份
     *
     * @param diff 距离
     * @return 年份集合
     */
    private static List<String> diffNowTimeYears(int diff) {
        LocalDateTime now = LocalDateTime.now();
        int oldYear = now.minusYears(diff).getYear();
        int nowYear = now.getYear();
        List<String> yearList = new ArrayList<>(diff);
        for (int i = oldYear; i < nowYear; i++) {
            yearList.add(String.valueOf(i + 1));
        }
        return yearList;
    }

    public static String getDiffYearFq(String fqFiled, int diff) {
        List<String> yearList = diffNowTimeYears(diff);
        Query query = new Query();
        query.fuzziness(Operation.OR, fqFiled, yearList.toArray(new String[0]));
        return query.build();
    }

}
