package com.wanfangdata.topic.util;


import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.WriteException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CRITIC {

    Scanner input = new Scanner(System.in);

    //矩阵每列最大值
    public double[] Max(double[][] m) {
        double max[] = new double[m[0].length];
        for (int j = 0; j < m[0].length; j++) {
            max[j] = m[0][j];
            for (int i = 0; i < m.length; i++) {
                if (m[i][j] >= max[j]) {
                    max[j] = m[i][j];
                }
            }
        }
        return max;
    }

    //矩阵每列最小值
    public double[] Min(double[][] m) {
        double min[] = new double[m[0].length];
        for (int j = 0; j < m[0].length; j++) {
            min[j] = m[0][j];
            for (int i = 0; i < m.length; i++) {
                if (m[i][j] <= min[j]) {
                    min[j] = m[i][j];
                }
            }
        }
        return min;
    }

    //矩阵每列平均值
    public double[] Average(double[][] m) {
        double avr[] = new double[m[0].length];
        for (int j = 0; j < m[0].length; j++) {
            double sum = 0;
            for (int i = 0; i < m.length; i++) {
                sum += m[i][j];
            }
            avr[j] = sum / m.length;
        }
        return avr;
    }

    //输出二维矩阵
    public List<List<Double>> matrixoutput(double[][] x) {
        List<List<Double>> out = new ArrayList<>();
        for (double[] doubles : x) {
            List<Double> list = new ArrayList<>();
            for (int j = 0; j < x[0].length; j++) {
                System.out.print(doubles[j] + "   ");
                list.add(doubles[j]);
            }
            System.out.println();
            out.add(list);
        }
        return out;
    }

    //输出一维矩阵
    public void matrixoutput1(double[] x) {
        for (int i = 0; i < x.length; i++) {
            System.out.print(String.format("%.8f\t", x[i]));
        }
        System.out.println();
    }

    /**
     * 从Excel表格读取数据，列为评价指标行为待评价样本
     * <p>
     * 假设有m个待评价样本，n个评价指标
     *
     * @param filepath 表格存储位置
     * @return componentMartix 返回原始矩阵
     */
    public double[][] read(String filepath) throws IOException, BiffException, WriteException {
        //创建输入流
        InputStream stream = new FileInputStream(filepath);
        //获取Excel文件对象
        Workbook rwb = Workbook.getWorkbook(stream);
        //获取文件的指定工作表 默认的第一个
        Sheet sheet = rwb.getSheet("Sheet1");
        int rows = sheet.getRows();
        int cols = sheet.getColumns();
        double[][] componentMatrix = new double[rows][cols];//原始矩阵
        //row为行
        for (int i = 0; i < sheet.getRows(); i++) {
            for (int j = 0; j < sheet.getColumns(); j++) {
                String[] str = new String[sheet.getColumns()];
                Cell cell = null;
                cell = sheet.getCell(j, i);
                str[j] = cell.getContents();
                componentMatrix[i][j] = Double.valueOf(str[j]);
            }
        }
        return componentMatrix;//返回原始矩阵
    }

    /**
     * 数据标准化处理，消除量纲影响
     *
     * @param componentMatrix 输入原始矩阵
     * @return normalizedMatrix 返回标准化后的矩阵
     */
    public double[][] normalized(double[][] componentMatrix, List<Integer> neg) {
        double[][] normalizedMatrix = new double[componentMatrix.length][componentMatrix[0].length];
        double[] max = Max(componentMatrix);
        double[] min = Min(componentMatrix);

        for (int i = 0; i < componentMatrix.length; i++) {
            for (int j = 0; j < componentMatrix[0].length; j++) {
                normalizedMatrix[i][j] = (componentMatrix[i][j] - min[j]) / (max[j] - min[j]);
            }
        }
        if (neg != null && !neg.isEmpty()) {
            System.out.println("有逆向指标？（越小越优型指标）所在列为：" + neg.toString());
            for (int i = 0; i < componentMatrix.length; i++) {
                for (Integer integer : neg) {
                    normalizedMatrix[i][integer] =
                            (max[integer] - componentMatrix[i][integer]) / (max[integer] - min[integer]);
                }
            }
        }
        return normalizedMatrix;
    }


    /**
     * 计算相关系数矩阵
     *
     * @param normalizedMatrix 标准化后数据
     * @return pearson 皮尔逊相关系数矩阵
     */

    public double[][] correlation(double[][] normalizedMatrix) {
        double[][] pearson = new double[normalizedMatrix[0].length][normalizedMatrix[0].length];//皮尔逊相关系数矩阵
        double[] avr = Average(normalizedMatrix);//每列平均值

        double[] s = new double[normalizedMatrix[0].length];
        for (int j = 0; j < normalizedMatrix[0].length; j++) {
            double sum = 0;
            for (int i = 0; i < normalizedMatrix.length; i++) {
                sum += Math.pow(normalizedMatrix[i][j] - avr[j], 2);
            }
            s[j] = Math.sqrt(sum / (normalizedMatrix[0].length - 1));
        }

        double[][] cxy = new double[normalizedMatrix[0].length][normalizedMatrix[0].length];
        for (int j = 0; j < normalizedMatrix[0].length; j++) {
            for (int k = 0; k < normalizedMatrix[0].length; k++) {
                double sum = 0;
                for (int i = 0; i < normalizedMatrix.length; i++) {
                    sum += (normalizedMatrix[i][j] - avr[j]) * (normalizedMatrix[i][k] - avr[k]);
                }
                cxy[j][k] = sum / (pearson.length - 1);
                pearson[j][k] = cxy[j][k] / (s[j] * s[k]);
            }
        }
        return pearson;
    }

    /**
     * 计算每个指标的信息承载量
     *
     * @param normalizedMatrix 标准化后的矩阵
     * @param pearson          皮尔逊相关系数矩阵
     * @return informationVolume 每个指标的信息承载量
     */
    public double[] information(double[][] normalizedMatrix, double[][] pearson) {
        double[] informationVolume = new double[normalizedMatrix[0].length];
        double[] avr = Average(normalizedMatrix);//每列平均值

        //计算对比强度（标准差）
        double[] s = new double[normalizedMatrix[0].length];
        for (int j = 0; j < normalizedMatrix[0].length; j++) {
            double sum = 0;
            for (int i = 0; i < normalizedMatrix.length; i++) {
                sum += Math.pow(normalizedMatrix[i][j] - avr[j], 2);
            }
            s[j] = Math.sqrt(sum / (normalizedMatrix[0].length - 1));
        }

        //计算冲突性
        double[] r = new double[normalizedMatrix[0].length];
        for (int j = 0; j < normalizedMatrix[0].length; j++) {
            double sum = 0;
            for (int i = 0; i < normalizedMatrix[0].length; i++) {
                sum += 1 - pearson[i][j];
            }
            r[j] = sum;
        }

        //计算信息量
        for (int j = 0; j < normalizedMatrix[0].length; j++) {
            informationVolume[j] = s[j] * r[j];
        }
        return informationVolume;
    }

    /**
     * 计算权重
     *
     * @param informationVolume 每个指标的信息量
     * @return weight 返回每个指标的权重
     */
    public double[] weight(double[] informationVolume) {
        double[] weight = new double[informationVolume.length];
        double sum = 0;
        for (int i = 0; i < informationVolume.length; i++) {
            sum += informationVolume[i];
        }
        for (int i = 0; i < informationVolume.length; i++) {
            weight[i] = informationVolume[i] / sum;
        }

        return weight;
    }

    /**
     * 改进算法
     *
     * @param normalizedMatrix 标准化后的矩阵
     * @param pearson          皮尔逊相关系数矩阵
     * @param ewm              熵权法求得的指标熵值
     * @return
     */
    public double[] weight1(double[][] normalizedMatrix, double[][] pearson, double[] ewm) {
        double[] informationVolume = new double[normalizedMatrix[0].length];
        double[] avr = Average(normalizedMatrix);//每列平均值
        double[] weight = new double[normalizedMatrix[0].length];
        double[] pear = new double[normalizedMatrix[0].length];

        //计算对比强度（标准差）
        double[] s = new double[normalizedMatrix[0].length];
        for (int j = 0; j < normalizedMatrix[0].length; j++) {
            double sum = 0;
            for (int i = 0; i < normalizedMatrix.length; i++) {
                sum += Math.pow(normalizedMatrix[i][j] - avr[j], 2);
            }
            s[j] = Math.sqrt(sum / (normalizedMatrix[0].length - 1));
        }

        double total = 0;
        for (int j = 0; j < normalizedMatrix[0].length; j++) {
            for (int i = 0; i < normalizedMatrix[0].length; i++) {
                pear[j] += Math.abs(pearson[i][j]);
            }
            total += ewm[j] + s[j];
        }

        for (int j = 0; j < normalizedMatrix[0].length; j++) {
            informationVolume[j] = ((ewm[j] + s[j]) * pear[j]) / (total + pear[j]);
        }

        double sum = 0;
        for (int i = 0; i < informationVolume.length; i++) {
            sum += informationVolume[i];
        }
        for (int i = 0; i < informationVolume.length; i++) {
            weight[i] = informationVolume[i] / sum;
        }

        return weight;
    }
}