package com.wanfangdata.topic.constant;

/**
 * @Description solr库名
 * @Authors smj
 * @Date 2023/2/17 15:25
 * @Version
 */
public class SolrCollection {

    /**
     * 中文期刊库
     */
    public static final String TOPIC_PERIODICAL_CHI = "TopicPeriodical";

    /**
     * 论文库
     */
    public static final String HANDLE_THESIS = "Handle_Thesis";

    /**
     * 专利库
     */
    public static final String HANDLE_PATENT = "Handle_Patent";

    /**
     * 项目库
     */
    public static final String HANDLE_PROJECT = "Handle_Project";


    /**
     * 别名 项目库-专利库-论文库
     */
    public static final String HANDLE_PAPER = "Handle_Paper";

}
