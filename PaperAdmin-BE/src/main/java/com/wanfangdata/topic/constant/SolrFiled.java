package com.wanfangdata.topic.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description solr字段
 * @Authors smj
 * @Date 2023/2/17 15:25
 * @Version
 */
public class SolrFiled {

    public static final String RETURN_FILED = "Id,Keywords,Title,CitedCnt";

    public static final String EDU_CODE = "EducationCode";
    public static final String KEYWORDS = "Keywords";





    /**
     * 论文库字段 集合
     */
    public static final List<String> THESIS_FILEDS = new ArrayList<>();
    /**
     * 专利库字段 集合
     */
    public static final List<String> PATENT_FILEDS = new ArrayList<>();
    /**
     * 项目库字段 集合
     */
    public static final List<String> PROJECT_FILEDS = new ArrayList<>();

    static {
        thesisFiledsInit();
        patentFiledsInit();
        projectFiledsInit();
    }


    /**
     * 共有字段
     */
    public static final String ID = "Id";
    public static final String TITLE = "Title";
    public static final String TITLE_ABSTRACT = "TitleAbstract";
    public static final String ABSTRACT = "Abstract";
    public static final String TYPE = "Type";
    public static final String BATCH = "Batch";
    /**
     * 专利
     */
    public static final String PATENT = "Patent";
    /**
     * 专利
     */
    public static final String PROJECT = "Project";
    /**
     * 论文
     */
    public static final String THESIS = "Thesis";


    /**
     * 论文库 字段
     */
    public static final String SOURCE = "Source";
    public static final String ABBREVIATED_TITLE = "AbbreviatedTitle";
    public static final String ISSUE_DATE = "IssueDate";
    public static final String CONFERENCE_NAME = "ConferenceName";
    public static final String CONFERENCE_DATE = "ConferenceDate";
    public static final String CONFERENCE_LOCATION = "ConferenceLocation";
    public static final String SPONSOR = "Sponsor";
    public static final String PUBLISHER = "Publisher";
    public static final String MONOGRAPH_TITLE = "MonographTitle";
    public static final String PUBLICATION_YEAR = "PublicationYear";
    public static final String LANGUAGE = "Language";
    public static final String REFERENCES = "References";
    public static final String MAIN_HEADING = "MainHeading";
    public static final String AFFILIATION = "Affiliation";
    public static final String ARTICLE_NUMBER = "ArticleNumber";
    public static final String ACCESSION_NUMBER = "AccessionNumber";
    public static final String CORRESPONDING_AUTHOR = "CorrespondingAuthor";
    public static final String PAGES = "Pages";
    public static final String VOLUME = "Volume";
    public static final String ISSUE = "Issue";
    public static final String VOLUME_TITLE = "VolumeTitle";
    public static final String PART_NUMBER = "PartNumber";
    public static final String ISSN = "ISSN";
    public static final String EISSN = "EISSN";
    public static final String ISBN = "ISBN";
    public static final String CONFERENCE_CODE = "ConferenceCode";
    public static final String CODEN = "CODEN";
    public static final String DOCUMENT_TYPE = "DocumentType";
    public static final String DATABASE = "Database";
    public static final String DATA_PROVIDER = "DataProvider";
    public static final String FUNDING_DETAILS = "FundingDetails";
    public static final String FUNDING_TEXT = "FundingText";
    public static final String OPEN_ACCESS_TYPE = "OpenAccessType";
    public static final String AUTHOR = "Author";
    public static final String CONTROLLED = "Controlled";
    public static final String UNCONTROLLED = "Uncontrolled";
    public static final String CLASSIFICATION_CODE = "ClassificationCode";
    public static final String CITED_COUNT = "CitedCount";


    /**
     * 项目库 字段
     */
    public static final String AWARDED_AMOUNT_TO_DATE = "AwardedAmountToDate";
    public static final String START_YEAR = "StartYear";
    public static final String END_YEAR = "EndYear";
    public static final String YEAR = "Year";
    public static final String NSFORGANIZATION = "NSFOrganization";
    public static final String PROGRAM = "Program";
    public static final String START_DATE = "StartDate";
    public static final String LAST_AMENDMENT_DATE = "LastAmendmentDate";
    public static final String PRINCIPAL_INVESTIGATOR = "PrincipalInvestigator";
    public static final String ORGANIZATION = "Organization";
    public static final String AWARD_INSTRUMENT = "AwardInstrument";
    public static final String PROGRAM_MANAGER = "ProgramManager";
    public static final String END_DATE = "EndDate";
    public static final String PIEMAIL_ADDRESS = "PIEmailAddress";
    public static final String NSFDIRECTORATE = "NSFDirectorate";
    public static final String PROGRAM_ELEMENT_CODE = "ProgramElementCode";
    public static final String PROGRAM_REFERENCE_CODE = "ProgramReferenceCode";
    public static final String CYCLE_YEAR = "cycleYear";
    public static final String ARRAAMOUNT = "ARRAAmount";

    /**
     * 专利库 字段
     */
    public static final String TF = "TF";
    public static final String EA = "EA";
    public static final String AE = "AE";
    public static final String GA = "GA";
    public static final String PD = "PD";
    public static final String AD = "AD";
    public static final String FD = "FD";
    public static final String DS = "DS";
    public static final String FS = "FS";
    public static final String MN = "MN";
    public static final String RI = "RI";
    public static final String AU = "AU";
    public static final String PN = "PN";
    public static final String DC = "DC";
    public static final String MC = "MC";
    public static final String IP = "IP";
    public static final String PI = "PI";
    public static final String CP = "CP";
    public static final String CR = "CR";
    public static final String DN = "DN";
    public static final String CI = "CI";
    public static final String PY = "PY";
    public static final String CC = "CC";

    public static void thesisFiledsInit() {
        THESIS_FILEDS.add(ID);
        THESIS_FILEDS.add(TITLE);
        THESIS_FILEDS.add(ABSTRACT);
        THESIS_FILEDS.add(SOURCE);
        THESIS_FILEDS.add(ABBREVIATED_TITLE);
        THESIS_FILEDS.add(ISSUE_DATE);
        THESIS_FILEDS.add(CONFERENCE_NAME);
        THESIS_FILEDS.add(CONFERENCE_DATE);
        THESIS_FILEDS.add(CONFERENCE_LOCATION);
        THESIS_FILEDS.add(SPONSOR);
        THESIS_FILEDS.add(PUBLISHER);
        THESIS_FILEDS.add(MONOGRAPH_TITLE);
        THESIS_FILEDS.add(PUBLICATION_YEAR);
        THESIS_FILEDS.add(LANGUAGE);
        THESIS_FILEDS.add(REFERENCES);
        THESIS_FILEDS.add(MAIN_HEADING);
        THESIS_FILEDS.add(AFFILIATION);
        THESIS_FILEDS.add(ARTICLE_NUMBER);
        THESIS_FILEDS.add(ACCESSION_NUMBER);
        THESIS_FILEDS.add(CORRESPONDING_AUTHOR);
        THESIS_FILEDS.add(PAGES);
        THESIS_FILEDS.add(VOLUME);
        THESIS_FILEDS.add(ISSUE);
        THESIS_FILEDS.add(VOLUME_TITLE);
        THESIS_FILEDS.add(PART_NUMBER);
        THESIS_FILEDS.add(ISSN);
        THESIS_FILEDS.add(EISSN);
        THESIS_FILEDS.add(ISBN);
        THESIS_FILEDS.add(CONFERENCE_CODE);
        THESIS_FILEDS.add(CODEN);
        THESIS_FILEDS.add(DOCUMENT_TYPE);
        THESIS_FILEDS.add(DATABASE);
        THESIS_FILEDS.add(DATA_PROVIDER);
        THESIS_FILEDS.add(FUNDING_DETAILS);
        THESIS_FILEDS.add(FUNDING_TEXT);
        THESIS_FILEDS.add(OPEN_ACCESS_TYPE);
        THESIS_FILEDS.add(AUTHOR);
        THESIS_FILEDS.add(CONTROLLED);
        THESIS_FILEDS.add(UNCONTROLLED);
        THESIS_FILEDS.add(CLASSIFICATION_CODE);
        THESIS_FILEDS.add(CITED_COUNT);
        THESIS_FILEDS.add(BATCH);
    }

    public static void patentFiledsInit() {
        PATENT_FILEDS.add(ID);
        PATENT_FILEDS.add(TITLE);
        PATENT_FILEDS.add(ABSTRACT);
        PATENT_FILEDS.add(TF);
        PATENT_FILEDS.add(EA);
        PATENT_FILEDS.add(AE);
        PATENT_FILEDS.add(GA);
        PATENT_FILEDS.add(PD);
        PATENT_FILEDS.add(AD);
        PATENT_FILEDS.add(FD);
        PATENT_FILEDS.add(DS);
        PATENT_FILEDS.add(FS);
        PATENT_FILEDS.add(MN);
        PATENT_FILEDS.add(RI);
        PATENT_FILEDS.add(AU);
        PATENT_FILEDS.add(PN);
        PATENT_FILEDS.add(DC);
        PATENT_FILEDS.add(MC);
        PATENT_FILEDS.add(IP);
        PATENT_FILEDS.add(PI);
        PATENT_FILEDS.add(CP);
        PATENT_FILEDS.add(CR);
        PATENT_FILEDS.add(DN);
        PATENT_FILEDS.add(CI);
        PATENT_FILEDS.add(PY);
        PATENT_FILEDS.add(CC);
        PATENT_FILEDS.add(BATCH);
    }

    public static void projectFiledsInit() {
        PROJECT_FILEDS.add(ID);
        PROJECT_FILEDS.add(TITLE);
        PROJECT_FILEDS.add(ABSTRACT);
        PROJECT_FILEDS.add(AWARDED_AMOUNT_TO_DATE);
        PROJECT_FILEDS.add(START_YEAR);
        PROJECT_FILEDS.add(END_YEAR);
        PROJECT_FILEDS.add(YEAR);
        PROJECT_FILEDS.add(NSFORGANIZATION);
        PROJECT_FILEDS.add(PROGRAM);
        PROJECT_FILEDS.add(START_DATE);
        PROJECT_FILEDS.add(LAST_AMENDMENT_DATE);
        PROJECT_FILEDS.add(PRINCIPAL_INVESTIGATOR);
        PROJECT_FILEDS.add(ORGANIZATION);
        PROJECT_FILEDS.add(AWARD_INSTRUMENT);
        PROJECT_FILEDS.add(PROGRAM_MANAGER);
        PROJECT_FILEDS.add(END_DATE);
        PROJECT_FILEDS.add(PIEMAIL_ADDRESS);
        PROJECT_FILEDS.add(NSFDIRECTORATE);
        PROJECT_FILEDS.add(PROGRAM_ELEMENT_CODE);
        PROJECT_FILEDS.add(PROGRAM_REFERENCE_CODE);
        PROJECT_FILEDS.add(CYCLE_YEAR);
        PROJECT_FILEDS.add(ARRAAMOUNT);
        PROJECT_FILEDS.add(BATCH);
    }


}
