package com.wanfangdata.topic.resultenum;

/**
 * @Description 枚举返回类型
 * @Authors smj
 * @Date 2023/2/18 11:42
 * @Version
 */
public enum EnumResultStates {

    SUCCESS(true,200,"成功"),
    UNKNOWN_REASON(false, -101, "未知错误"),
    BAD_SQL_GRAMMAR(false, -102, "sql语法错误"),
    JSON_PARSE_ERROR(false, -103, "json解析异常"),
    PARAM_ERROR(false, -104, "参数不正确"),
    FILE_UPLOAD_ERROR(false, -105, "文件上传错误"),
    EXCEL_DATA_IMPORT_ERROR(false, -106, "Excel数据导入错误"),
    NULL_POINT_ERROR(false, -107, "空指针异常"),
    ARRAY_INDEX_OUT_OF_BOUNDS_ERROR(false, -108, "数组下标越界"),
    ILLEGAL_ARGUMENT_EXCEPTION_ERROR(false, -109, "传递了一个不合法或不正确的参数"),
    ILLEGAL_FILETYPE_EXCEPTION_ERROR(false, -109, "文件不是excel文件！！"),
    SERVER_ERROR(false, 500, "服务器错误!"),
    NODE_RELATION_ERROR(false, 403, "节点关系正在使用,无法删除!"),
    SERVER_CANNOT(false, 54004 , "本月翻译已用完,请下个月在使用 !"),
    COORDINATE_ERROR(false, 403, "更新坐标失败!");

    EnumResultStates(boolean state, int code, String msg) {
        this.state = state;
        this.code = code;
        this.msg = msg;
    }

    private boolean state;
    private int code;
    private String msg;

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
