package com.wanfangdata.topic.resultenum;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description
 * @Authors smj
 * @Date 2023/2/18 11:45
 * @Version
 */
@Data
public class Result implements Serializable {


    private boolean state;
    private int code;
    private String msg;
    private Object data;

    public Result() {
    }

    public Result(boolean state, int code, String msg, Object data) {
        this.state = state;
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    /**
     * 成功
     *
     * @param data 成功数据
     * @return R
     */
    public static Result success(Object data) {
        return new Result()
                .setState(EnumResultStates.SUCCESS.isState())
                .setCode(EnumResultStates.SUCCESS.getCode())
                .setMsg(EnumResultStates.SUCCESS.getMsg())
                .setData(data);
    }

    /**
     * 成功
     *
     * @return R
     */
    public static Result success() {
        return new Result()
                .setState(EnumResultStates.SUCCESS.isState())
                .setCode(EnumResultStates.SUCCESS.getCode())
                .setMsg(EnumResultStates.SUCCESS.getMsg());
    }

    /**
     * 自定义异常
     *
     * @param enumResultStates 枚举
     * @return R
     */
    public static Result error(EnumResultStates enumResultStates) {
        return new Result()
                .setState(enumResultStates.isState())
                .setCode(enumResultStates.getCode())
                .setMsg(enumResultStates.getMsg());
    }

    /**
     * 未知异常
     *
     * @return R
     */
    public static Result error() {
        return new Result()
                .setState(EnumResultStates.UNKNOWN_REASON.isState())
                .setCode(EnumResultStates.UNKNOWN_REASON.getCode())
                .setMsg(EnumResultStates.UNKNOWN_REASON.getMsg());
    }


    public boolean isState() {
        return state;
    }

    public Result setState(boolean state) {
        this.state = state;
        return this;
    }

    public int getCode() {
        return code;
    }

    public Result setCode(int code) {
        this.code = code;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public Result setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public Object getData() {
        return data;
    }

    public Result setData(Object data) {
        this.data = data;
        return this;
    }
}
