package com.wanfangdata.topic.dao.resource.impl;

import com.wanfangdata.topic.constant.SolrCollection;
import com.wanfangdata.topic.constant.SolrFiled;
import com.wanfangdata.topic.dao.resource.ResourceUploadDao;
import com.wanfangdata.topic.solr.SolrUtils;
import com.wanfangdata.topic.solr.execute.update.UpdateRequest;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 项目excel数据处理
 *
 * @author Administrator
 * @date 2023/03/01 16:25
 **/
@Repository("Project")
public class ProjectResourceUploadDaoImpl implements ResourceUploadDao {



    @Override
    public boolean createDocument(List<Map<String, String>> rowMapList) {
        List<SolrInputDocument> documentList = new ArrayList<>();
        for (Map<String, String> rowMap : rowMapList) {
            SolrInputDocument solrInput = new SolrInputDocument();
            for (Map.Entry<String, String> rowEntry : rowMap.entrySet()) {
                String value = rowEntry.getValue();
                if (value.length() > 0) {
                    String filed = rowEntry.getKey();
                    if (SolrFiled.PROJECT_FILEDS.contains(filed)) {
                        solrInput.addField(filed, value);
                    }
                }
            }
            solrInput.addField(SolrFiled.TYPE,"Project");
            documentList.add(solrInput);
        }
        return SolrUtils.addAll(UpdateRequest.create(SolrCollection.HANDLE_PROJECT).documentList(documentList));
    }

}
