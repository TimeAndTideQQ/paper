package com.wanfangdata.topic.dao.resource;

import com.wanfangdata.topic.solr.execute.search.SearchRequest;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Administrator
 */
public interface ResourceDao {

    /**
     * 查询
     * @param searchRequest
     * @return
     */
    public Map<String, Object> selectSolr(SearchRequest searchRequest);


    /**
     *  solr列表查询
     * @param dataMap
     * @return
     */
    public boolean UpdateSolr(HashMap<String,Object> dataMap);
}
