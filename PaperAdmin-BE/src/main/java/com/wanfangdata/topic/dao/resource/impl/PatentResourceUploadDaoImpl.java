package com.wanfangdata.topic.dao.resource.impl;

import com.wanfangdata.topic.constant.SolrCollection;
import com.wanfangdata.topic.constant.SolrFiled;
import com.wanfangdata.topic.dao.resource.ResourceUploadDao;
import com.wanfangdata.topic.solr.SolrUtils;
import com.wanfangdata.topic.solr.execute.update.UpdateRequest;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 专利excel数据处理
 *
 * @author Administrator
 * @date 2023/03/01 16:23
 **/
@Repository("Patent")
public class PatentResourceUploadDaoImpl implements ResourceUploadDao {


    @Override
    public boolean createDocument(List<Map<String, String>> rowMapList) {
        List<SolrInputDocument> documentList = new ArrayList<>();
        for (Map<String, String> rowMap : rowMapList) {
            SolrInputDocument solrInput = new SolrInputDocument();
            for (Map.Entry<String, String> rowEntry : rowMap.entrySet()) {
                String value = rowEntry.getValue();
                if (value.length() > 0) {
                    String filed = rowEntry.getKey();
                    if (SolrFiled.PATENT_FILEDS.contains(filed)) {
                        if (SolrFiled.AU.equals(filed)) {
                            solrInput.addField(filed, value.split("; "));
                            continue;
                        }
                        if (SolrFiled.PN.equals(filed)) {
                            solrInput.addField(filed, value.split("; "));
                            continue;
                        }
                        if (SolrFiled.DC.equals(filed)) {
                            solrInput.addField(filed, value.split("; "));
                            continue;
                        }
                        if (SolrFiled.MC.equals(filed)) {
                            solrInput.addField(filed, value.split("; "));
                            continue;
                        }
                        if (SolrFiled.IP.equals(filed)) {
                            solrInput.addField(filed, value.split("; "));
                            continue;
                        }
                        if (SolrFiled.PI.equals(filed)) {
                            solrInput.addField(filed, value.split("; "));
                            continue;
                        }
                        if (SolrFiled.CP.equals(filed)) {
                            solrInput.addField(filed, value.split("; "));
                            continue;
                        }
                        if (SolrFiled.CR.equals(filed)) {
                            solrInput.addField(filed, value.split("; "));
                            continue;
                        }
                        if (SolrFiled.DN.equals(filed)) {
                            solrInput.addField(filed, value.split("; "));
                            continue;
                        }
                        if (SolrFiled.CI.equals(filed)) {
                            solrInput.addField(filed, value.split("; "));
                            continue;
                        }
                        solrInput.addField(filed, value);
                    }
                }
            }
            solrInput.addField(SolrFiled.TYPE,"Patent");
            documentList.add(solrInput);
        }
        return SolrUtils.addAll(UpdateRequest.create(SolrCollection.HANDLE_PATENT).documentList(documentList));
    }
}
