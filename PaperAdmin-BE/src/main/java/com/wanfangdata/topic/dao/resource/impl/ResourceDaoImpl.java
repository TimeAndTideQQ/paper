package com.wanfangdata.topic.dao.resource.impl;

import com.wanfangdata.topic.constant.SolrCollection;
import com.wanfangdata.topic.constant.SolrFiled;
import com.wanfangdata.topic.dao.resource.ResourceDao;
import com.wanfangdata.topic.solr.SolrUtils;
import com.wanfangdata.topic.solr.execute.search.SearchRequest;
import com.wanfangdata.topic.solr.execute.update.UpdateRequest;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author Administrator
 * @date 2023/03/07 16:57
 **/
@Service
public class ResourceDaoImpl implements ResourceDao {


    @Override
    public Map<String, Object> selectSolr(SearchRequest searchRequest) {
        return SolrUtils.search(searchRequest);
    }


    @Override
    public boolean UpdateSolr(HashMap<String, Object> dataMap) {
        String collection = null;
        List<SolrInputDocument> documentList = new ArrayList<>();
        SolrInputDocument solrInput = new SolrInputDocument();
        for (Map.Entry<String, Object> queryEntry : dataMap.entrySet()) {
            Object value = queryEntry.getValue();
            if (value != null) {
                String key = queryEntry.getKey();
                if (key.equals("Type")){
                    String str = (String) value;
                    switch (str) {
                        case "Thesis":
                            collection=SolrCollection.HANDLE_THESIS;
                            break;
                        case "Project":
                            collection=SolrCollection.HANDLE_PROJECT;
                            break;
                        case "Patent":
                            collection=SolrCollection.HANDLE_PATENT;
                            break;
                        default:
                            break;
                    }
                    solrInput.addField(key, value);
                }else {
                    solrInput.addField(key, value);
                }

            }
        }
        documentList.add(solrInput);
        return SolrUtils.add(UpdateRequest.create(collection).documentList(documentList));
    }



}
