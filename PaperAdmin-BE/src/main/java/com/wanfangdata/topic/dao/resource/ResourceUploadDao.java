package com.wanfangdata.topic.dao.resource;

import org.apache.solr.common.SolrInputDocument;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
public interface ResourceUploadDao {

    /**
     * 创建 solrDocument
     * @param rowMapList
     * @return
     */
    public boolean createDocument(List<Map<String,String>> rowMapList);

}
