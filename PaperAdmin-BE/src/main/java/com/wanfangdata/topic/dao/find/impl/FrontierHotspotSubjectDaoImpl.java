package com.wanfangdata.topic.dao.find.impl;

import com.wanfangdata.topic.constant.SolrCollection;
import com.wanfangdata.topic.constant.SolrFiled;
import com.wanfangdata.topic.dao.find.IFrontierDao;
import com.wanfangdata.topic.solr.Query;
import com.wanfangdata.topic.solr.SolrUtils;
import com.wanfangdata.topic.solr.execute.facet.FacetRequest;
import com.wanfangdata.topic.util.TimeUtils;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @Description 学科热点
 * @Authors smj
 * @Date 2023/2/19 10:23
 * @Version
 */
@Component
public class FrontierHotspotSubjectDaoImpl implements IFrontierDao {

    
    @Override
    public Map<String, Long> getFiveYearKeywords(Query query) {
        FacetRequest facet = FacetRequest.create(SolrCollection.TOPIC_PERIODICAL_CHI)
                .q(query.build())
                .facet(SolrFiled.KEYWORDS)
                .fl(SolrFiled.RETURN_FILED)
                .fq(TimeUtils.getDiffYearFq(SolrFiled.PUBLICATION_YEAR,1))
                .limit(1);
        return SolrUtils.facet(facet);
    }
}
