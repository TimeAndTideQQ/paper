package com.wanfangdata.topic.controller.sao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanfangdata.topic.entry.sao.SaoDataEntry;
import com.wanfangdata.topic.entry.sao.SaoFrontier;
import com.wanfangdata.topic.resultenum.Result;
import com.wanfangdata.topic.service.sao.ISaoDataService;
import com.wanfangdata.topic.service.sao.ISaoFrontDataService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description 前沿度
 * @Authors smj
 * @Date 2023/3/4 12:31
 * @Version
 */
@CrossOrigin
@RestController
@RequestMapping("sao/front")
public class SaoFrontDataController {

    @Autowired
    private ISaoFrontDataService saoFrontDataService;

    @Autowired
    private ISaoDataService service;


    /**
     * 查询SAO
     *
     * @return R
     */
    @GetMapping("list/{currentPage}/{pageSize}")
    public Result getSaoList(@PathVariable("currentPage") Integer currentPage, @PathVariable("pageSize") Integer pageSize) {
        IPage<SaoFrontier> iPage = new Page<>(currentPage, pageSize);
        IPage<SaoFrontier> page = saoFrontDataService.page(iPage);
        return Result.success().setData(page);
    }


    @GetMapping("delete/{id}")
    public Result deleteById(@PathVariable("id") String id) {
        List<SaoDataEntry> frontier = service.list(new QueryWrapper<SaoDataEntry>().eq("frontier", id));
        if (frontier != null && frontier.size() > 0) {
            return Result.success().setMsg("前沿等级正在使用,无法删除");
        } else {
            return Result.success().setData(saoFrontDataService.removeById(id));
        }
    }

    @PostMapping("update")
    public Result updateById(@RequestBody SaoFrontier saoDataEntry) {
        SaoFrontier searchData = saoFrontDataService.getById(saoDataEntry.getId());
        BeanUtils.copyProperties(saoDataEntry, searchData);
        return Result.success().setData(saoFrontDataService.updateById(saoDataEntry));
    }

    @PostMapping("add")
    public Result add(@RequestBody SaoFrontier saoDataEntry) {
        return Result.success().setData(saoFrontDataService.save(saoDataEntry));
    }

    @GetMapping("get/{id}")
    public Result getById(@PathVariable("id") String id) {
        return Result.success().setData(saoFrontDataService.getById(id));
    }
}
