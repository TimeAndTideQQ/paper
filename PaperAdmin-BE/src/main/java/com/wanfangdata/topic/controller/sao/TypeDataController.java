package com.wanfangdata.topic.controller.sao;

import com.wanfangdata.topic.entry.sao.Type;
import com.wanfangdata.topic.resultenum.Result;
import com.wanfangdata.topic.service.sao.ITypeDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description
 * @Authors smj
 * @Date 2023/3/4 12:31
 * @Version
 */
@CrossOrigin
@RestController
@RequestMapping("type")
public class TypeDataController {

    @Autowired
    private ITypeDataService typeDataService;


    /**
     * 查询SAO
     *
     * @return R
     */
    @GetMapping("list")
    public Result getSaoList() {
        return Result.success().setData(typeDataService.list());
    }


    @PostMapping("update")
    public Result updateById(@RequestBody Type type) {
        return Result.success().setData(typeDataService.updateById(type));
    }

    @PostMapping("criticUpdate")
    public Result criticUpdate(@RequestBody List<Type> typeList) {
        return Result.success().setData(typeDataService.updateBatchById(typeList));
    }

    /**
     * 计算比例
     *
     * @return R
     */
//    @Cacheable(cacheNames = "proportion")
    @GetMapping("proportion")
    public Result proportion() {
        double[] sampleProportions = typeDataService.getSampleProportions();
        List<Type> list = typeDataService.list();
        list.get(0).setWeight(sampleProportions[0]);
        list.get(1).setWeight(sampleProportions[1]);
        list.get(2).setWeight(sampleProportions[2]);
        return Result.success().setData(list);
    }

}
