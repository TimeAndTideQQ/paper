package com.wanfangdata.topic.controller.resource;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wanfangdata.topic.entry.sao.SaoPretreatment;
import com.wanfangdata.topic.resultenum.EnumResultStates;
import com.wanfangdata.topic.resultenum.Result;
import com.wanfangdata.topic.service.resource.ResourceService;
import com.wanfangdata.topic.service.sao.SaoPretreatmentService;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description 资源导入
 * @Authors
 * @Date 2023/2/23 8:19
 * @Version
 */
@CrossOrigin
@RestController
@RequestMapping("/resource")
public class ResourceUploadController {

    private static final Logger logger = LoggerFactory.getLogger(ResourceUploadController.class);
    private static final String ENDING_XLS = ".xls";
    private static final String ENDING_XLSX = ".xlsx";

    public static final String PATH = System.getProperty("user.dir");

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private SaoPretreatmentService saoPretreatmentService;

    /**
     * excel 上传
     *
     * @param type
     * @param file
     * @return
     */
    @PostMapping("/upload")
    public Result upload(String type, @RequestParam("file") MultipartFile file) {
        Workbook workbook = null;
        String fileName = file.getOriginalFilename();
        try {
            if (fileName.endsWith(ENDING_XLS)) {
                workbook = new HSSFWorkbook(file.getInputStream());
            } else if (fileName.endsWith(ENDING_XLSX)) {
                workbook = new XSSFWorkbook(file.getInputStream());
            } else {
                logger.warn("excel格式不符 === << {} >>  ", fileName);
                return Result.error(EnumResultStates.ILLEGAL_FILETYPE_EXCEPTION_ERROR);
            }
            resourceService.dataAnalysis(type, workbook);
        } catch (Exception e) {
            logger.error("excel解释异常！", e);
            return Result.error(EnumResultStates.EXCEL_DATA_IMPORT_ERROR);
        }
        return Result.success();
    }

    /**
     * solr 列表查询
     *
     * @param solrQuery
     * @return
     */
    @RequestMapping("/selectSolr")
    public Result selectSolr(@RequestBody HashMap<String, String> solrQuery) {
        Map solrData = null;
        try {
            if (solrQuery != null && solrQuery.size() > 0) {
                solrData = resourceService.selectSolr(solrQuery);
            }
        } catch (Exception e) {
            logger.error("solr列表查询异常！{}", e);
            return Result.error();
        }
        return Result.success(solrData);
    }

    /**
     * solr 列表查询
     *
     * @param solrMap
     * @return
     */
    @RequestMapping("/solrupdate")
    public Result solrUpdate(@RequestBody HashMap<String, String> solrMap) {
        if (resourceService.UpdateSolr(solrMap)) {
            return Result.success();
        }
        return Result.error();
    }

    /**
     * solr 列表查询
     *
     * @param id
     * @return
     */
    @RequestMapping("/solrdelete")
    public Result solrDelete(String id,String type) {
        try {
            boolean b = resourceService.deleteSolr(id,type);
        } catch (Exception e) {
            logger.error("solr 删除异常！{}", e);
            return Result.error();
        }
        return Result.success();
    }

    @RequestMapping("/getPAtchWord")
    public Result getPAtchWord(Integer pid,String name, HttpServletResponse response) {
        QueryWrapper<SaoPretreatment> pcid = new QueryWrapper<SaoPretreatment>().eq("pcid", pid);
        List<SaoPretreatment> saoPretreatments = saoPretreatmentService.list(pcid);
        List<String> termENs = saoPretreatments.parallelStream()
                .map(SaoPretreatment::getTermEN)
                .collect(Collectors.toList());
        XSSFWorkbook wb = new XSSFWorkbook();
        Sheet sheet = wb.createSheet("sheet");
        for (int i = 0; i < termENs.size(); i++) {
            Row headRow = sheet.createRow(i);
            Cell cell = headRow.createCell(0);
            cell.setCellValue(termENs.get(i));
        }
        response.setDateHeader("Expires", 0);
        response.setHeader("Content-disposition", String.format("attachment; filename="+name+".xlsx"));
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        try (ServletOutputStream out = response.getOutputStream()) {
            wb.write(out);
            out.flush();
        } catch (IOException e) {
            logger.error("批次sao下载失败！",e);
            return Result.error();
        }
        return Result.success();
    }

    @RequestMapping("/getTemplate")
    public Result getTemplate(String name, HttpServletResponse response) {
        List<String> termENs = new ArrayList<>();
        if (name != null && name.length() > 0){
            response.setDateHeader("Expires", 0);
            response.setHeader("Content-disposition", String.format("attachment; filename="+name+".xlsx"));
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
            InputStream inputStream = null;
            ServletOutputStream outputStream = null;
            try{
                byte[] b = new byte[1024];
                 outputStream = response.getOutputStream();
                 inputStream = new ClassPathResource(name+".xlsx").getInputStream();
                while (inputStream.read(b) > 0){
                    outputStream.write(b);
                }
            } catch (IOException e) {
                logger.error("批次sao下载失败！",e);
                return Result.error();
            }finally {
                try {
                    outputStream.close();
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return Result.success();
    }

}
