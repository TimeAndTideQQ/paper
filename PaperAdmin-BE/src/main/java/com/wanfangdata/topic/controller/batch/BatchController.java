package com.wanfangdata.topic.controller.batch;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanfangdata.topic.constant.SolrCollection;
import com.wanfangdata.topic.entry.batch.BatchEntry;
import com.wanfangdata.topic.mapper.batch.impl.IBatchServiceImpl;
import com.wanfangdata.topic.resultenum.EnumResultStates;
import com.wanfangdata.topic.resultenum.Result;
import com.wanfangdata.topic.service.sao.ISaoDataService;
import com.wanfangdata.topic.solr.SolrUtils;
import com.wanfangdata.topic.solr.execute.facet.FacetRequest;
import com.wanfangdata.topic.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @ClassName BatchController
 * @Author ZhouZhM
 * @Date 2023/3/23 14:09
 * @Description
 * @Version 1.0
 */
@CrossOrigin
@RestController
@RequestMapping("batch")
public class BatchController {

    private static final Logger logger = LoggerFactory.getLogger(BatchController.class);


    @Autowired
    private IBatchServiceImpl batchService;

    @Autowired
    private ISaoDataService saoDataService;



    @RequestMapping("/createSao")
    public Result createSao(int id){
        try {
            boolean sao = saoDataService.createSao(id);
            return Result.success(sao);
        } catch (Exception e) {
            logger.error("sao生出失败!", e);
            return Result.error(EnumResultStates.SERVER_ERROR);
        }
    }





    /**
     * 查询批次表
     */
    @GetMapping(value = "/select")
    public Result selectBatch() {
        try {
            List<BatchEntry> list = batchService.list();
            return Result.success(list);
        } catch (Exception e) {
            logger.error("查询批次失败!", e);
            return Result.error(EnumResultStates.SERVER_ERROR);
        }
    }

    /**
     * 查询批次表
     */
    @RequestMapping(value = "/selectPage")
    public Result selectBatchPage(@RequestBody HashMap<String, Integer> patchQuery,@RequestParam("param") String param) {
        IPage<BatchEntry> iPage = new Page<>(patchQuery.get("pagerNum"), patchQuery.get("pageSize"));
        try {
            IPage<BatchEntry> page;
            if (StringUtil.isEmpty(param)){
                page = batchService.page(iPage);
            }else {
                page = batchService.page(iPage,new QueryWrapper<BatchEntry>().like("name",param));
            }
            page.getRecords().forEach(batchEntry -> batchEntry.setSum(getSum(batchEntry.getId())));
            return Result.success(page);
        } catch (Exception e) {
            logger.error("查询批次失败!", e);
            return Result.error(EnumResultStates.SERVER_ERROR);
        }
    }

    /**
     * 通过Id修改批次是否生成sao数据状态
     */
    @GetMapping(value = "/update/{id}")
    public Result updateBatch(@PathVariable int id) {
        try {
            UpdateWrapper<BatchEntry> updateWrapper = new UpdateWrapper();
            updateWrapper.eq("id", id).set("status", 1);
            boolean update = batchService.update(null, updateWrapper);
            logger.info("批次状态修改{}!", update ? "成功" : "失败");
            return Result.success(update);
        } catch (Exception e) {
            logger.error("修改批次状态失败!", e);
            return Result.error(EnumResultStates.SERVER_ERROR);
        }
    }

    private long getSum(Integer id) {
        String query = "Batch:" + id;
        LinkedHashMap<String, Long> facet = SolrUtils.facet(new FacetRequest(SolrCollection.HANDLE_PAPER).facet("Batch").q(query));
        return facet.size() == 0 ? 0 : facet.get(String.valueOf(id));
    }

}
