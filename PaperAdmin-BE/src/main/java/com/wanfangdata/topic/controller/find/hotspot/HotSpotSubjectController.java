package com.wanfangdata.topic.controller.find.hotspot;

import com.wanfangdata.topic.controller.find.hotspot.prams.HotspotSubjectPram;
import com.wanfangdata.topic.resultenum.Result;
import com.wanfangdata.topic.service.find.IFrontierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @Description 学科热点控制器
 * @Authors smj
 * @Date 2023/2/19 13:22
 * @Version
 */
@RestController
@CrossOrigin
@RequestMapping("/find/hotspot")
public class HotSpotSubjectController {


    @Autowired
    private IFrontierService frontierService;

    /**
     * 获取800个关键词
     *
     * @return R
     */
    @GetMapping("/getKeywords/{code}")
    public Result getFiveYearKeywords(@PathVariable String code) {
        Map<String, Long> fiveYearKeywords = frontierService.getFiveYearKeywords(HotspotSubjectPram.getCommonKeywordsQuery(code));
        return Result.success().setData(fiveYearKeywords);
    }

    /**
     * 指数排序,取多少词
     *
     * @return R
     */
    @PostMapping("/sort/{num}")
    public Result keywordsSortIndex(@PathVariable int num) {

        return null;
    }

    /**
     * 指数排序,取多少词
     *
     * @return R
     */
    @PostMapping("/rule")
    public Result ruleKeywords() {

        return null;
    }

    /**
     * 添加产品提供关键词
     *
     * @param addKeywords 关键词
     * @return R
     */
    @PostMapping("/addKeywords")
    public Result setKeywords(@RequestParam List<String> addKeywords) {

        return null;
    }


}
