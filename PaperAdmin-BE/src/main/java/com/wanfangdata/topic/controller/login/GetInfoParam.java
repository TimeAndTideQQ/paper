package com.wanfangdata.topic.controller.login;


import com.wanfangdata.topic.constant.SolrFiled;
import com.wanfangdata.topic.solr.Query;

/**
 * @Description
 * @Authors smj
 * @Date 2023/2/17 15:56
 * @Version
 */
public class GetInfoParam {

    public static String getCommonQuery(String q) {
        Query query = new Query();
        return query
                .left()
                .accurate(SolrFiled.ID, q)
                .right()
                .build();

    }
}
