package com.wanfangdata.topic.controller.login;

import com.wanfangdata.topic.constant.SolrCollection;
import com.wanfangdata.topic.resultenum.Result;
import com.wanfangdata.topic.solr.QueryRequest;
import com.wanfangdata.topic.solr.execute.SolrExecute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Description 测试
 * @Authors smj
 * @Date 2023/2/17 15:53
 * @Version
 */
@Controller
public class GetInfoController {

    @Autowired
    private SolrExecute solrSearch;


    @GetMapping("/getId")
    @ResponseBody
    public Result getId() {
        QueryRequest queryRequest = QueryRequest.create(SolrCollection.TOPIC_PERIODICAL_CHI, GetInfoParam.getCommonQuery("QK201003915725"));
//        List<Map<String, Object>> search = solrSearch.search(queryRequest);
        return Result.success();
    }

}
