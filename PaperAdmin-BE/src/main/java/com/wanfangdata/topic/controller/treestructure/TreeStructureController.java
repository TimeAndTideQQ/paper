package com.wanfangdata.topic.controller.treestructure;

import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.wanfangdata.topic.controller.sao.SaoDataController;
import com.wanfangdata.topic.entry.treestructure.CrossRelationRequest;
import com.wanfangdata.topic.entry.treestructure.NodeRelationEntry;
import com.wanfangdata.topic.entry.treestructure.TreeEntry;
import com.wanfangdata.topic.entry.treestructure.TreeStructureEntry;
import com.wanfangdata.topic.resultenum.EnumResultStates;
import com.wanfangdata.topic.resultenum.Result;
import com.wanfangdata.topic.service.treestructure.impl.TreeStructureServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName TreeStructureController
 * @Author ZhouZhM
 * @Date 2023/3/7 15:33
 * @Description
 * @Version 1.0
 */
@CrossOrigin
@RestController
@RequestMapping("tree")
public class TreeStructureController {

    private static final Logger logger = LoggerFactory.getLogger(TreeStructureController.class);

    @Autowired
    private TreeStructureServiceImpl treeStructureService;

    @Autowired
    private SaoDataController saoDataController;

    /**
     * 新增树节点
     */
    @PostMapping(value = "/addNode")
    public Result addNode(@RequestBody TreeStructureEntry treeStructureEntry){
        try {
            int i = treeStructureService.insertStructureEntry(treeStructureEntry);
            logger.info("插入节点成功,共插入节点{}个",i);
            //更新sao数据的可视范围为不可视
            saoDataController.updateSaoIsShow(String.valueOf(treeStructureEntry.getSaoId()),true,String.valueOf(treeStructureEntry.getTreeId()));
            return Result.success(i);
        } catch (Exception e) {
            logger.error("插入节点失败!",e);
            return Result.error(EnumResultStates.SERVER_ERROR);
        }
    }

    /**
     * 检查当前sao词是否在树中存在
     */
    @GetMapping(value = "/isExist/{treeId}/{saoId}")
    public Result isExist(@PathVariable("treeId") int treeId, @PathVariable("saoId") int saoId){
        try {
            boolean exist = treeStructureService.isExist(treeId, saoId);
            logger.info("该sao词在当前树({})中{}",treeId,exist?"存在!":"不存在!");
            return Result.success(exist);
        } catch (Exception e) {
            logger.error("判断sao数据是否在树中存在错误!",e);
            return Result.error(EnumResultStates.SERVER_ERROR);
        }
    }

    /**
     * 删除树节点
     * @param treeId 树id
     * @param id 节点id
     */
    @GetMapping(value = "/deleteNode/{treeId}/{id}/{saoId}")
    public Result deleteTreeNode(@PathVariable("treeId") int treeId, @PathVariable("id") int id, @PathVariable("saoId") int saoId){
        try {
            treeStructureService.deleteStructureEntry(id,treeId);
            //更新sao数据的可视范围为不可视
            saoDataController.updateSaoIsShow(String.valueOf(saoId),false,String.valueOf(treeId));
            return Result.success(0);
        } catch (Exception e) {
            logger.error("删除节点失败!",e);
            return Result.error(EnumResultStates.SERVER_ERROR);
        }
    }

    /**
     * 查询树结构
     * @param treeId 树Id
     */
    @GetMapping(value = "/select/{treeId}")
    public Result getTree(@PathVariable("treeId") int treeId){
        try {
            List<Tree<Integer>> treeStructure = treeStructureService.selectStructureEntry(treeId);
            return Result.success(treeStructure);
        } catch (Exception e) {
            logger.error("查询失败!",e);
            return Result.error(EnumResultStates.SERVER_ERROR);
        }
    }

    /**
     * 查询节点关系
     */
    @GetMapping(value = "/selectNodeRelation")
    public Result getNodeRelation(){
        try {
            List<NodeRelationEntry> nodeRelationEntries = treeStructureService.selectNodeRelation();
            return Result.success(nodeRelationEntries);
        } catch (Exception e) {
            logger.error("查询节点关系失败!",e);
            return Result.error(EnumResultStates.SERVER_ERROR);
        }
    }

    /**
     * 新增节点关系
     */
    @PostMapping(value = "/addNodeRelation")
    public Result addNodeRelation(@RequestBody NodeRelationEntry nodeRelationEntry){
        try {
            int i = treeStructureService.addNodeRelation(nodeRelationEntry);
            logger.info("插入节点关系成功,共插入节点关系{}个",i);
            return Result.success(i);
        } catch (Exception e) {
            logger.error("插入节点关系失败!",e);
            return Result.error(EnumResultStates.SERVER_ERROR);
        }
    }

    /**
     * 修改节点关系
     */
    @PostMapping(value = "/updateRelation")
    public Result updateRelation(@RequestBody NodeRelationEntry nodeRelationEntry){
        try {
            int i = treeStructureService.updateNodeRelation(nodeRelationEntry);
            logger.info("修改节点关系成功,共修改节点关系{}个",i);
            return Result.success(i);
        } catch (Exception e) {
            logger.error("修改节点关系失败!",e);
            return Result.error(EnumResultStates.SERVER_ERROR);
        }
    }

    /**
     * 修改节点交叉关系
     */
    @PostMapping(value = "/updateCrossRelation")
    public Result updateCrossRelation(@RequestBody CrossRelationRequest crossRelationRequest){
        try {
            int i = treeStructureService.updateCrossRelation(crossRelationRequest);
            logger.info("修改节点交叉关系成功,共修改交叉关系节点{}个",i);
            return Result.success(i);
        } catch (Exception e) {
            logger.error("修改节点交叉关系失败!",e);
            return Result.error(EnumResultStates.SERVER_ERROR);
        }
    }

    /**
     * 更新节点坐标
     * @param id 节点id
     * @param coordinate 坐标值
     */
    @GetMapping(value = "/updateCoordinate/{id}/{coordinate}")
    public Result updateCoordinate(@PathVariable("id") int id, @PathVariable("coordinate") String coordinate){
        if (!StringUtils.hasLength(coordinate)){
            logger.error("坐标为空,更新坐标失败!");
            return Result.error(EnumResultStates.COORDINATE_ERROR);
        }
        UpdateWrapper<TreeStructureEntry> updateWrapper = new UpdateWrapper();
        updateWrapper.eq("id",id).set("coordinate",coordinate);
        boolean bool = treeStructureService.update(null, updateWrapper);
        logger.info("修改节点坐标{}!",bool?"成功":"失败");
        return Result.success(bool);
    }

    /**
     * 删除节点关系
     */
    @GetMapping(value = "/deleteRelation/{id}")
    public Result deleteRelation(@PathVariable("id") int id){
        try {
            //首先查找该节点关系有没有树节点正在使用，若有，则无法删除
            if (treeStructureService.nodeRelationIsUsing(id)){
                logger.info("节点关系正在使用,无法删除");
                return Result.success(EnumResultStates.NODE_RELATION_ERROR);
            }
            int i = treeStructureService.deleteNodeRelation(id);
            logger.info("删除节点关系成功,共删除节点关系{}个",i);
            return Result.success(i);
        } catch (Exception e) {
            logger.error("删除节点关系失败!",e);
            return Result.error(EnumResultStates.SERVER_ERROR);
        }
    }

    /**
     * 查询所有树
     */
    @GetMapping(value = "/selectTrees")
    public Result selectTrees(){
        try {
            return Result.success(treeStructureService.selectTrees());
        } catch (Exception e) {
            logger.error("查询所有树失败!",e);
            return Result.error(EnumResultStates.SERVER_ERROR);
        }
    }

    /**
     * 增加一棵树
     */
    @PostMapping(value = "/addTree")
    public Result addTree(@RequestBody TreeEntry treeEntry){
        try {
            int num = treeStructureService.addTree(treeEntry);
            logger.info("成功增加{}颗树",num);
            if (num == 1){
                // 在树节点结构里增加一个根节点
                int i = treeStructureService
                        .insertStructureEntry(new TreeStructureEntry(9, 1, treeEntry.getSaoId(), 0, treeEntry.getId()));
                logger.info("成功在树结构表中增加{}个根节点",i);
            }
            return Result.success(num);
        } catch (Exception e) {
            logger.error("增加树失败!",e);
            return Result.error(EnumResultStates.SERVER_ERROR);
        }
    }

    /**
     * 修改一棵树
     */
    @PostMapping(value = "/updateTree")
    public Result updateTree(@RequestBody TreeEntry treeEntry){
        try {
            int i = treeStructureService.updateTree(treeEntry);
            logger.info("成功修改{}颗树",i);
            return Result.success(i);
        } catch (Exception e) {
            logger.error("修改树失败!",e);
            return Result.error(EnumResultStates.SERVER_ERROR);
        }
    }

    /**
     * 删除一棵树
     */
    @GetMapping(value = "/deleteTree/{id}")
    public Result deleteTree(@PathVariable("id") int treeId){
        try {
            //先删除这棵树上的所有节点
            int num = treeStructureService.deleteStructureByTreeId(treeId);
            logger.info("共删除id为{}的树上节点{}个",treeId,num);
            //再在树库中删除这棵树
            int i = treeStructureService.deleteTree(treeId);
            logger.info("成功删除{}颗树",i);
            return Result.success(i);
        } catch (Exception e) {
            logger.error("删除树失败!",e);
            return Result.error(EnumResultStates.SERVER_ERROR);
        }
    }
}
