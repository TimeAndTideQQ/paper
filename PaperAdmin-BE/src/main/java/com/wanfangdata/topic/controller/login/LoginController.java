package com.wanfangdata.topic.controller.login;

import com.wanfangdata.topic.dto.UserLogin;
import com.wanfangdata.topic.resultenum.Result;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * @Authors smj
 * @Date 2023/2/18 17:19
 * @Version
 */
@CrossOrigin
@RequestMapping("/admin")
@RestController
public class LoginController {


    @PostMapping("/login")
    public Result login(@RequestBody UserLogin userLogin) {
        Map<String, String> map = new HashMap<>(1);
        map.put("token", "admin");
        return Result.success(map);
    }

    @GetMapping("/info")
    public Result info(@RequestParam String token) {
        Map<String, String> map = new HashMap<>(1);
        map.put("roles", "admin");
        map.put("name","admin");
        map.put("avatar","https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
        return Result.success(map);
    }

    @PostMapping("/logout")
    public Result logout() {
        return Result.success();
    }
}
