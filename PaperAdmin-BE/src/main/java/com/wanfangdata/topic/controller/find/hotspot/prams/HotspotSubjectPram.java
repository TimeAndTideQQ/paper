package com.wanfangdata.topic.controller.find.hotspot.prams;

import com.wanfangdata.topic.constant.SolrFiled;
import com.wanfangdata.topic.solr.Query;

/**
 * @Description 学科热点查询语句
 * @Authors smj
 * @Date 2023/2/19 21:59
 * @Version
 */
public class HotspotSubjectPram {

    public static Query getCommonKeywordsQuery(String eduCode) {
        Query query = new Query();
        return query
                .left()
                .accurate(SolrFiled.EDU_CODE, eduCode)
                .right();
    }
}
