package com.wanfangdata.topic.controller.sao;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanfangdata.topic.baidu.TransApi;
import com.wanfangdata.topic.constant.SolrCollection;
import com.wanfangdata.topic.constant.SolrFiled;
import com.wanfangdata.topic.entry.sao.*;
import com.wanfangdata.topic.resultenum.EnumResultStates;
import com.wanfangdata.topic.resultenum.Result;
import com.wanfangdata.topic.service.sao.*;
import com.wanfangdata.topic.solr.Query;
import com.wanfangdata.topic.solr.SolrUtils;
import com.wanfangdata.topic.solr.entity.SolrQueryResponseEntity;
import com.wanfangdata.topic.solr.execute.search.SearchRequest;
import com.wanfangdata.topic.util.StringUtil;
import com.wanfangdata.topic.vo.sao.SaoDataVO;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.solr.client.solrj.response.FieldStatsInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * @Description
 * @Authors smj
 * @Date 2023/3/4 12:31
 * @Version
 */
@CrossOrigin
@RestController
@RequestMapping("sao")
public class SaoDataController {

    private static final Logger logger = LoggerFactory.getLogger(SaoDataController.class);

    @Autowired
    private ISaoDataService saoDataService;
    @Autowired
    private ISaoDataPreService preService;
    @Autowired
    private ISaoFrontDataService frontDataService;
    @Autowired
    private SaoPretreatmentService saoPretreatmentService;
    @Autowired
    private ITypeDataService typeService;
    @Autowired
    private SaoExcelUploadService uploadService;
    @Autowired
    private ISaoDataPreService saoDataPreService;


    @Resource
    private TransApi transApi;
    private static final String ENDING_XLS = ".xls";
    private static final String ENDING_XLSX = ".xlsx";
    private static final Map<Long, String> TYPE = new HashMap<>();
    private static final Map<Long, Double> WEIGHT = new HashMap<>();

    private static final String NOVELTY = "novelty";
    private static final String INCREASE = "increase";
    private static final String ATTENTION = "attention";
    private static final String CROSSOVER = "crossover";
    private static final String INTERESTS = "interests";


    /**
     * 查询SAO
     *
     * @return R
     */
    @GetMapping("list")
    public Result getSaoList() {
        QueryWrapper<SaoDataEntry> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("createTime");
        List<SaoDataEntry> list = saoDataService.list(queryWrapper);
        return Result.success().setData(list);
    }

    /**
     * 修改sao
     *
     * @param id     saoId
     * @param isUse  是否要使用  true为添加 false为删除
     * @param treeId treeId
     * @return R
     */
    @GetMapping("update")
    public Result updateSaoIsShow(@RequestParam("id") String id, @RequestParam(value = "isUse") boolean isUse, @RequestParam(value = "treeId", required = false) String treeId) {
        SaoDataEntry searchData = saoDataService.getById(id);
        String getTreeIds = searchData.getIsShow();
        String[] split = Arrays.stream(getTreeIds.split(",")).filter(x -> !x.isEmpty()).toArray(String[]::new);
        UpdateWrapper<SaoDataEntry> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        if (isUse) {
            if (!StringUtil.isEmpty(getTreeIds)) {
                for (String tree : split) {
                    if (treeId.equals(tree)) {
                        return Result.success().setMsg("已经在该树上，无需添加");
                    }
                }
                updateWrapper.set("isShow", getTreeIds + "," + treeId);
            } else {
                updateWrapper.set("isShow", treeId);
            }
            saoDataService.update(updateWrapper);
            return Result.success().setMsg("添加成功");
        } else {
            List<String> newTreeId = new ArrayList<>();
            if (!StringUtil.isEmpty(getTreeIds)) {
                for (String tree : split) {
                    if (!treeId.equals(tree)) {
                        newTreeId.add(tree);
                    }
                }
                updateWrapper.set("isShow", newTreeId.toString().replace("\\s", "").replace("[", "").replace("]", ""));
                boolean update = saoDataService.update(updateWrapper);
                return Result.success().setMsg("移除成功");
            } else {
                return Result.success().setMsg("这个词没有在这颗树，无需删除");
            }
        }
    }

    /**
     * SAO词推送论文
     *
     * @param keyword SAO词
     * @return R
     */
    @GetMapping("push")
    public Result pushInformation(@RequestParam("keyword") String keyword) {
        if (keyword.isEmpty()) {
            return Result.error().setMsg("关键词参数不完整");
        }
        Query query = new Query();
        query.fuzziness(SolrFiled.TITLE, keyword).or().fuzziness(SolrFiled.ABSTRACT, keyword);
        List<SaoDataVO> information = saoDataService.getInformation(query);
        System.out.println("information = " + information.toString());
        return Result.success().setData(information);
    }

    /**
     * 删除数据，移除SAO数据及前置表
     *
     * @param id saoId
     * @return R
     */
    @Transactional(rollbackFor = Exception.class)
    @GetMapping("delete/{id}")
    public Result deleteById(@PathVariable("id") String id) {
        SaoDataEntry service = saoDataService.getById(id);
        if (service == null) {
            return Result.success().setMsg("Id不存在");
        }
        if (StringUtil.isEmpty(service.getIsShow())) {
            preService.removeByIds(Arrays.asList(service.getPreIds().split(",")));
            uploadService.remove(new QueryWrapper<SaoExcelUpload>().eq("termEN", service.getTermEn()));
            return Result.success().setData(saoDataService.removeById(id));
        } else {
            return Result.success().setMsg("该节点关系在使用中！");
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping("update")
    public Result updateById(@RequestBody SaoDataEntry saoDataEntry) {
        SaoDataEntry searchData = saoDataService.getById(saoDataEntry.getId());
        BeanUtils.copyProperties(saoDataEntry, searchData);
        saoDataService.updateById(saoDataEntry);

        String preIds = searchData.getPreIds();
        if (!StringUtil.isEmpty(preIds)) {
            String[] split = preIds.split(",");
            for (String perId : split) {
                saoDataPreService.update(
                        new UpdateWrapper<SaoDataPreEntry>()
                                .eq("id", perId)
                                .set("termCn", saoDataEntry.getTermCn())
                                .set("termEn", saoDataEntry.getTermEn()));
            }
        }
        return Result.success().setData(true);
    }

    @Transactional(rollbackFor = Exception.class)
    @PostMapping("addBatch")
    public Result addBatch(@RequestBody SaoDataPreEntry preEntry) {
        try {
            for (int i = 1; i <= 3; i++) {
                preEntry.setId(null);
                preEntry.setType(i);
                add(preEntry);
            }
            return Result.success().setData(true);
        } catch (Exception e) {
            logger.error("添加异常！", e);
        }
        return Result.error();
    }


    /**
     * 当出现Exception时，进行事务回滚
     * <p>
     * //     * @param saoDataEntry sao
     *
     * @return R
     */
    @Transactional(rollbackFor = Exception.class)
    @PostMapping("add")
    public Result add(@RequestBody SaoDataPreEntry preEntry) {
        if (StringUtil.isEmpty(preEntry.getTermEn())) {
            return Result.success().setMsg("数据不完整！");
        }
        preEntry.setCreateTime(new Date());
        preEntry.setTermEn(preEntry.getTermEn().replace(" ", ""));

        SaoDataEntry saoDataEntry = new SaoDataEntry();
        BeanUtils.copyProperties(preEntry, saoDataEntry);
        saoDataEntry.setIsShow("");

        SaoDataPreEntry pre = preService.getOne(new QueryWrapper<SaoDataPreEntry>()
                .eq("termEN", preEntry.getTermEn())
                .eq("type", preEntry.getType()));

        if (pre == null) {
            preService.save(preEntry);
            SaoDataEntry termEn = saoDataService.getOne(new QueryWrapper<SaoDataEntry>().eq("termEn", preEntry.getTermEn()));
            if (termEn == null) {
                saoDataEntry.setPreIds(String.valueOf(preEntry.getId()));
                saoDataService.save(saoDataEntry);
            } else {
                String preIds = preService.list(new QueryWrapper<SaoDataPreEntry>().eq("termEn", preEntry.getTermEn())).stream()
                        .map(SaoDataPreEntry::getId).collect(Collectors.toList())
                        .toString().replace("[", "").replace("]", "");
                saoDataEntry.setPreIds(preIds);
                saoDataEntry.setId(termEn.getId());
                saoDataService.updateById(saoDataEntry);
            }
        } else {
            preEntry.setId(preEntry.getId());
            preService.updateById(preEntry);
        }
        Result calculate = calculate(saoDataEntry);

        return Result.success().setData(calculate.isState());
    }


    private double getWeight(List<SaoDataPreEntry> typeList, int sumData, Type type, int location) {
        List<SaoDataPreEntry> collect1 = typeList.stream().filter(x -> x.getNovelty() > 0).collect(Collectors.toList());
        if (sumData <= 1) {
            return 1;
        }
        if (sumData >= 3 && collect1.size() == 3) {
            return type.getWeight();
        }

        List<Integer> collect = collect1.stream().map(SaoDataPreEntry::getType).collect(Collectors.toList());


        //关注度或交叉
        if (location == 3 || location == 4) {
            List<Integer> thesisAndPatent = new ArrayList<>();
            thesisAndPatent.add(1);
            thesisAndPatent.add(3);
            thesisAndPatent.retainAll(collect);
            if (thesisAndPatent.size() < 2) {
                return 1;
            }
        } else if (location == 5) {
            List<Integer> project = new ArrayList<>();
            project.add(2);
            project.retainAll(collect);
            if (project.size() < 1) {
                return 1;
            }
        }


        List<Integer> thesisAndPatent = new ArrayList<>();
        thesisAndPatent.add(1);
        thesisAndPatent.add(3);
        thesisAndPatent.removeAll(collect);
        //论文和专利
        if (thesisAndPatent.isEmpty()) {
            if (type.getId() == 3) {
                return WEIGHT.get(3L) / (WEIGHT.get(1L) + WEIGHT.get(3L));
            } else {
                return WEIGHT.get(1L) / (WEIGHT.get(1L) + WEIGHT.get(3L));
            }
        }
        List<Integer> thesisAndProject = new ArrayList<>();
        thesisAndProject.add(2);
        thesisAndProject.add(3);
        thesisAndProject.removeAll(collect);

        //论文和项目
        if (thesisAndProject.isEmpty()) {
            if (type.getId() == 3) {
                return WEIGHT.get(3L) / (WEIGHT.get(2L) + WEIGHT.get(3L));
            } else {
                return WEIGHT.get(2L) / (WEIGHT.get(2L) + WEIGHT.get(3L));
            }
        }
        List<Integer> patentAndProject = new ArrayList<>();
        patentAndProject.add(1);
        patentAndProject.add(2);
        patentAndProject.removeAll(collect);

        //专利和项目
        if (patentAndProject.isEmpty()) {

            if (type.getId() == 2) {
                return WEIGHT.get(2L) / (WEIGHT.get(1L) + WEIGHT.get(2L));
            } else {
                return WEIGHT.get(1L) / (WEIGHT.get(1L) + WEIGHT.get(2L));
            }
        }


        return 1;
    }


    @GetMapping("list/{currentPage}/{pageSize}")
    public Result getSaoList(@PathVariable("currentPage") Integer currentPage, @PathVariable("pageSize") Integer pageSize
            , @RequestParam(value = "param", required = false) String param) {

        IPage<SaoDataEntry> iPage = new Page<>(currentPage, pageSize);
        IPage<SaoDataEntry> page;
        if (StringUtil.isEmpty(param)) {
            page = saoDataService.page(iPage, new QueryWrapper<SaoDataEntry>().orderByDesc("id"));
        } else {
            param = param.trim();
            page = saoDataService.page(iPage, new QueryWrapper<SaoDataEntry>().like("UPPER(termEN)", param.toUpperCase()).or().like("termCN", param).orderByDesc("id"));
        }
        List<SaoDataEntry> collect = page.getRecords().stream()
                .peek(x -> x.setSaoFrontier(frontDataService.getById(x.getFrontier())))
                .peek(x -> x.setSaoDataPreEntryList(saoDataPreService.list(new QueryWrapper<SaoDataPreEntry>().eq("termEN", x.getTermEn()))))
                .collect(Collectors.toList());
        page.setRecords(collect);
        return Result.success().setData(page);
    }

    @GetMapping("get/{id}")
    public Result getById(@PathVariable("id") String id) {
        return Result.success().setData(saoDataService.getById(id));
    }

    @GetMapping("avg")
    public Result getAVG() {
        return Result.success().setData(getAverage());
    }


    @GetMapping("easyCalculate")
    public Result calculate() {
        List<SaoDataEntry> list = saoDataService.list();
        for (SaoDataEntry saoDataEntry : list) {
            calculate(saoDataEntry);
        }
        return Result.success();
    }


    /**
     * 计算关键词的各种数值，pre表
     *
     * @param saoDataEntry 数据
     * @return R
     */
    @PostMapping("calculate")
    public Result calculate(@RequestBody SaoDataEntry saoDataEntry) {

        if (StringUtil.isEmpty(saoDataEntry.getTermEn())) {
            return Result.success().setMsg("数据不完整！");
        }
        saoDataEntry.setTermEn(saoDataEntry.getTermEn().replace(" ", ""));
        SaoDataEntry service = saoDataService.getById(saoDataEntry.getId());
        if (service == null) {
            return Result.success().setMsg("数据不存在！");
        }

        double noveltyC;
        double increaseC;
        double attentionC;
        double crossoverC;
        double interestsC;

        String[] split = service.getPreIds().split(",");
        for (String id : split) {
            SaoDataPreEntry saoDataPreEntry = preService.getById(id);
            Integer typeId = saoDataPreEntry.getType();
            String type = this.getType(Long.valueOf(typeId));
            Query query = new Query();

            query.accurate(SolrFiled.TYPE, type).and().left().fuzziness(SolrFiled.TITLE, saoDataEntry.getTermEn()).or().fuzziness(SolrFiled.ABSTRACT, saoDataEntry.getTermEn()).right();
            LinkedHashMap<String, Long> linkedHashMap = preService.getSaoYears(query, type);
            //新颖性
            double novelty = this.novelty(linkedHashMap, saoDataEntry.getId());
            //增长性
            double increase = this.increase(linkedHashMap, saoDataEntry.getId());


            double crossover = 0.0;
            double attention = 0.0;
            if (SolrFiled.PATENT.equals(type) || SolrFiled.THESIS.equals(type)) {
                //交叉性

                SolrQueryResponseEntity responseEntity = preService.getCrossover(query, type);
                crossover = this.crossover(responseEntity, saoDataEntry.getId());

                //关注度……
                attention = this.attention(query, saoDataPreEntry.getId(), type);
            }
            //价值性
            double interests = 0.0;
            if (SolrFiled.PROJECT.equals(type)) {
                Map<String, Object> amountToDate = preService.getInterestsAwardedAmountToDate(query);
                interests = this.interests(amountToDate, saoDataEntry.getId());
            }

            noveltyC = novelty;
            increaseC = increase;
            attentionC = attention;
            crossoverC = crossover;
            interestsC = interests;

            updateMysql(Integer.parseInt(id.trim()), noveltyC, increaseC, attentionC, crossoverC, interestsC);

        }
        afterCalculatingWeights(saoDataEntry);
        return Result.success();
    }


    private void afterCalculatingWeights(SaoDataEntry preEntry) {
        try {
            List<SaoDataPreEntry> list = preService.list(new QueryWrapper<SaoDataPreEntry>().eq("termEn", preEntry.getTermEn()));
            SaoDataEntry termEn = saoDataService.getOne(new QueryWrapper<SaoDataEntry>().eq("termEn", preEntry.getTermEn()));
            double noveltyC = 0.0;
            double increaseC = 0.0;
            double attentionC = 0.0;
            double crossoverC = 0.0;
            double interestsC = 0.0;
            int sumData = list.size();
            for (SaoDataPreEntry dataPreEntry : list) {
                Type type = typeService.getOne(new QueryWrapper<Type>().eq("id", dataPreEntry.getType()));

                noveltyC += dataPreEntry.getNovelty() * getWeight(list, sumData, type, 1);
                increaseC += dataPreEntry.getIncrease() * getWeight(list, sumData, type, 2);
                attentionC += dataPreEntry.getAttention() * getWeight(list, sumData, type, 3);
                crossoverC += dataPreEntry.getCrossover() * getWeight(list, sumData, type, 4);
                interestsC += dataPreEntry.getInterests() * getWeight(list, sumData, type, 5);
            }
            termEn.setNovelty(noveltyC);
            termEn.setIncrease(increaseC);
            termEn.setAttention(attentionC);
            termEn.setCrossover(crossoverC);
            termEn.setInterests(interestsC);
            saoDataService.updateById(termEn);
            //计算前沿度
            if (noveltyC > 0 || increaseC > 0 || attentionC > 0 || crossoverC > 0 || interestsC > 0) {
                this.frontier(termEn.getId());
            }
        } catch (Exception e) {
            logger.error("afterCalculatingWeights计算错误！", e);
        }
    }


    private String getType(Long typeId) {

        List<Type> list = typeService.list();
        for (Type type : list) {
            TYPE.put(type.getId(), type.getAlias());
            WEIGHT.put(type.getId(), type.getWeight());
        }

        return TYPE.get(typeId);
    }


    private void updateMysql(long preId, double novelty, double increase, double attention, double crossover,
                             double interests) {
        try {
            UpdateWrapper<SaoDataPreEntry> preEntryUpdateWrapper = new UpdateWrapper<SaoDataPreEntry>()
                    .set("novelty", novelty)
                    .set("increase", increase)
                    .set("attention", attention)
                    .set("crossover", crossover)
                    .set("interests", interests)
                    .eq("id", preId);
            preService.update(preEntryUpdateWrapper);
        } catch (Exception e) {
            logger.error("前置表修改错误！", e);
        }

    }

    /**
     * 新颖性
     *
     * @param linkedHashMap 数据
     * @param id            id
     */
    private double novelty(LinkedHashMap<String, Long> linkedHashMap, Long id) {
        try {
            double sumYear = 0.0;
            int quantity = 0;
            for (Map.Entry<String, Long> entry : linkedHashMap.entrySet()) {
                sumYear += Double.parseDouble(entry.getKey()) * entry.getValue();
                quantity += entry.getValue();
            }
            if (quantity > 0) {
                sumYear = sumYear / quantity;
                return sumYear;
            }
            return 0.0;
        } catch (Exception e) {
            logger.error("新颖性计算错误！", e);
            return 0;
        }
    }

    /**
     * 增长性
     *
     * @param linkedHashMap 数据
     * @param id            id
     */
    private double increase(LinkedHashMap<String, Long> linkedHashMap, Long id) {
        try {
            double n = n(linkedHashMap);
            n = n * 100;
            DecimalFormat df = new DecimalFormat("#.00");
            String aDouble = df.format(n);
            return Double.parseDouble(aDouble);
        } catch (Exception e) {
            logger.error("增长性计算错误！", e);
            return 0;
        }
    }


    /**
     * 交叉性
     *
     * @param entity 数据
     * @param id     id
     */
    private double crossover(SolrQueryResponseEntity entity, Long id) {

        try {
            if (entity != null && entity.getSolrDocuments().getNumFound() > 0) {
                double r = ((double) entity.getFieldStatsInfo().getCount() / (double) entity.getSolrDocuments().getNumFound());
                DecimalFormat df = new DecimalFormat("#.00");
                String aDouble = df.format(r);
                return Double.parseDouble(aDouble);
            }
            return 0.0;
        } catch (Exception e) {
            logger.error("交叉性计算错误！", e);
            return 0;
        }
    }

    /**
     * 价值性
     *
     * @param map 数据
     * @param id  id
     */
    private double interests(Map<String, Object> map, Long id) {
        try {
            if (map.size() > 0) {
                List<LinkedHashMap<String, Object>> linkedHashMaps = (List<LinkedHashMap<String, Object>>) map.get("data");
                if (linkedHashMaps.isEmpty()) {
                    return 0.0;
                }
                List<Double> collect = linkedHashMaps.stream().map(x -> {
                    String amount = String.valueOf(x.get("AwardedAmountToDate"));
                    String replace = amount.replace("$", "").replace(".00", "").replace(".", "").replace(",", "");
                    return Double.valueOf(replace);
                }).collect(Collectors.toList());
                List<Double> cycleYearList = linkedHashMaps.stream().map(x -> {
                    String amount = String.valueOf(x.get("cycleYear"));
                    return Double.valueOf(amount);
                }).collect(Collectors.toList());
                double sumCount = 0;
                for (int i = 0; i < collect.size(); i++) {
                    sumCount += collect.get(i) / cycleYearList.get(i);
                }
                return sumCount / collect.size();
            }
            return 0.0;
        } catch (Exception e) {
            logger.error("价值性计算错误！", e);
            return 0;
        }

    }


    /**
     * 关注度(基金项目,论文)
     *
     * @param query 数据
     * @param id    id
     */
    private double attention(Query query, Long id, String type) {

        try {

            //专利
            if (SolrFiled.PATENT.equals(type)) {
                SearchRequest searchRequest = SearchRequest.create(SolrCollection.HANDLE_PATENT);
                searchRequest.q(query.build());
                searchRequest.statsFiled("CC");
                FieldStatsInfo stats = SolrUtils.stats(searchRequest);
                if (stats.getCount() > 0) {
                    return (Double) stats.getMean();
                }
                return 0.0;

            } else {
                LinkedHashMap<String, Double> yearWeight = new LinkedHashMap<>();
                yearWeight.put("2017", (1d / 15d));
                yearWeight.put("2018", (2d / 15d));
                yearWeight.put("2019", (3d / 15d));
                yearWeight.put("2020", (4d / 15d));
                yearWeight.put("2021", (5d / 15d));
                yearWeight.put("2022", (6d / 15d));
                AtomicReference<Double> a = new AtomicReference<>(0.0);
                SearchRequest searchRequest = SearchRequest.create(SolrCollection.HANDLE_THESIS);
                for (Map.Entry<String, Double> stringDoubleEntry : yearWeight.entrySet()) {
                    String key = stringDoubleEntry.getKey();
                    searchRequest.q(query.build() + " AND PublicationYear:" + key);
                    searchRequest.statsFiled("CitedCount");
                    FieldStatsInfo stats = SolrUtils.stats(searchRequest);
                    if (stats.getCount() > 0) {
                        a.updateAndGet(v1 -> v1 + (stringDoubleEntry.getValue() * (stats.getSum() == null ? 0.0 : (Double) stats.getSum())) / (stats.getCount() == null ? 0.0 : (stats.getCount())));
                    }

                }
                return a.get();
            }
        } catch (Exception e) {
            logger.error("关注度计算错误！", e);
            return 0;
        }

    }

    private void frontier(Long id) {

        HashMap<String, Double> average = getAverage();
        SaoDataEntry preEntry = saoDataService.getById(id);
        if (preEntry == null) {
            return;
        }
        List<Long> typeList = new ArrayList<>();
        for (String preId : preEntry.getPreIds().split(",")) {
            Integer type = saoDataPreService.getById(preId).getType();
            Type typeServiceById = typeService.getById(type);
            typeList.add(typeServiceById.getId());
        }
        int f;
        if (typeList.size() > 0) {
            boolean containProject = typeList.contains(2L);
            boolean containThesis = typeList.contains(3L);
            boolean containPatent = typeList.contains(1L);
            if (containProject && !containThesis && !containPatent) {
                //只存在于基金
                f = 4;
            } else {
                if (preEntry.getNovelty() < average.get(NOVELTY)) {
                    //新颖性低于平均值
                    f = 1;
                } else {
                    if (preEntry.getAttention() > average.get(ATTENTION)) {
                        //关注度高于平均值
                        f = 2;
                    } else {
                        if (preEntry.getIncrease() > average.get(INCREASE)) {
                            //增长性高于平均值
                            f = 3;
                        } else {
                            if (preEntry.getCrossover() > average.get(CROSSOVER)) {
                                //交叉性高于平均值
                                f = 5;
                            } else {
                                f = 4;
                            }
                        }
                    }
                }
            }
            preEntry.setFrontier(f);
            saoDataService.update(new UpdateWrapper<SaoDataEntry>().set("frontier", f).eq("id", preEntry.getId()));
        }
    }

    private HashMap<String, Double> getAverage() {
        List<SaoDataEntry> list = saoDataService.list();
        HashMap<String, Double> averageHashMap = new HashMap<>(5);

        List<Double> collectNovelty = list.stream().map(SaoDataEntry::getNovelty).filter(x -> x != 0.0).collect(Collectors.toList());
        double noveltyAverage = collectNovelty.stream().mapToDouble(Double::doubleValue).average().orElse(0.0);
        averageHashMap.put(NOVELTY, noveltyAverage);

        List<Double> collectIncrease = list.stream().map(SaoDataEntry::getIncrease).filter(x -> x != 0.0).collect(Collectors.toList());
        double increaseAverage = collectIncrease.stream().mapToDouble(Double::doubleValue).average().orElse(0.0);
        averageHashMap.put(INCREASE, increaseAverage);

        List<Double> collectAttention = list.stream().map(SaoDataEntry::getAttention).filter(x -> x != 0.0).collect(Collectors.toList());
        double attentionAverage = collectAttention.stream().mapToDouble(Double::doubleValue).average().orElse(0.0);
        averageHashMap.put(ATTENTION, attentionAverage);

        List<Double> collectCrossover = list.stream().map(SaoDataEntry::getCrossover).filter(x -> x != 0.0).collect(Collectors.toList());
        double crossoverAverage = collectCrossover.stream().mapToDouble(Double::doubleValue).average().orElse(0.0);
        averageHashMap.put(CROSSOVER, crossoverAverage);

        List<Double> collectInterests = list.stream().map(SaoDataEntry::getInterests).filter(x -> x != 0.0).collect(Collectors.toList());
        double interestsAverage = collectInterests.stream().mapToDouble(Double::doubleValue).average().orElse(0.0);
        averageHashMap.put(INTERESTS, interestsAverage);
        return averageHashMap;
    }


    /**
     * 增长性规则
     *
     * @param linkedHashMap 数据
     * @return double
     */
    private double n(LinkedHashMap<String, Long> linkedHashMap) {
        linkedHashMap = linkedHashMap.entrySet().stream().sorted(Map.Entry.comparingByKey()).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        double num = 0.0;
        Long preLength = null;

        for (Map.Entry<String, Long> entry : linkedHashMap.entrySet()) {
            if (linkedHashMap.size() == 1) {
                num = 1L;
            } else {
                if (preLength != null) {
                    if (preLength != 0) {
                        num = num + ((entry.getValue() - preLength) / (double) (preLength));
                    }
                }
                preLength = entry.getValue();
            }
        }
        return num / 5;
    }


    /**
     * 查询批次表
     */
    @RequestMapping(value = "/selectSaoPretreatment")
    public Result selectSaoPretreatment(@RequestBody HashMap<String, Integer> patchQuery, @RequestParam("param") String param) {
        IPage<SaoPretreatment> iPage = new Page<>(patchQuery.get("pagerNum"), patchQuery.get("pageSize"));
        try {
            IPage<SaoPretreatment> page;
            if (StringUtil.isEmpty(param)) {
                page = saoPretreatmentService.page(iPage, new QueryWrapper<SaoPretreatment>().eq("move", 0));
            } else {
                page = saoPretreatmentService.page(iPage, new QueryWrapper<SaoPretreatment>()
                        .eq("move", 0)
                        .like("UPPER(termEN)", param.toUpperCase()));
            }
            return Result.success(page);
        } catch (Exception e) {
            logger.error("查询sao失败!", e);
            return Result.error(EnumResultStates.SERVER_ERROR);
        }
    }


    @RequestMapping(value = "/createSAO")
    public Result testBd(@RequestBody List<Integer> ids) {
        List<SaoPretreatment> saoPretreatments = saoPretreatmentService.listByIds(ids);
        try {
            for (SaoPretreatment saoPretreatment : saoPretreatments) {
                String termEN = saoPretreatment.getTermEN();
                String transResult = transApi.getTransResult(termEN, "auto", "zh");
                logger.info("翻译接口：{}", transResult);
                Thread.sleep(1300);
                JSONObject entries = JSONUtil.parseObj(transResult);
                String errorCode = (String) entries.get("error_code");
                if (errorCode != null) {
                    if (errorCode.equals("54004")) {
                        return Result.error(EnumResultStates.SERVER_CANNOT);
                    } else {
                        return Result.error(EnumResultStates.SERVER_ERROR);
                    }
                }
                JSONArray transList = entries.getJSONArray("trans_result");
                String termCn = transList.getJSONObject(0).getStr("dst");
                SaoDataPreEntry saoDataPreEntry = new SaoDataPreEntry(0, termEN, termCn);
                if (saoPretreatment.getResourcesPatent() == 1) {
                    saoDataPreEntry.setType(1);
                    add(saoDataPreEntry);
                }
                if (saoPretreatment.getResourcesProject() == 1) {
                    saoDataPreEntry.setType(2);
                    add(saoDataPreEntry);
                }
                if (saoPretreatment.getResourcesThesis() == 1) {
                    saoDataPreEntry.setType(3);
                    add(saoDataPreEntry);
                }
                saoPretreatment.setMove(1);
                saoPretreatmentService.updateById(saoPretreatment);
            }
            return Result.success("sao词添加成功！");
        } catch (Exception e) {
            logger.error("sao预处理生成sao异常!", e);
            return new Result(false, 200, "sao词添加失败,服务异常!", null);
        }
    }

    @PostMapping("/upload")
    public Result upload(@RequestParam("file") MultipartFile file) {
        Workbook workbook = null;
        String fileName = file.getOriginalFilename();
        try {
            if (fileName.endsWith(ENDING_XLS)) {
                workbook = new HSSFWorkbook(file.getInputStream());
            } else if (fileName.endsWith(ENDING_XLSX)) {
                workbook = new XSSFWorkbook(file.getInputStream());
            } else {
                logger.warn("excel格式不符 === << {} >>  ", fileName);
                return Result.error(EnumResultStates.ILLEGAL_FILETYPE_EXCEPTION_ERROR);
            }
            List<SaoDataPreEntry> saoDataPreEntries = saoDataService.saoUpload(workbook);
            saoDataPreEntries.forEach(entry -> add(entry));
        } catch (Exception e) {
            logger.error("excel解释异常！", e);
            return Result.error(EnumResultStates.EXCEL_DATA_IMPORT_ERROR);
        }
        return Result.success();
    }


}
