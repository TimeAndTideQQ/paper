package com.wanfangdata.topic.entry.treestructure;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @ClassName NodeStructure
 * @Author ZhouZhM
 * @Date 2023/3/8 14:16
 * @Description 节点结构实体类
 * @Version 1.0
 */
public class NodeStructure implements Serializable {

    /**
     * 节点id
     */
    private int id;

    /**
     * 节点关系
     */
    private String nodeRelation;

    /**
     * 节点等级
     */
    private int nodeGrade;

    /**
     * saoId
     */
    private int saoId;

    /**
     * sao(节点)中文名称
     */
    private String termCn;

    /**
     * sao（节点）英文名称
     */
    private String termEn;

    /**
     * 前沿度名称
     */
    private String frontName;

    /**
     * 前沿度内容
     */
    private String frontComments;

    /**
     * 前沿度类型
     */
    private String frontType;

    /**
     * 父级Id
     */
    private int pId;

    /**
     * 树Id
     */
    private int treeId;

    /**
     * 新颖性
     */
    private double novelty;

    /**
     * 增长性
     */
    private double increase;

    /**
     * 关注度
     */
    private double attention;

    /**
     * 交叉性
     */
    private double crossover;

    /**
     * 价值性
     */
    private double interests;

    /**
     * 坐标
     */
    private String coordinate;

    /**
     * 交叉关系（json串）
     */
    private String crossRelation;

    public NodeStructure() {
    }

    public NodeStructure(int id, String nodeRelation, int nodeGrade, int saoId, String termCn, String termEn, String frontName, String frontComments, String frontType, int pId, int treeId, double novelty, double increase, double attention, double crossover, double interests, String coordinate, String crossRelation) {
        this.id = id;
        this.nodeRelation = nodeRelation;
        this.nodeGrade = nodeGrade;
        this.saoId = saoId;
        this.termCn = termCn;
        this.termEn = termEn;
        this.frontName = frontName;
        this.frontComments = frontComments;
        this.frontType = frontType;
        this.pId = pId;
        this.treeId = treeId;
        this.novelty = novelty;
        this.increase = increase;
        this.attention = attention;
        this.crossover = crossover;
        this.interests = interests;
        this.coordinate = coordinate;
        this.crossRelation = crossRelation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNodeRelation() {
        return nodeRelation;
    }

    public void setNodeRelation(String nodeRelation) {
        this.nodeRelation = nodeRelation;
    }

    public int getNodeGrade() {
        return nodeGrade;
    }

    public void setNodeGrade(int nodeGrade) {
        this.nodeGrade = nodeGrade;
    }

    public int getSaoId() {
        return saoId;
    }

    public void setSaoId(int saoId) {
        this.saoId = saoId;
    }

    public String getTermCn() {
        return termCn;
    }

    public void setTermCn(String termCn) {
        this.termCn = termCn;
    }

    public String getTermEn() {
        return termEn;
    }

    public void setTermEn(String termEn) {
        this.termEn = termEn;
    }

    public String getFrontName() {
        return frontName;
    }

    public void setFrontName(String frontName) {
        this.frontName = frontName;
    }

    public String getFrontComments() {
        return frontComments;
    }

    public void setFrontComments(String frontComments) {
        this.frontComments = frontComments;
    }

    public String getFrontType() {
        return frontType;
    }

    public void setFrontType(String frontType) {
        this.frontType = frontType;
    }

    public int getpId() {
        return pId;
    }

    public void setpId(int pId) {
        this.pId = pId;
    }

    public int getTreeId() {
        return treeId;
    }

    public void setTreeId(int treeId) {
        this.treeId = treeId;
    }

    public double getNovelty() {
        return novelty;
    }

    public void setNovelty(double novelty) {
        this.novelty = novelty;
    }

    public double getIncrease() {
        return increase;
    }

    public void setIncrease(double increase) {
        this.increase = increase;
    }

    public double getAttention() {
        return attention;
    }

    public void setAttention(double attention) {
        this.attention = attention;
    }

    public double getCrossover() {
        return crossover;
    }

    public void setCrossover(double crossover) {
        this.crossover = crossover;
    }

    public double getInterests() {
        return interests;
    }

    public void setInterests(double interests) {
        this.interests = interests;
    }

    public String getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(String coordinate) {
        this.coordinate = coordinate;
    }

    public String getCrossRelation() {
        return crossRelation;
    }

    public void setCrossRelation(String crossRelation) {
        this.crossRelation = crossRelation;
    }
}
