package com.wanfangdata.topic.entry.treestructure;

/**
 * @ClassName CrossRelationRequest
 * @Author ZhouZhM
 * @Date 2023/4/6 10:20
 * @Description 节点交叉关系请求实体
 * @Version 1.0
 */
public class CrossRelationRequest {

    /**
     * 树Id
     */
    private int treeId;

    /**
     * 节点id
     */
    private int id;

    /**
     * 节点交叉关系（json串）
     */
    private String crossRelation;

    public CrossRelationRequest() {
    }

    public CrossRelationRequest(int treeId, int id, String crossRelation) {
        this.treeId = treeId;
        this.id = id;
        this.crossRelation = crossRelation;
    }

    public int getTreeId() {
        return treeId;
    }

    public void setTreeId(int treeId) {
        this.treeId = treeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCrossRelation() {
        return crossRelation;
    }

    public void setCrossRelation(String crossRelation) {
        this.crossRelation = crossRelation;
    }
}
