package com.wanfangdata.topic.entry.treestructure;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * @ClassName NodeRelationEntry
 * @Author ZhouZhM
 * @Date 2023/3/9 8:18
 * @Description 节点关系实体类
 * @Version 1.0
 */
@TableName("nodeRelation")
public class NodeRelationEntry implements Serializable {

    /**
     * 节点id
     */
    @TableId(value = "id", type = IdType.INPUT)
    private int id;

    /**
     * 关系名称
     */
    private String name;

    public NodeRelationEntry() {
    }

    public NodeRelationEntry(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
