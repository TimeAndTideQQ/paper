package com.wanfangdata.topic.entry.sao;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


/**
 * @Description sao
 * @Authors smj
 * @Date 2023/3/3 12:04
 * @Version
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("saodatapre")
public class SaoDataPreEntry {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private Integer type;
    private String termEn;
    private String termCn;
    private double novelty;
    private double increase;
    private double attention;
    private double crossover;
    private double interests;
    private Integer frontier;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    @TableField(exist = false)
    private Type types;


    public SaoDataPreEntry(Integer type, String termEn, String termCn) {
        this.type = type;
        this.termEn = termEn;
        this.termCn = termCn;
    }

    @Override
    public String toString() {
        return "SaoDataPreEntry{" +
                "id=" + id +
                ", type=" + type +
                ", termEn='" + termEn + '\'' +
                ", termCn='" + termCn + '\'' +
                ", novelty=" + novelty +
                ", increase=" + increase +
                ", attention=" + attention +
                ", crossover=" + crossover +
                ", interests=" + interests +
                ", frontier=" + frontier +
                ", createTime=" + createTime +
                '}' + "\n";
    }
}
