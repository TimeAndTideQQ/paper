package com.wanfangdata.topic.entry.treestructure;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * @ClassName TreeEntry
 * @Author ZhouZhM
 * @Date 2023/3/16 10:59
 * @Description 树实体类
 * @Version 1.0
 */
@TableName("tree")
public class TreeEntry implements Serializable {

    /**
     * 节点id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private int id;

    /**
     * 树名称
     */
    private String name;

    /**
     * 创建者
     */
    private String creator;

    /**
     * saoId（树根）
     */
    private int saoId;

    /**
     * 项目名称（模块名称）
     */
    private String module;

    /**
     * 前端样式
     */
    private String style;

    public TreeEntry() {
    }

    public TreeEntry(int id, String name, String creator, int saoId, String module, String style) {
        this.id = id;
        this.name = name;
        this.creator = creator;
        this.saoId = saoId;
        this.module = module;
        this.style = style;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public int getSaoId() {
        return saoId;
    }

    public void setSaoId(int saoId) {
        this.saoId = saoId;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
}
