package com.wanfangdata.topic.entry.treestructure;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @ClassName TreeEntry
 * @Author ZhouZhM
 * @Date 2023/3/7 15:45
 * @Description 树结构实体类
 * @Version 1.0
 */
@TableName("treeStructure")
public class TreeStructureEntry implements Serializable {

    /**
     * 节点id
     */
    @TableId(value = "id", type = IdType.INPUT)
    private int id;

    /**
     * 节点关系Id
     */
    private int nodeRelation;

    /**
     * 节点等级
     */
    private int nodeGrade;

    /**
     * saoId
     */
    private int saoId;

    /**
     * 父级Id
     */
    private int pId;

    /**
     * 树Id
     */
    private int treeId;

    public TreeStructureEntry() {
    }

    public TreeStructureEntry(int id, int nodeRelation, int nodeGrade, int saoId, int pId, int treeId) {
        this.id = id;
        this.nodeRelation = nodeRelation;
        this.nodeGrade = nodeGrade;
        this.saoId = saoId;
        this.pId = pId;
        this.treeId = treeId;
    }

    public TreeStructureEntry(int nodeRelation, int nodeGrade, int saoId, int pId, int treeId) {
        this.nodeRelation = nodeRelation;
        this.nodeGrade = nodeGrade;
        this.saoId = saoId;
        this.pId = pId;
        this.treeId = treeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNodeRelation() {
        return nodeRelation;
    }

    public void setNodeRelation(int nodeRelation) {
        this.nodeRelation = nodeRelation;
    }

    public int getNodeGrade() {
        return nodeGrade;
    }

    public void setNodeGrade(int nodeGrade) {
        this.nodeGrade = nodeGrade;
    }

    public int getSaoId() {
        return saoId;
    }

    public void setSaoId(int saoId) {
        this.saoId = saoId;
    }

    public int getpId() {
        return pId;
    }

    public void setpId(int pId) {
        this.pId = pId;
    }

    public int getTreeId() {
        return treeId;
    }

    public void setTreeId(int treeId) {
        this.treeId = treeId;
    }
}
