package com.wanfangdata.topic.entry.sao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 圣excel上传
 *
 * @author wangyou
 * @description
 * @date 2023/05/15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("saoExcelUpload")
public class SaoExcelUpload {

    @TableId(value = "id", type = IdType.AUTO)
    private long id;
    private String termEN;
    private String termCN;
    private int resourcesPatent;
    private int resourcesProject;
    private int resourcesThesis;

}
