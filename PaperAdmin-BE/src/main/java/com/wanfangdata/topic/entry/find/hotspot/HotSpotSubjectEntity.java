package com.wanfangdata.topic.entry.find.hotspot;

import org.apache.solr.client.solrj.beans.Field;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * @Description 学科热点
 * @Authors smj
 * @Date 2023/2/19 12:04
 * @Version
 */
public class HotSpotSubjectEntity implements Serializable {


    /**
     * 学科号
     */
    @Field("educationcode")
    private String educationCode;

    /**
     * 关键词
     */
    @Field("newword")
    private String keyword;
    /**
     * 关键词数量
     */
    @Field("count")
    private Long count;
    /**
     * 2018数量
     */
    @Field("y2018")
    private Long y2018;
    /**
     * 2019数量
     */
    @Field("y2019")
    private Long y2019;
    /**
     * 2020数量
     */
    @Field("y2020")
    private Long y2020;
    /**
     * 2021数量
     */
    @Field("y2021")
    private Long y2021;
    /**
     * 2022数量
     */
    @Field("y2022")
    private Long y2022;


    public List<Long> getYearList() {
        List<Long> quantity = new ArrayList<>(5);
        quantity.add(this.getY2018());
        quantity.add(this.getY2019());
        quantity.add(this.getY2020());
        quantity.add(this.getY2021());
        quantity.add(this.getY2022());
        return quantity;
    }

    public String getEducationCode() {
        return educationCode;
    }

    public HotSpotSubjectEntity setEducationCode(String educationCode) {
        this.educationCode = educationCode;
        return this;
    }


    public String getKeyword() {
        return keyword;
    }

    public HotSpotSubjectEntity setKeyword(String keyword) {
        this.keyword = keyword;
        return this;
    }

    public Long getY2018() {
        return y2018;
    }


    public Long getY2019() {
        return y2019;
    }


    public Long getY2020() {
        return y2020;
    }

    public Long getY2021() {
        return y2021;
    }


    public Long getY2022() {
        return y2022;
    }


    public Long getCount() {
        return count;
    }

    public HotSpotSubjectEntity setCount(Long count) {
        this.count = count;
        return this;
    }

    @Override
    public String toString() {
        return "HotSpotSubjectEntity{" +
                "educationCode='" + educationCode + '\'' +
                ", keyword='" + keyword + '\'' +
                ", count=" + count +
                ", y2018=" + y2018 +
                ", y2019=" + y2019 +
                ", y2020=" + y2020 +
                ", y2021=" + y2021 +
                ", y2022=" + y2022 +
                '}';
    }
}

