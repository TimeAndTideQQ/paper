package com.wanfangdata.topic.entry.sao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description 资源类型
 * @Authors smj
 * @Version
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("type")
public class Type {

  @TableId(value = "id", type = IdType.AUTO)
  private long id;
  private String name;
  private String alias;
  private double weight;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public String getAlias() {
    return alias;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }


  public double getWeight() {
    return weight;
  }

  public void setWeight(double weight) {
    this.weight = weight;
  }

}
