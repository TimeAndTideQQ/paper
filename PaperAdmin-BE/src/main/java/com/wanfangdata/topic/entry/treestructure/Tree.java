package com.wanfangdata.topic.entry.treestructure;

import java.io.Serializable;

/**
 * @ClassName Tree
 * @Author ZhouZhM
 * @Date 2023/3/16 14:56
 * @Description
 * @Version 1.0
 */
public class Tree implements Serializable {
    /**
     * 节点id
     */
    private int id;

    /**
     * 树名称
     */
    private String name;

    /**
     * 创建者
     */
    private String creator;

    /**
     * 根节点名称
     */
    private String rootName;

    /**
     * 项目名称（模块名称）
     */
    private String module;

    /**
     * 前端样式
     */
    private String style;

    public Tree() {
    }

    public Tree(int id, String name, String creator, String rootName, String module, String style) {
        this.id = id;
        this.name = name;
        this.creator = creator;
        this.rootName = rootName;
        this.module = module;
        this.style = style;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getRootName() {
        return rootName;
    }

    public void setRootName(String rootName) {
        this.rootName = rootName;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
}
