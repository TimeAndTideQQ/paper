package com.wanfangdata.topic.entry.sao;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author smj
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("saoPretreatment")
public class SaoPretreatment {

  @TableId(value = "id", type = IdType.AUTO)
  private long id;
  private int pcid;
  private String termEN;
  private String termCN;
  private long move;
  private int resourcesPatent;
  private int resourcesProject;
  private int resourcesThesis;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public int getPcid() {
    return pcid;
  }

  public void setPcid(int pcid) {
    this.pcid = pcid;
  }

  public String getTermEN() {
    return termEN;
  }

  public void setTermEN(String termEN) {
    this.termEN = termEN;
  }

  public String getTermCN() {
    return termCN;
  }

  public void setTermCN(String termCN) {
    this.termCN = termCN;
  }

  public long getMove() {
    return move;
  }

  public void setMove(long move) {
    this.move = move;
  }

  public int getResourcesPatent() {
    return resourcesPatent;
  }

  public void setResourcesPatent(int resourcesPatent) {
    this.resourcesPatent = resourcesPatent;
  }

  public int getResourcesProject() {
    return resourcesProject;
  }

  public void setResourcesProject(int resourcesProject) {
    this.resourcesProject = resourcesProject;
  }

  public int getResourcesThesis() {
    return resourcesThesis;
  }

  public void setResourcesThesis(int resourcesThesis) {
    this.resourcesThesis = resourcesThesis;
  }
}
