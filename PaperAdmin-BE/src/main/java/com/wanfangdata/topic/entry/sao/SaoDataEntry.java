package com.wanfangdata.topic.entry.sao;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * @Description sao
 * @Authors smj
 * @Date 2023/3/3 12:04
 * @Version
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("saodata")
public class SaoDataEntry implements Serializable {

  @TableId(value = "id", type = IdType.AUTO)
  private Long id;
  private String termEn;
  private String termCn;
  private double novelty;
  private double increase;
  private double attention;
  private double crossover;
  private double interests;
  private Integer frontier;
  private String isShow;
  private String preIds;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone ="GMT+8")
  private Date createTime;



  /**
   * sql不查询字段
   */
  @TableField(exist = false)
  private SaoFrontier saoFrontier;
  /**
   * sql不查询字段
   */
  @TableField(exist = false)
  private String types;

  @TableField(exist = false)
  private List<SaoDataPreEntry> saoDataPreEntryList;

  @Override
  public String toString() {
    return "SaoDataEntry{" +
            "id=" + id +
            ", termEn='" + termEn + '\'' +
            ", termCn='" + termCn + '\'' +
            ", novelty=" + novelty +
            ", increase=" + increase +
            ", attention=" + attention +
            ", crossover=" + crossover +
            ", interests=" + interests +
            ", frontier=" + frontier +
            ", isShow='" + isShow + '\'' +
            ", preIds='" + preIds + '\'' +
            ", createTime=" + createTime +
            ", saoFrontier=" + saoFrontier +
            ", types='" + types + '\'' +
            '}' + "\n";
  }
}
