package com.wanfangdata.topic.entry.batch;

import cn.hutool.log.Log;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName BatchEntry
 * @Author ZhouZhM
 * @Date 2023/3/23 10:04
 * @Description 批次表实体类
 * @Version 1.0
 */
@TableName("batch")
public class BatchEntry implements Serializable {

    /**
     * 批次号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private int id;

    /**
     * 导入时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date time;

    /**
     * 导入时间
     */
    @TableField(fill = FieldFill.INSERT)
    private int status;

    /**
     * 导入时间
     */
    @TableField(fill = FieldFill.INSERT)
    private String name;

    @TableField(exist = false)
    private long sum;

    public BatchEntry() {
    }

    public BatchEntry(Date time, String name) {
        this.time = time;
        this.name = name;
    }

    public BatchEntry(int id, Date time, int status, String name, long sum) {
        this.id = id;
        this.time = time;
        this.status = status;
        this.name = name;
        this.sum = sum;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSum() {
        return sum;
    }

    public void setSum(long sum) {
        this.sum = sum;
    }
}
