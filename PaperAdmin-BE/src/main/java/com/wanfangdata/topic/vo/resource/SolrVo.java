package com.wanfangdata.topic.vo.resource;

/**
 * Solr
 *
 * @author Administrator
 * @date 2023/03/03 12:31
 **/
public class SolrVo {

    private String id;
    private String keyword;
    private String type;

    public SolrVo() {
    }

    public SolrVo(String id, String keyword, String type) {
        this.id = id;
        this.keyword = keyword;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setId(String id) {
        this.id = id;
    }
}
