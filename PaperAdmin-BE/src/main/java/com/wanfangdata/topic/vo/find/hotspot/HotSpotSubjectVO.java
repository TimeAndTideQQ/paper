package com.wanfangdata.topic.vo.find.hotspot;


import java.util.List;

/**
 * @Description 学科热点 传输数据到WEB页面视图层
 * @Authors smj
 * @Date 2023/2/19 16:12
 * @Version
 */
public class HotSpotSubjectVO {

    /**
     * 学科号
     */
    private String educationCode;

    /**
     * 主题词集合（3年）
     */
    private List<String> keywords;
    /**
     * 研究趋势集合（5年）
     */
    private List<List<Long>> quantity;
    /**
     * 指数集合
     */
    private List<Long> index;

    public String getEducationCode() {
        return educationCode;
    }

    public HotSpotSubjectVO setEducationCode(String educationCode) {
        this.educationCode = educationCode;
        return this;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public HotSpotSubjectVO setKeywords(List<String> keywords) {
        this.keywords = keywords;
        return this;
    }

    public List<List<Long>> getQuantity() {
        return quantity;
    }

    public HotSpotSubjectVO setQuantity(List<List<Long>> quantity) {
        this.quantity = quantity;
        return this;
    }

    public List<Long> getIndex() {
        return index;
    }

    public HotSpotSubjectVO setIndex(List<Long> index) {
        this.index = index;
        return this;
    }

    @Override
    public String toString() {
        return "HotSpotSubjectVO{" +
                "educationCode='" + educationCode + '\'' +
                ", keywords=" + keywords +
                ", quantity=" + quantity +
                ", index=" + index +
                '}';
    }
}
