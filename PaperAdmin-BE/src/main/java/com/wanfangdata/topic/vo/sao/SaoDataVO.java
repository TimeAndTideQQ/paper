package com.wanfangdata.topic.vo.sao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.solr.client.solrj.beans.Field;

import java.io.Serializable;

/**
 * @Description vo
 * @Authors smj
 * @Date 2023/3/4 14:32
 * @Version
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SaoDataVO implements Serializable {

    @Field("Id")
    public String id;
    @Field("Title")
    public String title;
    @Field("Abstract")
    public String Abstract;


}
