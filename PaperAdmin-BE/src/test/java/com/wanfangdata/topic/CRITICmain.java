package com.wanfangdata.topic;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanfangdata.topic.entry.sao.SaoDataPreEntry;
import com.wanfangdata.topic.service.sao.ISaoDataPreService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class CRITICmain {

    @Autowired
    private ISaoDataPreService saoDataPreService;

    @Test
    public void testCritic() {
        IPage<SaoDataPreEntry> iPage = new Page<>(0, 324);
        List<SaoDataPreEntry> records = saoDataPreService.page(iPage, new QueryWrapper<>()).getRecords();
        List<SaoDataPreEntry> c = new ArrayList<>();
        for (SaoDataPreEntry record : records) {
            List<SaoDataPreEntry> termEN = saoDataPreService.list(new QueryWrapper<SaoDataPreEntry>().eq("termEN", record.getTermEn()));
            if (termEN.size() == 3) {
                c.addAll(termEN);
            }
        }


        List<List<Double>> patentDoubleList = new ArrayList<>();
        List<List<Double>> projectDoubleList = new ArrayList<>();
        List<List<Double>> thesisDoubleList = new ArrayList<>();

        for (SaoDataPreEntry dataPreEntry : c) {
            if (dataPreEntry.getType().equals(1)) {
                List<Double> patentCollection = new ArrayList<>();
                patentCollection.add(dataPreEntry.getNovelty());
                patentCollection.add(dataPreEntry.getIncrease());
                patentCollection.add(dataPreEntry.getAttention());
                patentCollection.add(dataPreEntry.getCrossover());
                patentDoubleList.add(patentCollection);
            } else if (dataPreEntry.getType().equals(2)) {
                List<Double> projectCollection = new ArrayList<>();
                projectCollection.add(dataPreEntry.getNovelty());
                projectCollection.add(dataPreEntry.getIncrease());
                projectCollection.add(dataPreEntry.getInterests() / 10000);
                projectDoubleList.add(projectCollection);
            } else if (dataPreEntry.getType().equals(3)) {
                List<Double> thesisCollection = new ArrayList<>();
                thesisCollection.add(dataPreEntry.getNovelty());
                thesisCollection.add(dataPreEntry.getIncrease());
                thesisCollection.add(dataPreEntry.getAttention());
                thesisCollection.add(dataPreEntry.getCrossover());
                thesisDoubleList.add(thesisCollection);
            }
        }
        List<Double> frontierPatentMatrix = frontierMatrix(patentDoubleList, 1);
        List<Double> frontierProjectMatrix = frontierMatrix(projectDoubleList, 2);
        List<Double> frontierThesisMatrix = frontierMatrix(thesisDoubleList, 3);

        List<List<Double>> sampleList = new ArrayList<>();
        for (int i = 0; i < frontierPatentMatrix.size(); i++) {
            List<Double> sample = new ArrayList<>();
            sample.add(frontierPatentMatrix.get(i));
            sample.add(frontierProjectMatrix.get(i));
            sample.add(frontierThesisMatrix.get(i));
            sampleList.add(sample);
        }
        System.out.println("sampleList ====================================== " + sampleList);
        calculate(listToDimensionalArray(sampleList));
    }

    private double[][] listToDimensionalArray(List<List<Double>> d) {
        double[][] resultArray = new double[d.size()][];
        for (int i = 0; i < d.size(); i++) {
            List<Double> innerList = d.get(i);
            double[] innerArray = new double[innerList.size()];
            for (int j = 0; j < innerList.size(); j++) {
                innerArray[j] = innerList.get(j);
            }
            resultArray[i] = innerArray;
        }
        return resultArray;
    }

    private List<Double> frontierMatrix(List<List<Double>> d, int type) {
        CRITIC critic = new CRITIC();

        double[][] componentMatrix = listToDimensionalArray(d);
        System.out.println("--------------------原始数据矩阵---------------------");
//        critic.matrixoutput(componentMatrix);

        List<Integer> negList = new ArrayList<>();
        if (type == 1 || type == 2) {
            negList.add(1);
            negList.add(2);
        } else if (type == 3) {
            negList.add(1);
        }
        double[][] normalizedMatrix = critic.normalized(componentMatrix, negList);
        List<List<Double>> matrixOutput = critic.matrixoutput(normalizedMatrix);
        List<Double> outReduce = new ArrayList<>();
        for (List<Double> list : matrixOutput) {
            Double reduce = list.stream().filter(x -> !x.isNaN()).reduce(0.0, Double::sum);
            outReduce.add(reduce);
        }
        return outReduce;
    }


    private void calculate(double[][] componentMatrix) {
        CRITIC critic = new CRITIC();


        //critic.matrixoutput(componentMatrix);

        double[][] normalizedMatrix = critic.normalized(componentMatrix, null);
        System.out.println("--------------------标准化数据矩阵---------------------");

        //critic.matrixoutput(normalizedMatrix);

        double[][] pearson = critic.correlation(normalizedMatrix);
        System.out.println("--------------------皮尔逊相关系数矩阵---------------------");

        //critic.matrixoutput(pearson);

        double[] informationVolume = critic.information(normalizedMatrix, pearson);
        System.out.println("--------------------指标信息承载量---------------------");
        critic.matrixoutput1(informationVolume);

        double[] weight = critic.weight(informationVolume);
        System.out.println("--------------------指标权重---------------------");
        critic.matrixoutput1(weight);


    }
}