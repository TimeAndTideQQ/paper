package com.wanfangdata.topic;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.wanfangdata.topic.constant.SolrCollection;
import com.wanfangdata.topic.constant.SolrFiled;
import com.wanfangdata.topic.entry.sao.SaoDataEntry;
import com.wanfangdata.topic.entry.sao.SaoDataPreEntry;
import com.wanfangdata.topic.service.sao.ISaoDataPreService;
import com.wanfangdata.topic.service.sao.ISaoDataService;
import com.wanfangdata.topic.solr.Query;
import com.wanfangdata.topic.solr.SolrUtils;
import com.wanfangdata.topic.solr.execute.facet.FacetRequest;
import org.apache.solr.client.solrj.SolrClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

@SpringBootTest
class FindApplicationTests {

    @Autowired
    private SolrClient solrClient;

    @Autowired
    private ISaoDataService saoDataService;

    @Autowired
    private ISaoDataPreService preService;


    @Test
    void contextLoads() {
        List<SaoDataEntry> saoData = saoDataService.list();
        List<SaoDataEntry> collect = saoData.stream().peek(x -> {
            x.setTermCn(x.getTermCn().trim());
            x.setTermEn(x.getTermEn().trim());
        }).collect(Collectors.toList());
        for (SaoDataEntry data : collect) {
            data.setCreateTime(new Date());
            boolean b = saoDataService.updateById(data);
            System.out.println("b = " + b);
        }

    }


    //计算新颖
    @Test
    void novelty() {
        List<SaoDataPreEntry> list = preService.list();
        for (SaoDataPreEntry saoDataPreEntry : list) {
            Map<Integer, String> map = new HashMap<>();
            map.put(1, "Patent");//【专利】
            map.put(2, "Project");//【基金】
            map.put(3, "Thesis");//【论文】
            Query query = new Query();
            query.accurate(SolrFiled.TYPE, map.get(saoDataPreEntry.getType()))
                    .and()
                    .left()
                    .fuzziness(SolrFiled.TITLE, saoDataPreEntry.getTermEn())
                    .or()
                    .fuzziness(SolrFiled.ABSTRACT, saoDataPreEntry.getTermEn())
                    .right();
            LinkedHashMap<String, Long> saoYears = preService.getSaoYears(query, map.get(saoDataPreEntry.getType()));
            double v = saoYears.keySet().stream().map(Integer::new).mapToLong(Long::new).average().orElse(0.0);

//            if (v > 0.0) {
            preService.update(new UpdateWrapper<SaoDataPreEntry>().set("novelty", v).eq("id", saoDataPreEntry.getId()));
            saoDataService.update(new UpdateWrapper<SaoDataEntry>().set("novelty", v).eq("id", saoDataPreEntry.getId()));
//            }
        }
    }

    //计算增长
    @Test
    void increase() {
        List<SaoDataPreEntry> list = preService.list(new QueryWrapper<SaoDataPreEntry>().eq("termEN","The Advantage of Deep Learning over Traditional Shallow Learning Methods"));
        for (SaoDataPreEntry saoDataPreEntry : list) {
            Map<Integer, String> map = new HashMap<>();
            map.put(1, "Patent");//【专利】
            map.put(2, "Project");//【基金】
            map.put(3, "Thesis");//【论文】
            Query query = new Query();
            query.accurate(SolrFiled.TYPE, map.get(saoDataPreEntry.getType()))
                    .and()
                    .left()
                    .fuzziness(SolrFiled.TITLE, saoDataPreEntry.getTermEn())
                    .or()
                    .fuzziness(SolrFiled.ABSTRACT, saoDataPreEntry.getTermEn())
                    .right();
            LinkedHashMap<String, Long> saoYears = preService.getSaoYears(query, map.get(saoDataPreEntry.getType()));
            double n = n(saoYears);
            n = n * 100;
            DecimalFormat df = new DecimalFormat("#.00");
            String result = df.format(n);
            System.out.println("n = " + n);
//            if (n > 0.0) {
            preService.update(new UpdateWrapper<SaoDataPreEntry>().set("increase", result).eq("id", saoDataPreEntry.getId()));
            saoDataService.update(new UpdateWrapper<SaoDataEntry>().set("increase", result).eq("id", saoDataPreEntry.getId()));
//            }

        }
    }

    //增长性
    public static double n(LinkedHashMap<String, Long> saoYears) {
        saoYears = saoYears.entrySet().stream().sorted(Map.Entry.comparingByKey()).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        double num = 0.0;
        Long preLength = null;

        for (Map.Entry<String, Long> entry : saoYears.entrySet()) {
            if (saoYears.size() == 1) {
                num = 1L;
            } else {
                if (preLength != null) {
                    if (preLength != 0) {
                        num = num + ((entry.getValue() - preLength) / (double) (preLength));
                    }
                }
                preLength = entry.getValue();
            }
        }
        return num / 5;
    }


    //关注度(基金项目,论文)
    public double attention(LinkedHashMap<String, Long> saoYears, LinkedHashMap<String, Long> cited) {
        LinkedHashMap<String, Double> weight = new LinkedHashMap<>();
        weight.put("2017", (1d / 15d));
        weight.put("2018", (2d / 15d));
        weight.put("2019", (3d / 15d));
        weight.put("2020", (4d / 15d));
        weight.put("2021", (5d / 15d));
        weight.put("2022", (6d / 15d));

        FacetRequest facetRequest = new FacetRequest(SolrCollection.HANDLE_PAPER);
        facetRequest.q("Type:Patent");
        facetRequest.facet("PY");
        facetRequest.facet("CC");
        LinkedHashMap<String, Long> facet = SolrUtils.facet(facetRequest);


        return 0;
    }

    public double attentionPatent(LinkedHashMap<String, Long> saoYears) {


        return 0;
    }


    //交叉性
    @Test
    void crossover() {
        List<SaoDataPreEntry> list = preService.list();
        for (SaoDataPreEntry saoDataPreEntry : list) {
            Map<Integer, String> map = new HashMap<>();
            map.put(1, "Patent");//【专利】
            map.put(2, "Project");//【基金】
            map.put(3, "Thesis");//【论文】
            Query query = new Query();
            query.accurate(SolrFiled.TYPE, map.get(saoDataPreEntry.getType()))
                    .and()
                    .left()
                    .fuzziness(SolrFiled.TITLE, saoDataPreEntry.getTermEn())
                    .or()
                    .fuzziness(SolrFiled.ABSTRACT, saoDataPreEntry.getTermEn())
                    .right();

            List<LinkedHashMap<String, Long>> crossoverCount = preService.getCrossoverCount(query, map.get(saoDataPreEntry.getType()));
            if (crossoverCount.size() > 0) {
                Long value = crossoverCount.get(0).values().stream().reduce(Long::sum).orElse(0L);
                double r = ((double) value / (double) crossoverCount.get(1).get("numFound"));
                DecimalFormat df = new DecimalFormat("#.00");
                String result = df.format(r);
                System.out.println("result = " + result);
                preService.update(new UpdateWrapper<SaoDataPreEntry>().set("crossover", result).eq("id", saoDataPreEntry.getId()));
                saoDataService.update(new UpdateWrapper<SaoDataEntry>().set("crossover", result).eq("id", saoDataPreEntry.getId()));
//                LinkedHashMap<String, Long> oneResult = (LinkedHashMap<String, Long>)sonSaoYears.get(0);
//                List<LinkedHashMap<String, Long>> sonResult = (List<LinkedHashMap<String, Long>>)sonSaoYears.get(1);

//                List<Long> collect = sonResult.stream().map(x -> new ArrayList<>(x.values()).stream().reduce(Long::sum).orElse(0L)).collect(Collectors.toList());
//                List<List<Long>> lists = new ArrayList<>();
//                lists.add(new ArrayList<>(oneResult.values()));
//                lists.add(collect);
//                System.out.println("lists = " + lists);

            }

        }

    }


    @Test
    void interests() {
        List<SaoDataPreEntry> list = preService.list();
        for (SaoDataPreEntry saoDataPreEntry : list) {
            Map<Integer, String> map = new HashMap<>();
            map.put(1, "Patent");//【专利】
            map.put(2, "Project");//【基金】
            map.put(3, "Thesis");//【论文】
            Query query = new Query();
            query.accurate(SolrFiled.TYPE, map.get(saoDataPreEntry.getType()))
                    .and()
                    .left()
                    .fuzziness(SolrFiled.TITLE, saoDataPreEntry.getTermEn())
                    .or()
                    .fuzziness(SolrFiled.ABSTRACT, saoDataPreEntry.getTermEn())
                    .right();
            Map<String, Object> amountToDate = preService.getInterestsAwardedAmountToDate(query);
            if (amountToDate.size() > 0) {
                List<LinkedHashMap<String, Object>> linkedHashMaps = (List<LinkedHashMap<String, Object>>) amountToDate.get("data");
                List<Double> collect = linkedHashMaps.stream().map(x -> {
                    String amount = String.valueOf(x.get("AwardedAmountToDate"));
                    String replace = amount.replace("$", "").replace(".00", "").replace(".", "").replace(",", "");
                    return Double.valueOf(replace);
                }).collect(Collectors.toList());
                List<Double> cycleYearList = linkedHashMaps.stream().map(x -> {
                    String amount = String.valueOf(x.get("cycleYear"));
                    return Double.valueOf(amount);
                }).collect(Collectors.toList());
                double sumCount = 0;
                for (int i = 0; i < collect.size(); i++) {
                    sumCount += collect.get(i) / cycleYearList.get(i);
                }
                double s = sumCount / collect.size();


            }

        }


    }


    @Test
    void frontier() {
//        List<SaoDataPreEntry> list = preService.list();
//
//
//        List<Double> collectNovelty = list.stream().map(SaoDataPreEntry::getNovelty).filter(x -> x != 0.0).collect(Collectors.toList());
//        double noveltyAverage = collectNovelty.stream().mapToDouble(Double::doubleValue).average().orElse(0.0);
//
//        List<Double> collectIncrease = list.stream().map(SaoDataPreEntry::getIncrease).filter(x -> x != 0.0).collect(Collectors.toList());
//        double increaseAverage = collectIncrease.stream().mapToDouble(Double::doubleValue).average().orElse(0.0);
//
//        List<Double> collectAttention = list.stream().map(SaoDataPreEntry::getAttention).filter(x -> x != 0.0).collect(Collectors.toList());
//        double attentionAverage = collectAttention.stream().mapToDouble(Double::doubleValue).average().orElse(0.0);
//
//        List<Double> collectCrossover = list.stream().map(SaoDataPreEntry::getCrossover).filter(x -> x != 0.0).collect(Collectors.toList());
//        double crossoverAverage = collectCrossover.stream().mapToDouble(Double::doubleValue).average().orElse(0.0);
//
//        List<Double> collectInterests = list.stream().map(SaoDataPreEntry::getInterests).filter(x -> x != 0.0).collect(Collectors.toList());
//        double interestsAverage = collectInterests.stream().mapToDouble(Double::doubleValue).average().orElse(0.0);
//
//
//        for (SaoDataPreEntry saoDataPreEntry : list) {
//            int f = 0;
//            Query query = new Query();
//            query.left().fuzziness(SolrFiled.TITLE, saoDataPreEntry.getTermEn()).or().fuzziness(SolrFiled.ABSTRACT, saoDataPreEntry.getTermEn()).right();
//            Map<String, Object> amountToDate = preService.getInterestsAwardedAmountToDate(query);
//            if (amountToDate.size() > 0) {
//                List<LinkedHashMap<String, Object>> linkedHashMaps = (List<LinkedHashMap<String, Object>>) amountToDate.get("data");
//                List<String> types = linkedHashMaps.stream().map(x -> String.valueOf(x.get("Type"))).collect(Collectors.toList());
//                if (types.size() > 0) {
//                    boolean containProject = types.contains(SolrFiled.PROJECT);
//                    boolean containThesis = types.contains(SolrFiled.THESIS);
//                    boolean containPatent = types.contains(SolrFiled.PATENT);
//                    if (containProject && !containThesis && !containPatent) {
//                        //只存在于基金
//                        f = 4;
//                    } else {
//                        if (saoDataPreEntry.getNovelty() < noveltyAverage) {
//                            //新颖性低于平均值
//                            f = 1;
//                        } else {
//                            if (saoDataPreEntry.getAttention() > attentionAverage) {
//                                //关注度高于平均值
//                                f = 2;
//                            } else {
//                                if (saoDataPreEntry.getIncrease() > increaseAverage) {
//                                    f = 3;
//                                } else {
//                                    if (saoDataPreEntry.getCrossover() > crossoverAverage) {
//                                        f = 5;
//                                    } else {
//                                        f = 4;
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    saoDataPreEntry.setFrontier(f);
//                    preService.update(new UpdateWrapper<SaoDataPreEntry>().set("frontier", f).eq("id", saoDataPreEntry.getId()));
//                    saoDataService.update(new UpdateWrapper<SaoDataEntry>().set("frontier", f).eq("id", saoDataPreEntry.getId()));
//                }
//            }
//        }
    }


//
//    @Test
//    public void testCreateAndUpdateIndex() throws Exception {
//        // 1. 创建HttpSolrServer对象
//        // 设置solr服务接口,浏览器客户端地址http://127.0.0.1:8081/solr/#/
//
//        // 2. 创建SolrInputDocument对象
//        SolrQuery solrQuery = new SolrQuery();
//        solrQuery.setQuery("*:*");
//        QueryResponse query = solrClient.query(, solrQuery);
//
//        SolrDocumentList results = query.getResults();
//
//        System.out.println(results.toString());
//
//
//    }

    public static void main(String[] args) {
        List<Double> list = new ArrayList<>();
        list.add(5.5);
        list.add(6.5);
        list.add(7.5);

        double v = list.stream().mapToDouble(Double::doubleValue).average().orElse(0.0);
        System.out.println("v = " + v);
    }

}
